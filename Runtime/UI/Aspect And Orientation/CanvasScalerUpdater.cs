﻿using Buggestic.Runtime.Controllers;
using Buggestic.Runtime.Pooling.Pool_Manager;
using Buggestic.Runtime.Signals;
using Buggestic.Runtime.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Buggestic.Runtime.UI.Aspect_And_Orientation
{
    public class CanvasScalerUpdater : CachedMonoBehaviour
    {
        [Header("Portrait")]
        [SerializeField] private Vector2 _portraitReferenceResolution = new(1080f, 1920f);
        [SerializeField, Range(0f, 1f)] private float _portraitMatchWidthOrHeight;

        [Header("Landscape")]
        [SerializeField] private Vector2 _landscapeReferenceResolution = new(1920f, 1080f);
        [SerializeField, Range(0f, 1f)] private float _landscapeMatchWidthOrHeight = 1f;

        private CanvasScaler _canvasScaler;
        private Observer<ScreenOrientation> _onOrientationChangedObserver;

        protected override void AwakeImpl()
        {
            base.AwakeImpl();

            _canvasScaler = GetComponent<CanvasScaler>();
            _onOrientationChangedObserver = new Observer<ScreenOrientation>(OnOrientationChanged);
        }

        private void OnEnable()
        {
            BuggesticCore.ScreenAspectChangeDetector.OnOrientationChangedSignal.AddObserver(_onOrientationChangedObserver);
            OnOrientationChanged(ScreenUtils.ScreenOrientation);
        }

        private void OnDisable()
        {
            BuggesticCore.ScreenAspectChangeDetector.OnOrientationChangedSignal.RemoveObserver(_onOrientationChangedObserver);
        }

        private void OnOrientationChanged(ScreenOrientation screenOrientation)
        {
            switch (screenOrientation)
            {
                case ScreenOrientation.Portrait:
                    _canvasScaler.referenceResolution = _portraitReferenceResolution;
                    _canvasScaler.matchWidthOrHeight = _portraitMatchWidthOrHeight;
                    break;

                case ScreenOrientation.Landscape:
                    _canvasScaler.referenceResolution = _landscapeReferenceResolution;
                    _canvasScaler.matchWidthOrHeight = _landscapeMatchWidthOrHeight;
                    break;

                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(nameof(screenOrientation),
                        (int)screenOrientation,
                        typeof(ScreenOrientation));
            }
        }
    }
}
