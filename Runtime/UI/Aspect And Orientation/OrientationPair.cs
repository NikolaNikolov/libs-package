﻿using Buggestic.Runtime.Utils;
using UnityEngine;

namespace Buggestic.Runtime.UI.Aspect_And_Orientation
{
    [System.Serializable]
    public class OrientationPair
    {
        [SerializeField] private GameObject _bothOrientationsObject;
        [SerializeField] private GameObject _landscapeObject;
        [SerializeField] private GameObject _portraitObject;

        public GameObject LandscapeObject
        {
            get => _landscapeObject == null ? _bothOrientationsObject : _landscapeObject;
            set => _landscapeObject = value;
        }

        public GameObject PortraitObject
        {
            get => _portraitObject == null ? _bothOrientationsObject : _portraitObject;
            set => _portraitObject = value;
        }

        public GameObject SameOrientationObject => ScreenUtils.ScreenOrientation == ScreenOrientation.Landscape ? LandscapeObject : PortraitObject;

        public GameObject DifferentOrientationObject =>
            ScreenUtils.ScreenOrientation == ScreenOrientation.Landscape ? PortraitObject : LandscapeObject;
    }
}
