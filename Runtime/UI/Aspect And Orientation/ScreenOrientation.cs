﻿namespace Buggestic.Runtime.UI.Aspect_And_Orientation
{
    public enum ScreenOrientation
    {
        Landscape,
        Portrait,
    }
}
