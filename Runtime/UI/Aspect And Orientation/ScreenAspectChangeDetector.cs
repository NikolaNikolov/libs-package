﻿using Buggestic.Runtime.Pooling.Pool_Manager;
using Buggestic.Runtime.Signals;
using Buggestic.Runtime.Utils;

namespace Buggestic.Runtime.UI.Aspect_And_Orientation
{
    public class ScreenAspectChangeDetector : CachedMonoBehaviour
    {
        public readonly Signal<float> OnAspectChangedSignal = new();
        public readonly Signal<ScreenOrientation> OnOrientationChangedSignal = new();

        private float _lastFrameAspectRatio;
        private ScreenOrientation _lastFrameScreenOrientation;

        protected override void AwakeImpl()
        {
            base.AwakeImpl();

            _lastFrameAspectRatio = ScreenUtils.AspectRatio;
            _lastFrameScreenOrientation = ScreenUtils.ScreenOrientation;
        }

        private void Update()
        {
            float aspectRatio = ScreenUtils.AspectRatio;

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            if (aspectRatio != _lastFrameAspectRatio)
            {
                OnAspectChangedSignal.Invoke(aspectRatio);
            }

            ScreenOrientation screenOrientation = ScreenUtils.ScreenOrientation;
            if (screenOrientation != _lastFrameScreenOrientation)
            {
                OnOrientationChangedSignal.Invoke(screenOrientation);
            }

            _lastFrameAspectRatio = aspectRatio;
            _lastFrameScreenOrientation = screenOrientation;
        }
    }
}
