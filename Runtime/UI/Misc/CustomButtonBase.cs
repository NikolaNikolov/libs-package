﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace Buggestic.Runtime.UI.Misc
{
    public class CustomButtonBase : CustomSelectableBase, IPointerClickHandler, ISubmitHandler
    {
#if UNITY_EDITOR
        public static class CustomButtonBase_Editor
        {
            public const string ON_CLICK = nameof(m_OnClick);
        }
#endif

        [Serializable]
        public class ButtonClickedEvent : UnityEvent
        {
        }

        // ReSharper disable once InconsistentNaming
        [FormerlySerializedAs("onClick")] [SerializeField]
        private ButtonClickedEvent m_OnClick = new();

        // ReSharper disable once InconsistentNaming
        public ButtonClickedEvent onClick
        {
            get => m_OnClick;
            set => m_OnClick = value;
        }

        protected CustomButtonBase()
        {
        }

        public virtual void ExecuteSubmitEvent()
        {
            if (!IsActive() || !IsInteractable())
            {
                return;
            }

            ExecuteEvents.Execute(gameObject, new BaseEventData(EventSystem.current), ExecuteEvents.submitHandler);
        }

        public virtual void OnPointerClick(PointerEventData eventData)
        {
            if (eventData.button != PointerEventData.InputButton.Left)
            {
                return;
            }

            Press();
        }

        public virtual void OnSubmit(BaseEventData eventData)
        {
            Press();
        }

        protected virtual void Press()
        {
            if (!IsActive() || !IsInteractable())
            {
                return;
            }

            UISystemProfilerApi.AddMarker("Button.onClick", this);

            DoStateTransition(SelectionState.Pressed, false);
            StartCoroutine(OnFinishSubmit());
            PlayClickSound();

            m_OnClick.Invoke();
        }

        protected IEnumerator OnFinishSubmit()
        {
            float fadeTime = colors.fadeDuration;
            float elapsedTime = 0f;

            while (elapsedTime < fadeTime)
            {
                elapsedTime += Time.unscaledDeltaTime;
                yield return null;
            }

            DoStateTransition(GetStateFromCustomPriority(currentSelectionState), false);
        }

        protected virtual void PlayClickSound()
        {
        }
    }
}
