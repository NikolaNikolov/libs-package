﻿using PrimeTween;
using UnityEngine;

namespace Buggestic.Runtime.UI.Misc
{
    [CreateAssetMenu(menuName = "UI/ScreenTweenAnimationInfo")]
    public class ScreenTweenAnimationInfo : ScriptableObject
    {
        [SerializeField] private AnimationCurve _showCurve;
        [SerializeField] private AnimationCurve _hideCurve;
        [SerializeField] private float _showDuration;
        [SerializeField] private float _hideDuration;

        public AnimationCurve ShowCurve => _showCurve;
        public AnimationCurve HideCurve => _hideCurve;
        public float ShowDuration => _showDuration;
        public float HideDuration => _hideDuration;

        public virtual Tween GetFadeTweener(CanvasGroup targetCanvasGroup, bool useShowSettings)
        {
            if (useShowSettings)
            {
                targetCanvasGroup.alpha = 0f;
                return Tween.Alpha(targetCanvasGroup, 1f, _showDuration, _showCurve, useUnscaledTime: true);
            }
            else
            {
                targetCanvasGroup.alpha = 1f;
                return Tween.Alpha(targetCanvasGroup, 0f, _hideDuration, _hideCurve, useUnscaledTime: true);
            }
        }
    }
}
