using System;
using Buggestic.Runtime.Controllers;
using Buggestic.Runtime.Validation;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Buggestic.Runtime.UI.Misc
{
    public class CustomSelectableBase : Selectable
    {
#if UNITY_EDITOR
        public static class CustomSelectableBase_Editor
        {
            public const string ON_DO_STATE_TRANSITION = nameof(_onDoStateTransition);
        }
#endif

        [Serializable]
        public class OnDoStateTransitionEvent : UnityEvent<CustomSelectionState, bool>
        {
        }

        [SerializeField] private OnDoStateTransitionEvent _onDoStateTransition;
        [SerializeField, HideInInspector] private Animator _animator;

        public OnDoStateTransitionEvent OnDoStateTransition => _onDoStateTransition;
        public Animator Animator => _animator;

        public CustomSelectionState CurrentCustomSelectionState { get; private set; }

#if UNITY_EDITOR
        protected override void OnValidate()
        {
            using OnValidateChangeScope changeScope = OnValidateChangeScope.Get(this);

            if (transition == Transition.Animation)
            {
                changeScope.GetComponent(ref _animator);
                if (_animator != null)
                {
                    if (!_animator.writeDefaultValuesOnDisable)
                    {
                        _animator.writeDefaultValuesOnDisable = true;
                        OnValidateChangeScope.SetDirty(_animator);
                    }

                    if (_animator.updateMode != AnimatorUpdateMode.UnscaledTime)
                    {
                        _animator.updateMode = AnimatorUpdateMode.UnscaledTime;
                        OnValidateChangeScope.SetDirty(_animator);
                    }
                }
            }
            else
            {
                changeScope.Assign(ref _animator, null);
            }

            base.OnValidate();
        }
#endif

        protected SelectionState GetStateFromCustomPriority(SelectionState state)
        {
            if (state == SelectionState.Selected && BuggesticCore.UINavigationController.HoveredSelectable == this)
            {
                return SelectionState.Highlighted;
            }

            return state;
        }

        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            state = GetStateFromCustomPriority(state);
            CurrentCustomSelectionState = (CustomSelectionState)state;

            if (transition == Transition.Animation)
            {
                _animator.enabled = true;
            }

            base.DoStateTransition(state, instant);

            _onDoStateTransition?.Invoke(CurrentCustomSelectionState, instant);
        }

        protected override void InstantClearState()
        {
            if (transition == Transition.Animation)
            {
                _animator.enabled = true;
            }

            base.InstantClearState();

            DoStateTransition(GetStateFromCustomPriority(currentSelectionState), true);
        }

        protected override void OnCanvasGroupChanged()
        {
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                base.OnCanvasGroupChanged();
                return;
            }
#endif

            bool isInteractable = IsInteractable();

            base.OnCanvasGroupChanged();

            if (!isInteractable && IsInteractable())
            {
                if (BuggesticCore.UINavigationController.HoveredSelectable != this && currentSelectionState == SelectionState.Highlighted)
                {
                    base.OnPointerExit(null);
                }
            }
        }

        public override void OnPointerEnter(PointerEventData eventData)
        {
            bool wasSelected = currentSelectionState == SelectionState.Selected;

            BuggesticCore.UINavigationController.HoveredSelectable = this;

            base.OnPointerEnter(eventData);

            if (currentSelectionState == SelectionState.Highlighted || (wasSelected && currentSelectionState == SelectionState.Selected))
            {
                PlayHighlightedSound();
            }
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            if (BuggesticCore.UINavigationController.HoveredSelectable == this)
            {
                BuggesticCore.UINavigationController.HoveredSelectable = null;
            }

            base.OnPointerExit(eventData);
        }

        // public override void OnPointerUp(PointerEventData eventData)
        // {
        //     base.OnPointerUp(eventData);
        //
        //     if (eventData.button == PointerEventData.InputButton.Left)
        //     {
        //         EventSystem.current?.SetSelectedGameObject(null);
        //     }
        // }

        public override void OnSelect(BaseEventData eventData)
        {
            base.OnSelect(eventData);

            if (currentSelectionState == SelectionState.Selected)
            {
                PlayHighlightedSound();
            }
        }

        protected virtual void PlayHighlightedSound()
        {
        }
    }
}
