﻿using System.Collections.Generic;
using Buggestic.Runtime.UI.Base;

namespace Buggestic.Runtime.UI.Misc
{
    public class ScreenManagerQueue
    {
        public struct QueuedScreenInfo
        {
            public QueuedScreenInfo(ScreenBase screen, object data, bool isAnimated, bool forceAllowPopupOnPopup)
            {
                Screen = screen;
                Data = data;
                IsAnimated = isAnimated;
                ForceAllowPopupOnPopup = forceAllowPopupOnPopup;
            }

            public ScreenBase Screen { get; set; }
            public object Data { get; set; }
            public bool IsAnimated { get; set; }
            public bool ForceAllowPopupOnPopup { get; set; }
        }

        protected readonly HashSet<ScreenBase> _screensQueueLookup = new();
        protected readonly List<QueuedScreenInfo> _queuedScreenInfos = new();

        public int Count => _screensQueueLookup.Count;

        public QueuedScreenInfo this[int index] => _queuedScreenInfos[index];

        public void AddFirst(ScreenBase screen, object screenData, bool isAnimated, bool forceAllowPopupOnPopup)
        {
            if (_screensQueueLookup.Add(screen))
            {
                _queuedScreenInfos.Insert(0, new QueuedScreenInfo(screen, screenData, isAnimated, forceAllowPopupOnPopup));
            }
        }

        public void AddLast(ScreenBase screen, object screenData, bool isAnimated, bool forceAllowPopupOnPopup)
        {
            if (_screensQueueLookup.Add(screen))
            {
                _queuedScreenInfos.Add(new QueuedScreenInfo(screen, screenData, isAnimated, forceAllowPopupOnPopup));
            }
        }

        public QueuedScreenInfo GetFirst()
        {
            return this[0];
        }

        public QueuedScreenInfo GetAt(int index)
        {
            return this[index];
        }

        public void RemoveFirst()
        {
            RemoveAt(0);
        }

        public void RemoveAt(int index)
        {
            QueuedScreenInfo queueedScreenInfo = _queuedScreenInfos[index];

            _screensQueueLookup.Remove(queueedScreenInfo.Screen);
            _queuedScreenInfos.RemoveAt(index);
        }

        public bool Contains(ScreenBase screen)
        {
            return _screensQueueLookup.Contains(screen);
        }

        public void ClearQueue()
        {
            _screensQueueLookup.Clear();
            _queuedScreenInfos.Clear();
        }
    }
}
