﻿using Buggestic.Runtime.Pooling.Pool_Manager;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Buggestic.Runtime.UI.Misc
{
    [RequireComponent(typeof(EventSystem))]
    [AddComponentMenu("UI/Extensions/DragCorrector")]
    public class DragCorrector : CachedMonoBehaviour
    {
        public int BaseTh = 6;
        public int BasePpi = 210;

        private EventSystem _eventSystem;
        private int _initialDragThreshold;

        protected override void AwakeImpl()
        {
            base.AwakeImpl();

            _eventSystem = GetComponent<EventSystem>();
            _initialDragThreshold = _eventSystem.pixelDragThreshold;
        }

        private void OnEnable()
        {
            int dragTh = BaseTh * (int)Screen.dpi / BasePpi;
            _eventSystem.pixelDragThreshold = dragTh;
        }

        private void OnDisable()
        {
            _eventSystem.pixelDragThreshold = _initialDragThreshold;
        }
    }
}
