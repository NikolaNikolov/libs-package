﻿using System;
using System.Collections.Generic;
using Buggestic.Runtime.Controllers;
using Buggestic.Runtime.Controllers.Input;
using Buggestic.Runtime.Pooling.Pool_Manager;
using Buggestic.Runtime.Signals;
using Buggestic.Runtime.UI.Misc;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Buggestic.Runtime.UI.Base
{
    public class ScreenManagerBase : CachedMonoBehaviour
    {
        public readonly Signal<ScreenBase> OnOpenScreenAnimationStartedSignal = new();
        public readonly Signal<ScreenBase> OnOpenScreenAnimationEndedSignal = new();
        public readonly Signal<ScreenBase> OnCloseScreenAnimationStartedSignal = new();
        public readonly Signal<ScreenBase> OnCloseScreenAnimationEndedSignal = new();

        [SerializeField] protected bool _dontDestroyOnLoad = true;

        [Space] [SerializeField] protected Canvas _mainCanvas;

        protected readonly LinkedList<ScreenBase> _openScreens = new();
        protected readonly HashSet<ScreenBase> _openScreensLookup = new();
        protected readonly HashSet<ScreenBase> _animatingScreensLookup = new();

        protected readonly ScreenManagerQueue _screensQueue = new();

        protected bool _isClosingAllScreens;

        private Observer<InputType, InputType> _onInputTypeChangedObserver;
        private Observer<GameObject, GameObject> _onSelectedObjectChangedObserver;
        private Observer<Selectable, Selectable> _onHoveredSelectableChangedObserver;

        protected static readonly HashSet<Type> INSTANCES_BY_TYPE = new();

        public Canvas MainCanvas => _mainCanvas;
        public RectTransform MainCanvasRectTransform { get; private set; }

        public bool IsInitialized { get; protected set; }
        public int OpenAndQueuedScreensCount => _openScreensLookup.Count + _screensQueue.Count;

        public GameObject SelectedGameObject
        {
            get => EventSystem.current?.currentSelectedGameObject;
            set
            {
                if (_openScreens.Count == 0)
                {
                    return;
                }

                if (BuggesticCore.InputController.InputType.IsTouch())
                {
                    ScreenOnTop.SetPreviousSelectable(value, false);
                }
                else
                {
                    if (ScreenOnTop.IsInteractable)
                    {
                        EventSystem.current?.SetSelectedGameObject(value);
                    }
                    else
                    {
                        ScreenOnTop.SetPreviousSelectable(value, false);
                    }
                }

                //if (BuggesticCore.InputController.InputType.IsKeyboardOrController())
                //{
                //    if (ScreenOnTop.IsInteractable)
                //    {
                //        EventSystem.current?.SetSelectedGameObject(value);
                //    }
                //    else
                //    {
                //        ScreenOnTop.SetPreviousSelectable(value, false);
                //    }
                //}
                //else
                //{
                //    ScreenOnTop.SetPreviousSelectable(value, false);
                //}
            }
        }

        protected ScreenBase ScreenOnTop => _openScreens.Last.Value;

#if UNITY_EDITOR
        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.SubsystemRegistration)]
        private static void Init()
        {
            INSTANCES_BY_TYPE.Clear();
        }
#endif

        protected override void AwakeImpl()
        {
            base.AwakeImpl();

            MainCanvasRectTransform = (RectTransform)_mainCanvas.transform;

            _onInputTypeChangedObserver = new Observer<InputType, InputType>(OnInputTypeChanged);
            _onSelectedObjectChangedObserver = new Observer<GameObject, GameObject>(OnSelectedObjectChanged);
            _onHoveredSelectableChangedObserver = new Observer<Selectable, Selectable>(OnHoveredSelectableChanged);

            Type type = GetType();
            if (!INSTANCES_BY_TYPE.Add(type))
            {
                Destroy(gameObject);
                return;
            }

            if (_dontDestroyOnLoad)
            {
                DontDestroyOnLoad(gameObject);
            }
        }

        protected virtual void Start()
        {
            BuggesticCore.InputController.OnInputTypeChangedSignal.AddObserver(_onInputTypeChangedObserver);
            BuggesticCore.UINavigationController.OnSelectedObjectChangedSignal.AddObserver(_onSelectedObjectChangedObserver);
            BuggesticCore.UINavigationController.OnHoveredSelectableChangedSignal.AddObserver(_onHoveredSelectableChangedObserver);

            IsInitialized = true;
        }

        protected virtual void OnDestroy()
        {
            INSTANCES_BY_TYPE.Remove(GetType());

            BuggesticCore.InputController.OnInputTypeChangedSignal.RemoveObserver(_onInputTypeChangedObserver);
            BuggesticCore.UINavigationController.OnSelectedObjectChangedSignal.RemoveObserver(_onSelectedObjectChangedObserver);
            BuggesticCore.UINavigationController.OnHoveredSelectableChangedSignal.RemoveObserver(_onHoveredSelectableChangedObserver);
        }

        private void LateUpdate()
        {
            LateUpdateTopScreen();
            HandleInput();
        }

        public virtual void OpenScreen(ScreenBase screen, object screenData = null, bool isAnimated = true, bool forceAllowPopupOnPopup = false)
        {
            if (_isClosingAllScreens)
            {
                return;
            }

            if (ShouldEnqueueScreen(forceAllowPopupOnPopup))
            {
                _screensQueue.AddLast(screen, screenData, isAnimated, forceAllowPopupOnPopup);
                return;
            }

            if (_openScreensLookup.Contains(screen))
            {
                UnityEngine.Debug.LogError($"Screen {screen} is already open", screen);
                return;
            }

            if (_openScreens.Count > 0)
            {
                ScreenOnTop.UpdateInteractableState(false, false);
                ScreenOnTop.OnFocusLost(false);
            }

            _openScreens.AddLast(screen);
            _openScreensLookup.Add(screen);
            _animatingScreensLookup.Add(screen);

            screen.IsAnimated = isAnimated;
            screen.IsAnimating = true;
            screen.OnEnter(screenData);
            screen.OnFocusGained(true);
            screen.PlayEnterAnimation();
        }

        public virtual void CloseScreen(ScreenBase screen, bool isAnimated = true)
        {
            if (!_openScreensLookup.Contains(screen))
            {
                UnityEngine.Debug.LogError($"Screen {screen} is not open", screen);
                return;
            }

            if (ScreenOnTop == screen)
            {
                _openScreens.RemoveLast();
            }
            else
            {
                _openScreens.Remove(screen);
            }

            _openScreensLookup.Remove(screen);
            _animatingScreensLookup.Add(screen);

            screen.IsAnimated = isAnimated;
            screen.IsAnimating = true;
            screen.OnFocusLost(true);
            screen.OnExit();
            screen.PlayExitAnimation();
        }

        public virtual void ToggleScreen(ScreenBase previousScreen,
            ScreenBase newScreen,
            object screenData = null,
            bool isAnimated = true,
            bool forceAllowPopupOnPopup = false)
        {
            if (_isClosingAllScreens)
            {
                return;
            }

            _screensQueue.AddFirst(newScreen, screenData, isAnimated, forceAllowPopupOnPopup);
            CloseScreen(previousScreen, isAnimated);
        }

        public virtual void CloseAllScreensAndClearQueue(bool isAnimated = true)
        {
            _screensQueue.ClearQueue();
            while (_openScreens.Count > 0)
            {
                CloseScreen(ScreenOnTop, isAnimated);
            }
        }

        public virtual async Awaitable CloseAllScreensAndClearQueueAsync(bool isAnimated = true)
        {
            _isClosingAllScreens = true;

            CloseAllScreensAndClearQueue(isAnimated);
            while (IsAnyScreenAnimating())
            {
                await Awaitable.NextFrameAsync();
            }

            _isClosingAllScreens = false;
        }

        public virtual void TriggerOnOpenScreenAnimationStarted(ScreenBase screen)
        {
            OnOpenScreenAnimationStartedSignal.Invoke(screen);
        }

        public virtual void TriggerOnOpenScreenAnimationEnded(ScreenBase screen)
        {
            _animatingScreensLookup.Remove(screen);

            OnOpenScreenAnimationEndedSignal.Invoke(screen);

            TryOpenScreenFromQueue();
        }

        public virtual void TriggerOnCloseScreenAnimationStarted(ScreenBase screen)
        {
            OnCloseScreenAnimationStartedSignal.Invoke(screen);
        }

        public virtual void TriggerOnCloseScreenAnimationEnded(ScreenBase screen)
        {
            _animatingScreensLookup.Remove(screen);

            OnCloseScreenAnimationEndedSignal.Invoke(screen);

            EventSystem.current?.SetSelectedGameObject(null);

            bool isOpeningScreenFromQueue = TryOpenScreenFromQueue();
            if (!isOpeningScreenFromQueue && _openScreens.Count > 0)
            {
                ScreenOnTop.UpdateInteractableState(true, true);
                ScreenOnTop.OnFocusGained(false);
            }
        }

        public virtual bool IsScreenOpen(ScreenBase screen)
        {
            return _openScreensLookup.Contains(screen);
        }

        public virtual bool IsScreenQueued(ScreenBase screen)
        {
            return _screensQueue.Contains(screen);
        }

        public virtual bool IsScreenOpenOrQueued(ScreenBase screen)
        {
            return IsScreenOpen(screen) || IsScreenQueued(screen);
        }

        public virtual bool IsScreenOnTop(ScreenBase screen)
        {
            if (_openScreens.Count == 0)
            {
                return false;
            }

            return ScreenOnTop == screen;
        }

        protected virtual bool ShouldEnqueueScreen(bool forceAllowPopupOnPopup)
        {
            if (IsAnyScreenAnimating())
            {
                return true;
            }

            ScreenBase screenOnTop = GetScreenOnTop();
            if (screenOnTop is null)
            {
                return false;
            }

            return screenOnTop.IsPopup && !forceAllowPopupOnPopup;
        }

        public virtual bool IsAnyScreenAnimating()
        {
            return _animatingScreensLookup.Count > 0;
        }

        public virtual ScreenBase GetScreenOnTop()
        {
            return _openScreens.Count > 0 ? ScreenOnTop : null;
        }

        public virtual bool HasInteractableScreen()
        {
            return _openScreens.Count > 0 && ScreenOnTop.IsInteractable;
        }

        protected virtual bool TryOpenScreenFromQueue()
        {
            if (_screensQueue.Count == 0)
            {
                return false;
            }

            for (int i = 0; i < _screensQueue.Count; i++)
            {
                ScreenManagerQueue.QueuedScreenInfo queuedScreenInfo = _screensQueue[i];
                if (!ShouldEnqueueScreen(queuedScreenInfo.ForceAllowPopupOnPopup))
                {
                    _screensQueue.RemoveAt(i);

                    OpenScreen(queuedScreenInfo.Screen, queuedScreenInfo.Data, queuedScreenInfo.IsAnimated, queuedScreenInfo.ForceAllowPopupOnPopup);
                    return true;
                }
            }

            return false;
        }

        protected virtual void LateUpdateTopScreen()
        {
            if (_openScreens.Count == 0)
            {
                return;
            }

            ScreenOnTop.LateUpdateScreen();
        }

        protected virtual void HandleInput()
        {
            if (!HasInteractableScreen())
            {
                return;
            }

            ScreenOnTop.HandleInput();
        }

        protected virtual void OnInputTypeChanged(InputType previousInputType, InputType newInputType)
        {
            if (_openScreens.Count == 0)
            {
                return;
            }

            if (previousInputType.IsTouch() && !newInputType.IsTouch())
            {
                if (ScreenOnTop.IsInteractable)
                {
                    ScreenOnTop.UpdateSelection(true, true);
                }
            }
            else if (!previousInputType.IsTouch() && newInputType.IsTouch())
            {
                EventSystem.current?.SetSelectedGameObject(null);
            }

            //if (!previousInputType.IsKeyboardOrController() && newInputType.IsKeyboardOrController())
            //{
            //    if (ScreenOnTop.IsInteractable)
            //    {
            //        ScreenOnTop.UpdateSelection(true, true);
            //    }
            //}
            //else if (previousInputType.IsKeyboardOrController() && !newInputType.IsKeyboardOrController())
            //{
            //    if (!previousInputType.IsKeyboard() || !newInputType.IsMouse())
            //    {
            //        EventSystem.current?.SetSelectedGameObject(null);
            //    }
            //}
        }

        protected virtual void OnSelectedObjectChanged(GameObject previousSelectedObject, GameObject newSelectedObject)
        {
            if (_openScreens.Count == 0)
            {
                return;
            }

            if (newSelectedObject is not null)
            {
                ScreenOnTop.SetPreviousSelectable(newSelectedObject, false);
            }
        }

        protected virtual void OnHoveredSelectableChanged(Selectable previousHoveredSelectable, Selectable newHoveredSelectable)
        {
            if (_openScreens.Count == 0)
            {
                return;
            }

            if (newHoveredSelectable == null)
            {
                //if (EventSystem.current?.currentSelectedGameObject == previousHoveredSelectable.gameObject)
                //{
                //    EventSystem.current?.SetSelectedGameObject(null);
                //}
            }
            else if (newHoveredSelectable.navigation.mode != Navigation.Mode.None)
            {
                if (ScreenOnTop.IsInteractable)
                {
                    ScreenOnTop.SetPreviousSelectable(newHoveredSelectable.gameObject, true);
                }
            }

            //if (newHoveredSelectable != null && newHoveredSelectable.navigation.mode != Navigation.Mode.None)
            //{
            //    ScreenOnTop.SetPreviousSelectable(newHoveredSelectable.gameObject, false);
            //}
        }
    }
}
