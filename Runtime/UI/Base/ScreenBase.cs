﻿using Buggestic.Runtime.Controllers;
using Buggestic.Runtime.Controllers.Input;
using Buggestic.Runtime.Pooling.Pool_Manager;
using Buggestic.Runtime.Signals;
using Buggestic.Runtime.UI.Aspect_And_Orientation;
using Buggestic.Runtime.Utils;
using Buggestic.Runtime.Validation;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using ScreenOrientation = Buggestic.Runtime.UI.Aspect_And_Orientation.ScreenOrientation;

namespace Buggestic.Runtime.UI.Base
{
    [RequireComponent(typeof(Canvas), typeof(CanvasGroup))]
    public abstract class ScreenBase : CachedMonoBehaviour
    {
        [SerializeField, HideInInspector] protected Canvas _screenCanvas;
        [SerializeField, HideInInspector] protected CanvasGroup _canvasGroup;
        [SerializeField, HideInInspector] protected GraphicRaycaster[] _graphicRaycasters;

        [SerializeField] protected bool _isPopup;
        [SerializeField] protected bool _alwaysFocusDefaultSelectable;

        [Space] [SerializeField] protected OrientationPair _defaultSelectablePair;

        [Space] [SerializeField] protected OrientationPair[] _toggledByOrientationPairs;
        [SerializeField] protected OrientationPair[] _toggledByOrientationSelectablePairs;

        protected ScreenManagerBase _managerBase;
        protected GameObject _previousSelectable;
        protected bool _isFocused;

        private Observer<ScreenOrientation> _onOrientationChangedObserver;
        private Observer<InputType, InputType> _onInputTypeChangedObserver;

        public Canvas ScreenCanvas => _screenCanvas;
        public CanvasGroup CanvasGroup => _canvasGroup;

        public bool IsInteractable
        {
            get => _canvasGroup.interactable;
            private set
            {
                _canvasGroup.interactable = value;
                _canvasGroup.blocksRaycasts = value;
            }
        }

        public bool IsPopup => _isPopup;
        public bool IsFocused => _isFocused;

        public bool IsAnimated { get; set; }
        public bool IsAnimating { get; set; }

        public bool IsOpen => _managerBase.IsScreenOpen(this);
        public bool IsQueued => _managerBase.IsScreenQueued(this);
        public bool IsOpenOrQueued => _managerBase.IsScreenOpenOrQueued(this);
        public bool IsOnTop => _managerBase.IsScreenOnTop(this);

        protected override void OnValidateImpl(OnValidateChangeScope changeScope)
        {
            base.OnValidateImpl(changeScope);

            changeScope.GetComponent(ref _screenCanvas);
            changeScope.GetComponent(ref _canvasGroup);
            changeScope.GetComponentsInChildren(ref _graphicRaycasters, true);
        }

        protected override void AwakeImpl()
        {
            base.AwakeImpl();

            _onOrientationChangedObserver = new Observer<ScreenOrientation>(OnOrientationChanged);
            _onInputTypeChangedObserver = new Observer<InputType, InputType>(OnInputTypeChanged);

            _screenCanvas.enabled = false;
            IsInteractable = false;
            UpdateGraphicRaycasters();
        }

        public virtual void Initialize(ScreenManagerBase managerBase)
        {
            _managerBase = managerBase;
        }

        public virtual void OnEnter(object data)
        {
            _screenCanvas.enabled = true;
            UpdateInteractableState(false, false);

            OnEnterAddListeners();

            _managerBase.TriggerOnOpenScreenAnimationStarted(this);
        }

        public virtual void OnExit()
        {
            UpdateInteractableState(false, false);

            _managerBase.TriggerOnCloseScreenAnimationStarted(this);
        }

        public virtual void OnFocusGained(bool isOpeningScreen)
        {
            _isFocused = true;
        }

        public virtual void OnFocusLost(bool isClosingScreen)
        {
            _isFocused = false;
        }

        public virtual void PlayEnterAnimation()
        {
            OnEnterAnimationCompleted();
        }

        public virtual void PlayExitAnimation()
        {
            OnExitAnimationCompleted();
        }

        public virtual void LateUpdateScreen()
        {
        }

        public virtual void UpdateInteractableState(bool isInteractable, bool updateSelection)
        {
            IsInteractable = isInteractable;

            if (isInteractable && _managerBase.IsScreenOnTop(this))
            {
                if (updateSelection)
                {
                    UpdateSelection(true, false);
                }
            }

            UpdateGraphicRaycasters();
        }

        public virtual void SetPreviousSelectable(GameObject selectable, bool updateSelection)
        {
            if (_previousSelectable != selectable)
            {
                if (!selectable.transform.IsChildOf(transform))
                {
                    return;
                }

                _previousSelectable = selectable;
            }

            if (updateSelection)
            {
                UpdateSelection(true, true);
            }
        }

        public virtual void UpdateSelection(bool selectPreviousSelectable, bool forcePreviousSelectable)
        {
            if (_alwaysFocusDefaultSelectable && !forcePreviousSelectable)
            {
                if (_defaultSelectablePair.SameOrientationObject is not null)
                {
                    _managerBase.SelectedGameObject = _defaultSelectablePair.SameOrientationObject;
                }
            }
            else if (selectPreviousSelectable && _previousSelectable is not null)
            {
                _managerBase.SelectedGameObject = _previousSelectable;
            }
            else
            {
                if (_defaultSelectablePair.SameOrientationObject is not null)
                {
                    _managerBase.SelectedGameObject = _defaultSelectablePair.SameOrientationObject;
                }
            }
        }

        public virtual void HandleInput()
        {
            if (BuggesticCore.InputController.HasSubmitButtonDown)
            {
                HandleSubmitInput();
            }

            if (BuggesticCore.InputController.HasCancelButtonDown)
            {
                HandleCancelInput();
            }

            if (BuggesticCore.InputController.HasTabNextNavigationButtonDown)
            {
                HandleTabNextNavigationInput();
            }

            if (BuggesticCore.InputController.HasTabPreviousNavigationButtonDown)
            {
                HandleTabPreviousNavigationInput();
            }
        }

        protected abstract void HandleSubmitInput();
        protected abstract void HandleCancelInput();

        protected virtual void HandleTabNextNavigationInput()
        {
            HandleTabNavigation(false);
        }

        protected virtual void HandleTabPreviousNavigationInput()
        {
            HandleTabNavigation(true);
        }

        protected virtual void HandleTabNavigation(bool reverseDirections)
        {
            if (EventSystem.current?.currentSelectedGameObject is not null)
            {
                Selectable initialSelectable = EventSystem.current.currentSelectedGameObject.GetComponent<Selectable>();
                if (initialSelectable is not null)
                {
                    Selectable currentSelectable = initialSelectable;

                    // Navigate right
                    Selectable nextSelectableRight =
                        reverseDirections ? currentSelectable.FindSelectableOnLeft() : currentSelectable.FindSelectableOnRight();
                    if (nextSelectableRight is not null)
                    {
                        SetPreviousSelectable(nextSelectableRight.gameObject, true);
                        return;
                    }

                    // Return all the way to the left
                    while (true)
                    {
                        Selectable nextSelectableLeft =
                            reverseDirections ? currentSelectable.FindSelectableOnRight() : currentSelectable.FindSelectableOnLeft();
                        if (nextSelectableLeft is not null)
                        {
                            currentSelectable = nextSelectableLeft;
                        }
                        else
                        {
                            break;
                        }
                    }

                    // Navigate down
                    Selectable nextSelectableDown =
                        reverseDirections ? currentSelectable.FindSelectableOnUp() : currentSelectable.FindSelectableOnDown();
                    if (nextSelectableDown is not null)
                    {
                        SetPreviousSelectable(nextSelectableDown.gameObject, true);
                        return;
                    }

                    // Return all the way to top-right
                    while (true)
                    {
                        Selectable nextSelectableLeft =
                            reverseDirections ? currentSelectable.FindSelectableOnRight() : currentSelectable.FindSelectableOnLeft();
                        if (nextSelectableLeft is not null)
                        {
                            currentSelectable = nextSelectableLeft;
                        }

                        Selectable nextSelectableUp =
                            reverseDirections ? currentSelectable.FindSelectableOnDown() : currentSelectable.FindSelectableOnUp();
                        if (nextSelectableUp is not null)
                        {
                            currentSelectable = nextSelectableUp;
                        }

                        if (nextSelectableLeft is null && nextSelectableUp is null)
                        {
                            break;
                        }
                    }

                    if (currentSelectable != initialSelectable)
                    {
                        SetPreviousSelectable(currentSelectable.gameObject, true);
                    }
                }
            }
        }

        protected virtual void UpdateGraphicRaycasters()
        {
            bool areEnabled = IsInteractable && BuggesticCore.InputController.InputType.IsMouseOrTouch();
            for (int i = _graphicRaycasters.Length - 1; i >= 0; i--)
            {
                _graphicRaycasters[i].enabled = areEnabled;
            }
        }

        protected virtual void OnEnterAnimationCompleted()
        {
            IsAnimating = false;
            UpdateInteractableState(true, true);

            _managerBase.TriggerOnOpenScreenAnimationEnded(this);
        }

        protected virtual void OnExitAnimationCompleted()
        {
            IsAnimating = false;
            _screenCanvas.enabled = false;

            OnExitRemoveListeners();

            _managerBase.TriggerOnCloseScreenAnimationEnded(this);
        }

        protected virtual void OnEnterAddListeners()
        {
            BuggesticCore.ScreenAspectChangeDetector?.OnOrientationChangedSignal.AddObserver(_onOrientationChangedObserver);
            OnOrientationChanged(ScreenUtils.ScreenOrientation);

            BuggesticCore.InputController.OnInputTypeChangedSignal.AddObserver(_onInputTypeChangedObserver);
            OnInputTypeChanged(InputType.None, BuggesticCore.InputController.InputType);
        }

        protected virtual void OnExitRemoveListeners()
        {
            BuggesticCore.ScreenAspectChangeDetector?.OnOrientationChangedSignal.RemoveObserver(_onOrientationChangedObserver);
            BuggesticCore.InputController.OnInputTypeChangedSignal.RemoveObserver(_onInputTypeChangedObserver);
        }

        protected virtual void OnOrientationChanged(ScreenOrientation orientation)
        {
            foreach (OrientationPair toggledPair in _toggledByOrientationPairs)
            {
                toggledPair.SameOrientationObject.SetActive(true);
                toggledPair.DifferentOrientationObject.SetActive(false);
            }

            foreach (OrientationPair selectable in _toggledByOrientationSelectablePairs)
            {
                if (_managerBase.SelectedGameObject == selectable.DifferentOrientationObject)
                {
                    _managerBase.SelectedGameObject = selectable.SameOrientationObject;
                }

                if (_previousSelectable == selectable.DifferentOrientationObject)
                {
                    _previousSelectable = selectable.SameOrientationObject;
                }
            }
        }

        protected virtual void OnInputTypeChanged(InputType previousInputType, InputType newInputType)
        {
            UpdateGraphicRaycasters();
        }
    }
}
