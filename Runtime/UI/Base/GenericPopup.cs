﻿using System;
using Buggestic.Runtime.UI.Misc;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Buggestic.Runtime.UI.Base
{
    public class GenericPopup : ScreenAnimated
    {
        public class GenericPopupData
        {
            public string Title { get; }
            public string Description { get; }
            public string ConfirmButtonText { get; }
            public string CancelButtonText { get; }
            public bool HasCancelButton { get; }
            public bool HasBackgroundEventTrigger { get; }
            public Action ConfirmCallbackBeforeClosing { get; }
            public Action ConfirmCallbackAfterClosing { get; }
            public Action CancelCallbackBeforeClosing { get; }
            public Action CancelCallbackAfterClosing { get; }

            public GenericPopupData(string title,
                string description,
                string confirmButtonText,
                string cancelButtonText,
                bool hasCancelButton,
                bool hasBackgroundEventTrigger,
                Action confirmCallbackBeforeClosing,
                Action confirmCallbackAfterClosing,
                Action cancelCallbackBeforeClosing,
                Action cancelCallbackAfterClosing)
            {
                Title = title;
                Description = description;
                ConfirmButtonText = confirmButtonText;
                CancelButtonText = cancelButtonText;
                HasCancelButton = hasCancelButton;
                HasBackgroundEventTrigger = hasBackgroundEventTrigger;
                ConfirmCallbackBeforeClosing = confirmCallbackBeforeClosing;
                ConfirmCallbackAfterClosing = confirmCallbackAfterClosing;
                CancelCallbackBeforeClosing = cancelCallbackBeforeClosing;
                CancelCallbackAfterClosing = cancelCallbackAfterClosing;
            }
        }

        [Space] [SerializeField] protected TextMeshProUGUI _titleText;
        [SerializeField] protected TextMeshProUGUI _descriptionText;
        [SerializeField] protected CustomButtonBase _confirmButton;
        [SerializeField] protected TextMeshProUGUI _confirmButtonText;
        [SerializeField] protected CustomButtonBase _cancelButton;
        [SerializeField] protected TextMeshProUGUI _cancelButtonText;
        [SerializeField] protected EventTrigger _backgroundEventTrigger;

        protected GenericPopupData _genericData;

        protected bool _hasConfirmed;

        public override void OnEnter(object data)
        {
            base.OnEnter(data);

            _genericData = (GenericPopupData)data;

            _titleText.text = _genericData.Title;
            _descriptionText.text = _genericData.Description;
            _confirmButtonText.text = _genericData.ConfirmButtonText;

            if (_genericData.HasCancelButton)
            {
                _cancelButtonText.text = _genericData.CancelButtonText;
            }

            _cancelButton.gameObject.SetActive(_genericData.HasCancelButton);
            _backgroundEventTrigger.enabled = _genericData.HasBackgroundEventTrigger;
        }

        protected override void OnExitAnimationCompleted()
        {
            Action callbackAfterClosing = _hasConfirmed ? _genericData.ConfirmCallbackAfterClosing : _genericData.CancelCallbackAfterClosing;

            base.OnExitAnimationCompleted();

            callbackAfterClosing?.Invoke();
        }

        public void OnConfirmButton()
        {
            _hasConfirmed = true;

            _genericData.ConfirmCallbackBeforeClosing?.Invoke();
            ClosePopup();
        }

        public void OnCancelButton()
        {
            _hasConfirmed = false;

            _genericData.CancelCallbackBeforeClosing?.Invoke();
            ClosePopup();
        }

        protected virtual void ClosePopup()
        {
            _managerBase.CloseScreen(this);
        }

        protected override void HandleSubmitInput()
        {
            _confirmButton.ExecuteSubmitEvent();
        }

        protected override void HandleCancelInput()
        {
            if (_genericData.HasCancelButton)
            {
                _cancelButton.ExecuteSubmitEvent();
            }
            else
            {
                _confirmButton.ExecuteSubmitEvent();
            }
        }
    }
}
