﻿using Buggestic.Runtime.UI.Misc;
using PrimeTween;
using UnityEngine;
using UnityEngine.UI;

namespace Buggestic.Runtime.UI.Base
{
    public abstract class ScreenAnimated : ScreenBase
    {
        [SerializeField] protected Image _backgroundImageToFade;

        [Space] [SerializeField] protected ScreenTweenAnimationInfo _screenTweenAnimationInfo;

        protected float _backgroundImageDefaultAlpha;

        public ScreenTweenAnimationInfo ScreenTweenAnimationInfo
        {
            get => _screenTweenAnimationInfo;
            set => _screenTweenAnimationInfo = value;
        }

        protected virtual void Start()
        {
            if (_backgroundImageToFade != null)
            {
                _backgroundImageDefaultAlpha = _backgroundImageToFade.color.a;
            }
        }

        public virtual Tween GetFadeTweener(CanvasGroup targetCanvasGroup, bool useShowSettings)
        {
            return _screenTweenAnimationInfo.GetFadeTweener(targetCanvasGroup, useShowSettings);
        }

        public override void PlayEnterAnimation()
        {
            UpdateBackgroundImageToFadeRaycastTarget(false);

            Sequence enterAnimationSequence = Sequence.Create(useUnscaledTime: true);

            FadeCanvasGroup(ref enterAnimationSequence, true);
            FadeBackgroundImage(ref enterAnimationSequence,
                0f,
                _backgroundImageDefaultAlpha,
                _screenTweenAnimationInfo.ShowDuration,
                _screenTweenAnimationInfo.ShowCurve);
            enterAnimationSequence.OnComplete(this, target => target.OnPlayEnterAnimationTweenCompleted());

            if (!IsAnimated)
            {
                enterAnimationSequence.Complete();
            }
        }

        private void OnPlayEnterAnimationTweenCompleted()
        {
            UpdateBackgroundImageToFadeRaycastTarget(true);
            OnEnterAnimationCompleted();
        }

        public override void PlayExitAnimation()
        {
            UpdateBackgroundImageToFadeRaycastTarget(false);

            Sequence exitAnimationSequence = Sequence.Create(useUnscaledTime: true);

            FadeCanvasGroup(ref exitAnimationSequence, false);
            FadeBackgroundImage(ref exitAnimationSequence,
                _backgroundImageDefaultAlpha,
                0f,
                _screenTweenAnimationInfo.HideDuration,
                _screenTweenAnimationInfo.HideCurve);
            exitAnimationSequence.OnComplete(this, target => target.OnExitAnimationCompleted());

            if (!IsAnimated)
            {
                exitAnimationSequence.Complete();
            }
        }

        protected virtual void FadeCanvasGroup(ref Sequence sequence, bool useShowSettings)
        {
            Tween fadeTweener = GetFadeTweener(_canvasGroup, useShowSettings);
            sequence.Insert(0f, fadeTweener);
        }

        protected virtual void FadeBackgroundImage(ref Sequence sequence, float startAlpha, float targetAlpha, float duration, AnimationCurve curve)
        {
            if (_backgroundImageToFade == null)
            {
                return;
            }

            Color backgroundImageStartColor = _backgroundImageToFade.color;
            backgroundImageStartColor.a = startAlpha;
            _backgroundImageToFade.color = backgroundImageStartColor;

            Tween fadeTweener = Tween.Alpha(_backgroundImageToFade, targetAlpha, duration, curve, useUnscaledTime: true);
            sequence.Insert(0f, fadeTweener);
        }

        protected virtual void UpdateBackgroundImageToFadeRaycastTarget(bool isRaycastTarget)
        {
            if (_backgroundImageToFade == null)
            {
                return;
            }

            _backgroundImageToFade.raycastTarget = isRaycastTarget;
        }
    }
}
