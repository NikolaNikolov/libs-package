﻿using Buggestic.Runtime.Pooling.Pool_Manager;
using TMPro;
using UnityEngine;

namespace Buggestic.Runtime.UI.Debug
{
    public class FPSCounter : CachedMonoBehaviour
    {
        [SerializeField] private float _updateInterval = 0.25f;

        [Space] [SerializeField] private TextMeshProUGUI _fpsLabel;

        private float _totalDeltaTime;
        private int _framesCount;

        public float FPS { get; private set; }

        private void Update()
        {
            _totalDeltaTime += Time.unscaledDeltaTime;
            _framesCount++;

            if (_totalDeltaTime > _updateInterval)
            {
                FPS = _framesCount / _totalDeltaTime;
                _fpsLabel.text = Mathf.RoundToInt(FPS).ToString();

                _totalDeltaTime = 0;
                _framesCount = 0;
            }
        }
    }
}
