﻿using System;
using System.Threading;
using UnityEngine;

namespace Buggestic.Runtime.Utils
{
    public static class AwaitableUtils
    {
        public static async Awaitable WaitUntil(Func<bool> condition, CancellationToken cancellationToken = default)
        {
            while (!condition())
            {
                cancellationToken.ThrowIfCancellationRequested();
                await Awaitable.NextFrameAsync(cancellationToken);
            }
        }
    }
}
