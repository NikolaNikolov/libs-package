using UnityEngine;

namespace Buggestic.Runtime.Utils
{
    public static class UIUtils
    {
        public static Vector2 ClampAnchoredPositionInsideCanvas(RectTransform canvasRectTransform,
            RectTransform targetRectTransform,
            Vector2 anchoredPosition)
        {
            Vector2 canvasSizeDelta = canvasRectTransform.sizeDelta;
            Vector2 targetSizeDelta = targetRectTransform.sizeDelta;
            Vector2 targetPivot = targetRectTransform.pivot;

            Vector2 anchorOffset = canvasSizeDelta * (targetRectTransform.anchorMin - Vector2.one / 2f);
            Vector2 maxPivotOffset = targetSizeDelta * (targetPivot - ((Vector2.one / 2f) * 2f));
            Vector2 minPivotOffset = targetSizeDelta * (((Vector2.one / 2f) * 2f) - targetPivot);

            float minX = ((canvasSizeDelta.x) * -0.5f) - anchorOffset.x - minPivotOffset.x + targetSizeDelta.x;
            float maxX = ((canvasSizeDelta.x) * 0.5f) - anchorOffset.x + maxPivotOffset.x;
            float minY = ((canvasSizeDelta.y) * -0.5f) - anchorOffset.y - minPivotOffset.y + targetSizeDelta.y;
            float maxY = ((canvasSizeDelta.y) * 0.5f) - anchorOffset.y + maxPivotOffset.y;

            anchoredPosition.x = Mathf.Clamp(anchoredPosition.x, minX, maxX);
            anchoredPosition.y = Mathf.Clamp(anchoredPosition.y, minY, maxY);

            return anchoredPosition;
        }
    }
}
