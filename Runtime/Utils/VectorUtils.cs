﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace Buggestic.Runtime.Utils
{
    public static class VectorUtils
    {
        public enum IntersectionType
        {
            None,
            Intersecting,
            IntersectingOutOfBounds,
            Parallel,
            Orthogonal
        }

        // Vector conversion
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ZeroX(this Vector3 v)
        {
            v.x = 0f;
            return v;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ZeroY(this Vector3 v)
        {
            v.y = 0f;
            return v;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ZeroZ(this Vector3 v)
        {
            v.z = 0f;
            return v;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 WithX(this Vector3 v, float x)
        {
            v.x = x;
            return v;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 WithY(this Vector3 v, float y)
        {
            v.y = y;
            return v;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 WithZ(this Vector3 v, float z)
        {
            v.z = z;
            return v;
        }

        // Normalize
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 NormalizedFromMagnitude(this Vector3 v, float magnitude)
        {
            return magnitude > 9.999999747378752E-06 ? v / magnitude : Vector3.zero;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 NormalizedFromMagnitude(this Vector2 v, float magnitude)
        {
            return magnitude > 9.999999747378752E-06 ? v / magnitude : Vector2.zero;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 NormalizedFromSqrMagnitude(this Vector3 v, float sqrMagnitude)
        {
            float magnitude = Mathf.Sqrt(sqrMagnitude);
            return NormalizedFromMagnitude(v, magnitude);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 NormalizedFromSqrMagnitude(this Vector2 v, float sqrMagnitude)
        {
            float magnitude = Mathf.Sqrt(sqrMagnitude);
            return NormalizedFromMagnitude(v, magnitude);
        }

        // Absolute value
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 Abs(Vector2 a)
        {
            return new Vector2(Mathf.Abs(a.x), Mathf.Abs(a.y));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2Int Abs(Vector2Int a)
        {
            return new Vector2Int(Mathf.Abs(a.x), Mathf.Abs(a.y));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 Abs(Vector3 a)
        {
            return new Vector3(Mathf.Abs(a.x), Mathf.Abs(a.y), Mathf.Abs(a.z));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3Int Abs(Vector3Int a)
        {
            return new Vector3Int(Mathf.Abs(a.x), Mathf.Abs(a.y), Mathf.Abs(a.z));
        }

        // Lerping
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float InverseLerp(Vector3 a, Vector3 b, Vector3 value)
        {
            Vector3 ab = b - a;
            Vector3 av = value - a;
            return Mathf.Clamp01(Vector3.Dot(av, ab) / Vector3.Dot(ab, ab));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float InverseLerp(Vector2 a, Vector2 b, Vector2 value)
        {
            Vector2 ab = b - a;
            Vector2 av = value - a;
            return Mathf.Clamp01(Vector2.Dot(av, ab) / Vector2.Dot(ab, ab));
        }

        // Relations between lines and points
        public static Vector2 FindNearestPointOnLine(Vector2 lineStart, Vector2 lineEnd, Vector2 point)
        {
            //Get heading
            Vector2 heading = (lineEnd - lineStart);
            float magnitudeMax = heading.magnitude;
            heading.Normalize();

            //Do projection from the point but clamp it
            Vector2 lhs = point - lineStart;
            float dotP = Vector2.Dot(lhs, heading);
            dotP = Mathf.Clamp(dotP, 0f, magnitudeMax);
            return lineStart + heading * dotP;
        }

        public static IntersectionType IsIntersecting(out Vector2 intersectionPosition,
            Vector2 l1Start,
            Vector2 l1End,
            Vector2 l2Start,
            Vector2 l2End)
        {
            //Direction of the lines
            Vector2 l1Dir = (l1End - l1Start).normalized;
            Vector2 l2Dir = (l2End - l2Start).normalized;

            //If we know the direction we can get the normal vector to each line
            Vector2 l1Normal = new(-l1Dir.y, l1Dir.x);
            Vector2 l2Normal = new(-l2Dir.y, l2Dir.x);


            //Step 1: are the lines parallel? -> no solutions
            if (IsParallel(l1Normal, l2Normal))
            {
                intersectionPosition = Vector2.zero;
                return IntersectionType.Parallel;
            }


            //Step 2: are the lines the same line? -> infinite amount of solutions
            //Pick one point on each line and test if the vector between the points is orthogonal to one of the normals
            if (IsOrthogonal(l1Start - l2Start, l1Normal))
            {
                intersectionPosition = Vector2.zero;
                return IntersectionType.Orthogonal;
            }


            //Step 3: Rewrite the lines to a general form: Ax + By = k1 and Cx + Dy = k2
            //The normal vector is the A, B
            float a = l1Normal.x;
            float b = l1Normal.y;

            float c = l2Normal.x;
            float d = l2Normal.y;

            //To get k we just use one point on the line
            float k1 = (a * l1Start.x) + (b * l1Start.y);
            float k2 = (c * l2Start.x) + (d * l2Start.y);


            //Step 4: calculate the intersection point -> one solution
            float xIntersect = (d * k1 - b * k2) / (a * d - b * c);
            float yIntersect = (-c * k1 + a * k2) / (a * d - b * c);

            intersectionPosition = new Vector2(xIntersect, yIntersect);


            //Step 5: but we have line segments so we have to check if the intersection point is within the segment
            if (IsBetween(l1Start, l1End, intersectionPosition) && IsBetween(l2Start, l2End, intersectionPosition))
            {
                return IntersectionType.Intersecting;
            }

            return IntersectionType.IntersectingOutOfBounds;
        }

        public static bool IsParallel(Vector2 v1, Vector2 v2)
        {
            //2 vectors are parallel if the angle between the vectors are 0 or 180 degrees
            return Vector2.Angle(v1, v2) == 0f || Mathf.Approximately(Vector2.Angle(v1, v2), 180f);
        }

        public static bool IsOrthogonal(Vector2 v1, Vector2 v2)
        {
            //2 vectors are orthogonal is the dot product is 0
            //We have to check if close to 0 because of floating numbers
            if (Mathf.Abs(Vector2.Dot(v1, v2)) < 0.000001f)
            {
                return true;
            }

            return false;
        }

        public static bool IsBetween(Vector2 a, Vector2 b, Vector2 value)
        {
            bool isBetween = false;

            //Entire line segment
            Vector2 ab = b - a;
            //The intersection and the first point
            Vector2 ac = value - a;

            //Need to check 2 things: 
            //1. If the vectors are pointing in the same direction = if the dot product is positive
            //2. If the length of the vector between the intersection and the first point is smaller than the entire line
            if (Vector2.Dot(ab, ac) > 0f && ab.sqrMagnitude >= ac.sqrMagnitude)
            {
                isBetween = true;
            }

            return isBetween;
        }
    }
}
