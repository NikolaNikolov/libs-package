﻿using UnityEngine;

namespace Buggestic.Runtime.Utils
{
    public static class ParticleSystemUtils
    {
        public static void ApplyEmissionMultiplier(this ParticleSystem particleSystem, float multiplier)
        {
            ParticleSystem.EmissionModule emissionModule = particleSystem.emission;

            emissionModule.rateOverTime = emissionModule.rateOverTime.ApplyMultiplier(multiplier, false);
            emissionModule.rateOverDistance = emissionModule.rateOverDistance.ApplyMultiplier(multiplier, false);

            for (int i = 0; i < emissionModule.burstCount; i++)
            {
                ParticleSystem.Burst burst = emissionModule.GetBurst(i);

                burst.count = burst.count.ApplyMultiplier(multiplier, true);

                if (burst.cycleCount > 1)
                {
                    burst.cycleCount = (int)ApplyMultiplier(burst.cycleCount, multiplier, true);
                }

                burst.repeatInterval = ApplyMultiplier(burst.repeatInterval, 1f / multiplier, false);

                emissionModule.SetBurst(i, burst);
            }
        }

        public static ParticleSystem.MinMaxCurve ApplyMultiplier(this ParticleSystem.MinMaxCurve minMaxCurve, float multiplier, bool ceilToInt)
        {
            switch (minMaxCurve.mode)
            {
                case ParticleSystemCurveMode.Constant:
                    minMaxCurve.constant = ApplyMultiplier(minMaxCurve.constant, multiplier, ceilToInt);
                    break;

                case ParticleSystemCurveMode.Curve:
                case ParticleSystemCurveMode.TwoCurves:
                    minMaxCurve.curveMultiplier *= multiplier;
                    break;

                case ParticleSystemCurveMode.TwoConstants:
                    minMaxCurve.constantMin = ApplyMultiplier(minMaxCurve.constantMin, multiplier, ceilToInt);
                    minMaxCurve.constantMax = ApplyMultiplier(minMaxCurve.constantMax, multiplier, ceilToInt);
                    break;

                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(nameof(minMaxCurve.mode),
                        (int)minMaxCurve.mode,
                        typeof(ParticleSystemCurveMode));
            }

            return minMaxCurve;
        }

        private static float ApplyMultiplier(float value, float multiplier, bool ceilToInt)
        {
            value *= multiplier;
            if (ceilToInt)
            {
                value = Mathf.CeilToInt(value);
            }

            return value;
        }

        public static float CalculateMaxValue(this ParticleSystem.MinMaxCurve minMaxCurve)
        {
            return minMaxCurve.mode switch
            {
                ParticleSystemCurveMode.Constant => minMaxCurve.constant,
                ParticleSystemCurveMode.TwoConstants => Mathf.Max(minMaxCurve.constantMin, minMaxCurve.constantMax),
                ParticleSystemCurveMode.Curve => minMaxCurve.curve.CalculateMaxValue(),
                ParticleSystemCurveMode.TwoCurves => Mathf.Max(minMaxCurve.curveMin.CalculateMaxValue(), minMaxCurve.curveMax.CalculateMaxValue()),
                _ => throw new System.ComponentModel.InvalidEnumArgumentException(nameof(minMaxCurve.mode),
                    (int)minMaxCurve.mode,
                    typeof(ParticleSystemCurveMode)),
            };
        }

        public static float CalculateMaxValue(this AnimationCurve curve)
        {
            float maxValue = float.MinValue;

            for (int i = 0; i < curve.length; i++)
            {
                if (curve[i].value > maxValue)
                {
                    maxValue = curve[i].value;
                }
            }

            return maxValue;
        }
    }
}
