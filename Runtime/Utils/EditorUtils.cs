﻿#if UNITY_EDITOR
using System;
using System.Collections;
using Unity.EditorCoroutines.Editor;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Buggestic.Runtime.Utils
{
    public static class EditorUtils
    {
        public const string BACKING_FIELD_FORMAT = "<{0}>k__BackingField";

        public static string GetBackingFieldName(string fieldName)
        {
            return string.Format(BACKING_FIELD_FORMAT, fieldName);
        }

        public static bool IsPrefab(this GameObject gameObject)
        {
            return PrefabUtility.IsPartOfAnyPrefab(gameObject) ||
                   (PrefabStageUtility.GetCurrentPrefabStage() != null &&
                    PrefabStageUtility.GetCurrentPrefabStage().IsPartOfPrefabContents(gameObject));
        }

        public static void ExecuteAfterAssetDatabaseUpdate(Action action)
        {
            EditorCoroutineUtility.StartCoroutineOwnerless(ExecuteAfterAssetDatabaseUpdateCoroutine(action));
        }

        public static void ExecuteAfterAssetDatabaseUpdate(Action action, object owner)
        {
            if (EditorApplication.isUpdating)
            {
                EditorCoroutineUtility.StartCoroutine(ExecuteAfterAssetDatabaseUpdateCoroutine(action), owner);
            }
            else
            {
                action();
            }
        }

        private static IEnumerator ExecuteAfterAssetDatabaseUpdateCoroutine(Action action)
        {
            while (EditorApplication.isUpdating)
            {
                yield return null;
            }

            action();
        }
    }
}
#endif
