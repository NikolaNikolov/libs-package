﻿using UnityEngine;
using ScreenOrientation = Buggestic.Runtime.UI.Aspect_And_Orientation.ScreenOrientation;

namespace Buggestic.Runtime.Utils
{
    public static class ScreenUtils
    {
        public static float AspectRatio => (float)Screen.width / Screen.height;

        public static float AspectRatioReversed => (float)Screen.height / Screen.width;

        public static ScreenOrientation ScreenOrientation => Screen.width >= Screen.height ? ScreenOrientation.Landscape : ScreenOrientation.Portrait;
    }
}
