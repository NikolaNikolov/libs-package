using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Buggestic.Runtime.Utils
{
    public static class CollectionUtils
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T GetFirst<T>(this HashSet<T> hashSet)
        {
            foreach (T value in hashSet)
            {
                return value;
            }

            return default;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TValue GetFirst<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            foreach (TValue value in dictionary.Values)
            {
                return value;
            }

            return default;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T GetElementAt<T>(this HashSet<T> hashSet, int index)
        {
            int currentIndex = 0;
            foreach (T value in hashSet)
            {
                if (currentIndex == index)
                {
                    return value;
                }

                currentIndex++;
            }

            return default;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static KeyValuePair<TKey, TValue> GetElementAt<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, int index)
        {
            int currentIndex = 0;
            foreach (KeyValuePair<TKey, TValue> keyValuePair in dictionary)
            {
                if (currentIndex == index)
                {
                    return keyValuePair;
                }

                currentIndex++;
            }

            return default;
        }
    }
}
