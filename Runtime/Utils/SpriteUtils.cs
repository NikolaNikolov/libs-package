﻿using UnityEngine;

namespace Buggestic.Runtime.Utils
{
    public static class SpriteUtils
    {
        public static Sprite GetSpriteFromBytes(byte[] bytes)
        {
            Texture2D texture = new(1, 1, TextureFormat.RGBA32, false);
            texture.LoadImage(bytes);

            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0.5f, 0.5f));

            return sprite;
        }
    }
}
