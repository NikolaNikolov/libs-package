﻿using System.Runtime.CompilerServices;
using UnityEngine;

namespace Buggestic.Runtime.Utils
{
    public static class CameraUtils
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float GetHalfWidth2D(this Camera camera)
        {
            float halfWidth = camera.orthographicSize * ScreenUtils.AspectRatio;
            return halfWidth;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float GetHalfHeight2D(this Camera camera)
        {
            return camera.orthographicSize;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 GetHalfArea2D(this Camera camera, Vector2 padding = new())
        {
            return new Vector2(camera.GetHalfWidth2D() - padding.x, camera.GetHalfHeight2D() - padding.y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 GetHalfArea2D(this Camera camera, float padding = 0f)
        {
            return camera.GetHalfArea2D(new Vector2(padding, padding));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float GetTopBound2D(this Camera camera)
        {
            return camera.transform.position.y + camera.orthographicSize;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float GetBottomBound2D(this Camera camera)
        {
            return camera.transform.position.y - camera.orthographicSize;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float GetLeftBound2D(this Camera camera)
        {
            return camera.transform.position.x - camera.GetHalfWidth2D();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float GetRightBound2D(this Camera camera)
        {
            return camera.transform.position.x + camera.GetHalfWidth2D();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Bounds GetBounds2D(this Camera camera, Vector2 padding = new())
        {
            return new Bounds(camera.transform.position, camera.GetHalfArea2D(padding));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 GetMouseWorldPosition2D(this Camera camera, Vector3 mousePosition)
        {
            return camera.ScreenToWorldPoint(mousePosition);
        }
    }
}
