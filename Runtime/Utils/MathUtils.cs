using UnityEngine;

namespace Buggestic.Runtime.Utils
{
    public static class MathUtils
    {
        public static float ClampAngle180(float value, float min, float max)
        {
            value %= 360f;
            if (value < 0f)
            {
                value += 360f;
            }

            return Mathf.Clamp(value - 180f, min, max);
        }

        public static void CalculateJumpApexStats(float jumpImpulse,
            float jumpGravity,
            float fallGravity,
            out float jumpApexHeight,
            out float jumpApexDuration,
            out float fallApexDuration)
        {
            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (jumpGravity == 0f)
            {
                jumpApexDuration = 0f;
                jumpApexHeight = 0f;
                fallApexDuration = 0f;
                return;
            }

            if (jumpGravity < 0f)
            {
                jumpGravity = -jumpGravity;
            }

            if (fallGravity < 0f)
            {
                fallGravity = -fallGravity;
            }

            jumpApexDuration = jumpImpulse / jumpGravity;
            jumpApexHeight = jumpGravity * jumpApexDuration * jumpApexDuration * 0.5f;
            fallApexDuration = Mathf.Sqrt(jumpApexHeight / (fallGravity * 0.5f));
        }
    }
}
