using UnityEngine;

namespace Buggestic.Runtime.Utils
{
    public static class GizmoUtils
    {
        public static void DrawWireCubeGizmo(Vector3 position, Quaternion rotation, Vector3 size)
        {
            Matrix4x4 originalMatrix = Gizmos.matrix;
            Gizmos.matrix = Matrix4x4.TRS(position, rotation, Vector3.one);
            Gizmos.DrawWireCube(Vector3.zero, size);
            Gizmos.matrix = originalMatrix;
        }
    }
}
