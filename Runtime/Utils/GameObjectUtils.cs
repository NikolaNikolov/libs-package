﻿using System;
using System.Text;
using Buggestic.Runtime.Validation;
using UnityEngine;
using UnityEngine.Pool;

namespace Buggestic.Runtime.Utils
{
    public static class GameObjectUtils
    {
        public static bool IsDontDestroyOnLoad(this GameObject gameObject)
        {
            return gameObject.scene.buildIndex == -1;
        }

        public static void SetLayerWithChildren(this GameObject gameObject, LayerMask layerMask)
        {
            SetLayerWithChildren(gameObject.transform, layerMask);
        }

        public static void SetLayerWithChildren(this Transform transform, LayerMask layerMask)
        {
            int layerIndex = Mathf.RoundToInt(Mathf.Log(layerMask, 2f));
            SetLayerWithChildren(transform, layerIndex);
        }

        public static void SetLayerWithChildren(this GameObject gameObject, int layerIndex)
        {
            SetLayerWithChildren(gameObject.transform, layerIndex);
        }

        public static void SetLayerWithChildren(this Transform transform, int layerIndex)
        {
            transform.gameObject.layer = layerIndex;
            foreach (Transform childTransform in transform)
            {
                childTransform.SetLayerWithChildren(layerIndex);
            }
        }

        public static string GetHierarchyName(this GameObject gameObject)
        {
            using (GenericPool<StringBuilder>.Get(out StringBuilder builder))
            {
                builder.Append(gameObject.name);

                Transform currentTransform = gameObject.transform.parent;
                while (currentTransform is not null)
                {
                    builder.Insert(0, '/');
                    builder.Insert(0, currentTransform.name);
                    currentTransform = currentTransform.parent;
                }

                string hierarchyName = builder.ToString();
                builder.Clear();
                return hierarchyName;
            }
        }

        public static void OnValidateSafeDelayed(Action callback)
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.delayCall += () => callback();
#else
            callback();
#endif
        }

        public static void OnValidateSafeAddComponentIfMissing<T>(this GameObject gameObject, Action<T> callback = null) where T : Component
        {
            if (gameObject.TryGetComponent(out T component))
            {
                callback?.Invoke(component);
                return;
            }

            OnValidateSafeDelayed(() =>
            {
                if (gameObject == null)
                {
                    return;
                }

                if (!gameObject.TryGetComponent(out component))
                {
                    component = gameObject.AddComponent<T>();
                    OnValidateChangeScope.SetDirty(gameObject);
                }

                callback?.Invoke(component);
            });
        }

        public static void OnValidateSafeRemoveComponent<T>(this GameObject gameObject, Action callback = null) where T : Component
        {
            if (!gameObject.TryGetComponent(out T _))
            {
                callback?.Invoke();
                return;
            }

            OnValidateSafeDelayed(() =>
            {
                if (gameObject == null)
                {
                    return;
                }

                if (gameObject.TryGetComponent(out T component))
                {
                    GameObject.DestroyImmediate(component, true);
                    OnValidateChangeScope.SetDirty(gameObject);
                }

                callback?.Invoke();
            });
        }
    }
}
