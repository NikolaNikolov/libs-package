using UnityEngine;

namespace Buggestic.Runtime.Utils
{
    public static class AnimationUtils
    {
        public static float EvaluateAverage(this AnimationCurve animationCurve, float startTime, float endTime, int samplesCount = 3)
        {
            float timeStep = (endTime - startTime) / (samplesCount - 1);
            float totalValue = 0f;
            for (int i = 0; i < samplesCount; i++)
            {
                totalValue += animationCurve.Evaluate(startTime + (i * timeStep));
            }

            return totalValue / samplesCount;
        }

        public static void ResetAllTriggers(this Animator animator)
        {
            foreach (AnimatorControllerParameter parameter in animator.parameters)
            {
                if (parameter.type == AnimatorControllerParameterType.Trigger)
                {
                    animator.ResetTrigger(parameter.name);
                }
            }
        }
    }
}
