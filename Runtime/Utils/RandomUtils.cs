﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

namespace Buggestic.Runtime.Utils
{
    public static class RandomUtils
    {
        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T GetRandomElement<T>(this List<T> list)
        {
            int randomIndex = Random.Range(0, list.Count);
            return list[randomIndex];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T GetRandomElementExcept<T>(this List<T> list, int exceptIndex)
        {
            int randomIndex = GetRandomRangeExcept(0, list.Count, exceptIndex);
            return list[randomIndex];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T RemoveRandomElement<T>(this List<T> list)
        {
            int randomIndex = Random.Range(0, list.Count);
            T randomElement = list[randomIndex];
            list.RemoveAt(randomIndex);
            return randomElement;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T RemoveRandomElementExcept<T>(this List<T> list, int exceptIndex)
        {
            int randomIndex = GetRandomRangeExcept(0, list.Count, exceptIndex);
            T randomElement = list[randomIndex];
            list.RemoveAt(randomIndex);
            return randomElement;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T GetRandomElement<T>(this HashSet<T> hashSet)
        {
            int randomIndex = Random.Range(0, hashSet.Count);
            return hashSet.GetElementAt(randomIndex);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T GetRandomElementExcept<T>(this HashSet<T> hashSet, int exceptIndex)
        {
            int randomIndex = GetRandomRangeExcept(0, hashSet.Count, exceptIndex);
            return hashSet.GetElementAt(randomIndex);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T RemoveRandomElement<T>(this HashSet<T> hashSet)
        {
            int randomIndex = Random.Range(0, hashSet.Count);
            T randomElement = hashSet.GetElementAt(randomIndex);
            hashSet.Remove(randomElement);
            return randomElement;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T RemoveRandomElementExcept<T>(this HashSet<T> hashSet, int exceptIndex)
        {
            int randomIndex = GetRandomRangeExcept(0, hashSet.Count, exceptIndex);
            T randomElement = hashSet.GetElementAt(randomIndex);
            hashSet.Remove(randomElement);
            return randomElement;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static KeyValuePair<TKey, TValue> GetRandomElement<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            int randomIndex = Random.Range(0, dictionary.Count);
            return dictionary.GetElementAt(randomIndex);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static KeyValuePair<TKey, TValue> GetRandomElementExcept<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, int exceptIndex)
        {
            int randomIndex = GetRandomRangeExcept(0, dictionary.Count, exceptIndex);
            return dictionary.GetElementAt(randomIndex);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static KeyValuePair<TKey, TValue> RemoveRandomElement<TKey, TValue>(this Dictionary<TKey, TValue> dictionary)
        {
            int randomIndex = Random.Range(0, dictionary.Count);
            KeyValuePair<TKey, TValue> randomElement = dictionary.GetElementAt(randomIndex);
            dictionary.Remove(randomElement.Key);
            return randomElement;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static KeyValuePair<TKey, TValue> RemoveRandomElementExcept<TKey, TValue>(this Dictionary<TKey, TValue> dictionary, int exceptIndex)
        {
            int randomIndex = GetRandomRangeExcept(0, dictionary.Count, exceptIndex);
            KeyValuePair<TKey, TValue> randomElement = dictionary.GetElementAt(randomIndex);
            dictionary.Remove(randomElement.Key);
            return randomElement;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T GetRandomElement<T>(this T[] array)
        {
            int randomIndex = Random.Range(0, array.Length);
            return array[randomIndex];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T GetRandomElementExcept<T>(this T[] array, int exceptIndex)
        {
            int randomIndex = GetRandomRangeExcept(0, array.Length, exceptIndex);
            return array[randomIndex];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static int GetRandomRangeExcept(int min, int max, int except)
        {
            int randomInt = Random.Range(min, max - 1);
            if (randomInt >= except)
            {
                randomInt++;
            }

            return randomInt;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TEnum GetRandomEnumValueAlloc<TEnum>() where TEnum : struct, System.IConvertible
        {
            System.Array enumValues = System.Enum.GetValues(typeof(TEnum));
            return (TEnum)enumValues.GetValue(Random.Range(0, enumValues.Length));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static TEnum GetRandomEnumValueAlloc<TEnum>(params TEnum[] valuesToChooseFrom) where TEnum : struct, System.IConvertible
        {
            int randomIndex = Random.Range(0, valuesToChooseFrom.Length);
            TEnum randomEnumValue = valuesToChooseFrom[randomIndex];
            return randomEnumValue;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static bool GetRandomBoolValue()
        {
            return Random.Range(0, 2) == 1;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static T GetRandomValueAlloc<T>(params T[] values)
        {
            int randomIndex = Random.Range(0, values.Length);
            return values[randomIndex];
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 GetRandomPointInsideCircleXZ(float radius, float y)
        {
            Vector2 randomInsideCircle2D = Random.insideUnitCircle * radius;
            return new Vector3(randomInsideCircle2D.x, y, randomInsideCircle2D.y);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 GetRandomPointOnCircleXZ(float radius, float y)
        {
            return GetRandomRotationY() * new Vector3(0f, y, radius);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 GetRandomPointInsideBounds2D(this Bounds bounds)
        {
            return new Vector2(Random.Range(bounds.min.x, bounds.max.x), Random.Range(bounds.min.y, bounds.max.y));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 GetRandomPointInsideBounds(this Bounds bounds)
        {
            return new Vector3(Random.Range(bounds.min.x, bounds.max.x),
                Random.Range(bounds.min.y, bounds.max.y),
                Random.Range(bounds.min.z, bounds.max.z));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 GetRandomPointInsideBoundsX0Z(this Bounds bounds)
        {
            return new Vector3(Random.Range(bounds.min.x, bounds.max.x), 0f, Random.Range(bounds.min.z, bounds.max.z));
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Quaternion GetRandomRotationY()
        {
            return Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float ApplyRandomSpread(this float value, float spread)
        {
            return value + GetRandomSpread(spread);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector2 ApplyRandomSpread(this Vector2 value, float spread)
        {
            float randomSpread = GetRandomSpread(spread);
            if (randomSpread == 0f)
            {
                return value;
            }

            return value + (value.normalized * randomSpread);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ApplyRandomSpread(this Vector3 value, float spread)
        {
            float randomSpread = GetRandomSpread(spread);
            if (randomSpread == 0f)
            {
                return value;
            }

            return value + (value.normalized * randomSpread);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static Vector3 ApplyRandomYAngleSpread(this Vector3 value, float spread)
        {
            float randomSpread = GetRandomSpread(spread);
            if (randomSpread == 0f)
            {
                return value;
            }

            return Quaternion.Euler(0f, spread, 0f) * value;
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public static float GetRandomSpread(float spread)
        {
            return spread > 0f ? Random.Range(-spread, spread) : 0f;
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            int count = list.Count;
            int last = count - 1;
            for (int i = 0; i < last; ++i)
            {
                int randomIndex = Random.Range(i, count);
                (list[i], list[randomIndex]) = (list[randomIndex], list[i]);
            }
        }
    }
}
