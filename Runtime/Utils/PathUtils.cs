﻿using System.IO;

namespace Buggestic.Runtime.Utils
{
    public static class PathUtils
    {
        public static string NormalizeDirectorySeparators(this string path)
        {
            return path.Replace(Path.AltDirectorySeparatorChar, Path.DirectorySeparatorChar);
        }
    }
}
