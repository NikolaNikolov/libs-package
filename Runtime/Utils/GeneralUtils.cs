﻿using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Pool;

namespace Buggestic.Runtime.Utils
{
    public static class GeneralUtils
    {
        public static void Log(params object[] messages)
        {
            using (GenericPool<StringBuilder>.Get(out StringBuilder builder))
            {
                for (int i = 0; i < messages.Length; i++)
                {
                    if (i > 0)
                    {
                        builder.Append(" | ");
                    }

                    builder.Append(messages[i]);
                }

                Debug.Log(builder);
                builder.Clear();
            }
        }

        public static string NicifyVariableName(string name)
        {
#if UNITY_EDITOR
            return UnityEditor.ObjectNames.NicifyVariableName(name);
#else
            return name;
#endif
        }

        public static string GetScriptableObjectFolderPath_Editor(ScriptableObject scriptableObject)
        {
#if UNITY_EDITOR
            string fullPath = UnityEditor.AssetDatabase.GetAssetPath(scriptableObject);
            int nameIndex = fullPath.LastIndexOf(scriptableObject.name, StringComparison.Ordinal);
            return fullPath[..nameIndex];
#else
            return null;
#endif
        }

        public static void LoadScriptableObject_Editor<T>(ref T scriptableObject, string nameFilter = null, params string[] searchInFolders)
            where T : ScriptableObject
        {
#if UNITY_EDITOR
            string[] assetGuids = UnityEditor.AssetDatabase.FindAssets($"t:{typeof(T)} {nameFilter}", searchInFolders);
            if (assetGuids.Length > 0)
            {
                string assetGuid = assetGuids[0];
                scriptableObject = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(UnityEditor.AssetDatabase.GUIDToAssetPath(assetGuid));
            }
#endif
        }

        public static void LoadScriptableObjects_Editor<T>(ref T[] scriptableObjects, string nameFilter = null, params string[] searchInFolders)
            where T : ScriptableObject
        {
#if UNITY_EDITOR
            string[] assetGuids = UnityEditor.AssetDatabase.FindAssets($"t:{typeof(T)} {nameFilter}", searchInFolders);
            scriptableObjects = new T[assetGuids.Length];

            for (int i = 0; i < assetGuids.Length; i++)
            {
                scriptableObjects[i] = UnityEditor.AssetDatabase.LoadAssetAtPath<T>(UnityEditor.AssetDatabase.GUIDToAssetPath(assetGuids[i]));
            }
#endif
        }

        public static void LoadScriptableObjects_Editor<T>(ref List<T> scriptableObjects, string nameFilter = null, params string[] searchInFolders)
            where T : ScriptableObject
        {
#if UNITY_EDITOR
            string[] assetGuids = UnityEditor.AssetDatabase.FindAssets($"t:{typeof(T)} {nameFilter}", searchInFolders);
            scriptableObjects = new List<T>(assetGuids.Length);

            foreach (string assetGuid in assetGuids)
            {
                scriptableObjects.Add(UnityEditor.AssetDatabase.LoadAssetAtPath<T>(UnityEditor.AssetDatabase.GUIDToAssetPath(assetGuid)));
            }
#endif
        }

        public static bool Contains(this string source, string toCheck, StringComparison comparison)
        {
            return source?.IndexOf(toCheck, comparison) >= 0;
        }
    }
}
