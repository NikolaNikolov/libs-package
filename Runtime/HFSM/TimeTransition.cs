using System;

namespace Buggestic.Runtime.HFSM
{
    public class TimeTransition<T> : TransitionBase<T> where T : struct, Enum
    {
        private readonly float _transitionTime;

        public TimeTransition(T toStateId, float transitionTime) : base(toStateId)
        {
            _transitionTime = transitionTime;
        }

        public override bool CanTransition(State<T> state)
        {
            return state.ActiveTime >= _transitionTime;
        }
    }
}
