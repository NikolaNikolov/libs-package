using System;

namespace Buggestic.Runtime.HFSM
{
    public abstract class TransitionBase<T> where T : struct, Enum
    {
        public T ToStateId { get; }

        protected TransitionBase(T toStateId)
        {
            ToStateId = toStateId;
        }

        public abstract bool CanTransition(State<T> state);
    }
}
