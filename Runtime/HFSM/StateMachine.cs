using System;
using System.Collections.Generic;
using Buggestic.Runtime.Signals;

namespace Buggestic.Runtime.HFSM
{
    public class StateMachine<T> : State<T> where T : struct, Enum
    {
        private readonly Dictionary<T, State<T>> _stateLookup = new();

        private readonly T _defaultStateId;

        private State<T> _requestedTransitionState;

        public readonly Signal<T> OnEnterAnyChildStateSignal = new();

        public T CurrentStateId { get; private set; }
        public State<T> CurrentState { get; private set; }

        internal bool IsInvoking { get; set; }

        // Create
        public StateMachine(T defaultStateId) : this(null, defaultStateId, null, null, null, null)
        {
        }

        private StateMachine(T? stateId,
            T defaultStateId,
            StateMachine<T> parent,
            Action<State<T>> onStateEnter,
            Action<State<T>, T?> onStateExit,
            Action<State<T>> onStateUpdate) : base(stateId, parent, onStateEnter, onStateExit, onStateUpdate)
        {
            _defaultStateId = defaultStateId;
            CurrentStateId = _defaultStateId;
        }

        public StateMachine<T> AddStateMachine(T stateId,
            T defaultStateId,
            Action<State<T>> onStateEnter,
            Action<State<T>, T?> onStateExit,
            Action<State<T>> onStateUpdate)
        {
            StateMachine<T> stateMachine = new(stateId, defaultStateId, this, onStateEnter, onStateExit, onStateUpdate);
            _stateLookup.Add(stateId, stateMachine);
            return stateMachine;
        }

        public void AddState(T stateId, Action<State<T>> onStateEnter, Action<State<T>, T?> onStateExit, Action<State<T>> onStateUpdate)
        {
            _stateLookup.Add(stateId, new State<T>(stateId, this, onStateEnter, onStateExit, onStateUpdate));
        }

        public void AddTimeTransition(T fromStateId, T toStateId, float transitionTime)
        {
            AddTransition(fromStateId, new TimeTransition<T>(toStateId, transitionTime));
        }

        public void AddConditionalTransition(T fromStateId, T toStateId, Func<bool> condition)
        {
            AddTransition(fromStateId, new ConditionalTransition<T>(toStateId, condition));
        }

        public void AddTransition(T fromStateId, TransitionBase<T> transition)
        {
            _stateLookup[fromStateId].AddTransition(transition);
        }

        // Lifecycle public API
        public void Enter()
        {
            OnEnterState();
        }

        public void Exit()
        {
            OnExitState(null);
        }

        public void Update()
        {
            OnUpdateState();
        }

        public override void RequestTransition(T stateId)
        {
            if (_stateLookup.TryGetValue(stateId, out _requestedTransitionState))
            {
                TryHandleTransitionRequest();
            }
            else
            {
                base.RequestTransition(stateId);
            }
        }

        internal bool TryHandleTransitionRequest()
        {
            if (_requestedTransitionState is null || IsInvoking)
            {
                return false;
            }

            State<T> requestedTransitionState = _requestedTransitionState;
            _requestedTransitionState = null;

            ChangeState(requestedTransitionState.StateId!.Value);
            return true;
        }

        // On Enter
        internal override bool OnEnterState()
        {
            bool hasTransitioned = base.OnEnterState();
            if (CurrentState is null)
            {
                SetState(_defaultStateId);
            }
            else
            {
                CurrentState.OnEnterState();
            }

            return hasTransitioned;
        }

        internal void OnEnterAnyChildState(T stateId)
        {
            OnEnterAnyChildStateSignal.Invoke(stateId);

            if (_hasParent)
            {
                _parent.OnEnterAnyChildState(stateId);
            }
        }

        // On Exit
        internal override bool OnExitState(T? nextStateId)
        {
            bool hasTransitioned = base.OnExitState(nextStateId);
            ClearState();

            return hasTransitioned;
        }

        // On Update
        internal override bool OnUpdateState()
        {
            bool hasTransitioned = base.OnUpdateState();
            CurrentState.OnUpdateState();

            return hasTransitioned;
        }

        // State Change 
        private void ChangeState(T stateId)
        {
            if (CurrentState.OnExitState(stateId))
            {
                return;
            }

            SetState(stateId);
        }

        private void SetState(T stateId)
        {
            CurrentStateId = stateId;
            CurrentState = _stateLookup[stateId];
            CurrentState.OnEnterState();
        }

        private void ClearState()
        {
            while (CurrentState.OnExitState(null))
            {
            }

            CurrentStateId = _defaultStateId;
            CurrentState = null;
        }

        // Debug
        public override string GetDebugActiveHierarchyPath()
        {
            if (_hasParent)
            {
                return CurrentState is not null ? $"{base.GetDebugActiveHierarchyPath()}/{CurrentState.GetDebugActiveHierarchyPath()}" : string.Empty;
            }

            return CurrentState is not null ? CurrentState.GetDebugActiveHierarchyPath() : string.Empty;
        }
    }
}
