using System;
using System.Collections.Generic;
using Buggestic.Runtime.Controllers;

namespace Buggestic.Runtime.HFSM
{
    public class State<T> where T : struct, Enum
    {
        private readonly T? _stateId;
        protected readonly StateMachine<T> _parent;
        protected readonly bool _hasParent;

        private readonly List<TransitionBase<T>> _transitions = new();

        public T? StateId => _stateId;

        public float ActivatedAtTime { get; private set; }
        public float ActiveTime => TimeCache.Time - ActivatedAtTime;

        private readonly Action<State<T>> _onStateEnterCallback;
        private readonly Action<State<T>, T?> _onStateExitCallback;
        private readonly Action<State<T>> _onStateUpdateCallback;

        internal State(T? stateId,
            StateMachine<T> parent,
            Action<State<T>> onStateEnter,
            Action<State<T>, T?> onStateExit,
            Action<State<T>> onStateUpdate)
        {
            _stateId = stateId;
            _parent = parent;
            _hasParent = parent != null;
            _onStateEnterCallback = onStateEnter;
            _onStateExitCallback = onStateExit;
            _onStateUpdateCallback = onStateUpdate;
        }

        internal void AddTransition(TransitionBase<T> transition)
        {
            _transitions.Add(transition);
        }

        public virtual void RequestTransition(T stateId)
        {
            _parent.RequestTransition(stateId);
        }

        internal virtual bool OnEnterState()
        {
            ActivatedAtTime = TimeCache.Time;

            if (_onStateEnterCallback != null)
            {
                _parent.IsInvoking = true;
                _onStateEnterCallback.Invoke(this);
                _parent.IsInvoking = false;

                _parent.OnEnterAnyChildState(_stateId!.Value);

                return _parent.TryHandleTransitionRequest();
            }

            if (_hasParent)
            {
                _parent.OnEnterAnyChildState(_stateId!.Value);
            }

            return false;
        }

        internal virtual bool OnExitState(T? nextStateId)
        {
            if (_onStateExitCallback != null)
            {
                _parent.IsInvoking = true;
                _onStateExitCallback.Invoke(this, nextStateId);
                _parent.IsInvoking = false;

                return _parent.TryHandleTransitionRequest();
            }

            return false;
        }

        internal virtual bool OnUpdateState()
        {
            if (_onStateUpdateCallback != null)
            {
                _parent.IsInvoking = true;
                _onStateUpdateCallback.Invoke(this);
                _parent.IsInvoking = false;

                return _parent.TryHandleTransitionRequest() || TryTransition();
            }

            return TryTransition();
        }

        private bool TryTransition()
        {
            foreach (TransitionBase<T> transition in _transitions)
            {
                if (transition.CanTransition(this))
                {
                    _parent.RequestTransition(transition.ToStateId);
                    return true;
                }
            }

            return false;
        }

        public virtual string GetDebugActiveHierarchyPath()
        {
            return _stateId.ToString();
        }
    }
}
