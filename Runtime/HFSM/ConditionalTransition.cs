using System;

namespace Buggestic.Runtime.HFSM
{
    public class ConditionalTransition<T> : TransitionBase<T> where T : struct, Enum
    {
        private readonly Func<bool> _condition;

        public ConditionalTransition(T toStateId, Func<bool> condition) : base(toStateId)
        {
            _condition = condition;
        }

        public override bool CanTransition(State<T> state)
        {
            return _condition();
        }
    }
}
