namespace Buggestic.Runtime.Pooling
{
    public interface IOnValidateDependency
    {
        // ReSharper disable once InconsistentNaming
        public string name { get; }

        public void OnValidate();
    }
}
