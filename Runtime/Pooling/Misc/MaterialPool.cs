﻿using System.Collections.Generic;
using Buggestic.Runtime.Pooling.Pool_Manager;
using UnityEngine;

namespace Buggestic.Runtime.Pooling.Misc
{
    public class MaterialPool : CachedMonoBehaviour
    {
        private readonly Dictionary<Material, Stack<Material>> _materialPools = new();
        private readonly Dictionary<Material, Material> _sourceMaterialsByCopies = new();

#if UNITY_EDITOR
        private readonly Dictionary<Material, int> _materialTotalCounts = new();

        public Dictionary<Material, Stack<Material>>.KeyCollection SourceMaterials => _materialPools.Keys;

        public int GetTotalCount(Material sourceMaterial)
        {
            return _materialTotalCounts[sourceMaterial];
        }

        public int GetActiveCount(Material sourceMaterial)
        {
            return GetTotalCount(sourceMaterial) - GetInactiveCount(sourceMaterial);
        }

        public int GetInactiveCount(Material sourceMaterial)
        {
            return _materialPools[sourceMaterial].Count;
        }
#endif

        public Material Get(Material sourceMaterial)
        {
            if (_materialPools.TryGetValue(sourceMaterial, out Stack<Material> materialPool))
            {
                if (materialPool.Count > 0)
                {
                    return materialPool.Pop();
                }
            }
            else
            {
                Stack<Material> newMaterialPool = new();
                _materialPools.Add(sourceMaterial, newMaterialPool);

#if UNITY_EDITOR
                _materialTotalCounts.Add(sourceMaterial, 0);
#endif
            }

#if UNITY_EDITOR
            _materialTotalCounts[sourceMaterial]++;
#endif

            Material material = new(sourceMaterial);
            _sourceMaterialsByCopies.Add(material, sourceMaterial);
            return material;
        }

        public Material Release(Material material)
        {
            Material sourceMaterial = _sourceMaterialsByCopies[material];
            _materialPools[sourceMaterial].Push(material);
            return sourceMaterial;
        }
    }
}
