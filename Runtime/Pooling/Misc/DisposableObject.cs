﻿using System;

namespace Buggestic.Runtime.Pooling.Misc
{
    public readonly struct DisposableObject<T> : IDisposable
    {
        private readonly T _targetObject;
        private readonly Action<T> _disposeAction;

        public DisposableObject(T targetObject, Action<T> disposeAction)
        {
            _targetObject = targetObject;
            _disposeAction = disposeAction;
        }

        public void Dispose()
        {
            _disposeAction(_targetObject);
        }
    }
}
