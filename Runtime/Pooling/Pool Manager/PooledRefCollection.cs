using System.Collections.Generic;
using Buggestic.Runtime.Signals;

namespace Buggestic.Runtime.Pooling.Pool_Manager
{
    public class PooledRefCollection<T> where T : PooledObject
    {
        protected readonly Observer<PooledMonoBehaviour> _onTargetDespawnedObserverAutoCleanUp;

        public HashSet<T> Targets { get; } = new();
        public bool HasTargets => Targets.Count > 0;

        public PooledRefCollection()
        {
            _onTargetDespawnedObserverAutoCleanUp = new Observer<PooledMonoBehaviour>(target => Targets.Remove((T)target));
        }

        public virtual void AddTarget(T target)
        {
            if (Targets.Add(target))
            {
                target.OnDespawnedSignal_AutoCleanUp.AddObserver(_onTargetDespawnedObserverAutoCleanUp);
            }
        }

        public virtual void RemoveTarget(T target)
        {
            if (Targets.Remove(target))
            {
                target.OnDespawnedSignal_AutoCleanUp.RemoveObserver(_onTargetDespawnedObserverAutoCleanUp);
            }
        }

        public virtual void RemoveAllTargets()
        {
            foreach (T target in Targets)
            {
                target.OnDespawnedSignal_AutoCleanUp.RemoveObserver(_onTargetDespawnedObserverAutoCleanUp);
            }

            Targets.Clear();
        }
    }
}
