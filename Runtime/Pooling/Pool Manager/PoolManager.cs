﻿using System;
using System.Collections.Generic;
using Buggestic.Runtime.Controllers;
using Buggestic.Runtime.Pooling.Pool_Manager.Pooled_Signal;
using Buggestic.Runtime.Signals;
using Buggestic.Runtime.Utils;
using UnityEngine;

namespace Buggestic.Runtime.Pooling.Pool_Manager
{
    public class PoolManager : CachedMonoBehaviour
    {
        private const int INITIAL_POOL_CAPACITY = 128;

        private readonly Dictionary<int, HashSet<PooledObject>> _inactiveObjectsByPoolIds = new(INITIAL_POOL_CAPACITY);
        private readonly Dictionary<int, HashSet<PooledObject>> _activeObjectsByPoolIds = new(INITIAL_POOL_CAPACITY);

        private readonly PooledUpdateSignal _pooledUpdateSignal = new(INITIAL_POOL_CAPACITY);
        private readonly PooledPeriodicUpdateSignal _pooledPeriodicUpdateSignal =
            new(PeriodicUpdateSignal.DEFAULT_PERIODIC_UPDATE_RATE, INITIAL_POOL_CAPACITY);

        private readonly Dictionary<PooledObject, DelayedDespawnData> _delayedDespawnDataByObjects = new(INITIAL_POOL_CAPACITY);
        private readonly List<PooledObject> _pooledObjectsToDespawn = new(INITIAL_POOL_CAPACITY);
        private readonly HashSet<PooledObject> _internalPooledObjectsToDespawn = new(INITIAL_POOL_CAPACITY);

        private Transform _inactiveParentTransform;
        private Transform _inactiveParentTransformUI;
        private Canvas _inactiveParentUICanvas;
        private float _nextDespawnTime;

        public float PeriodicUpdateRate
        {
            get => _pooledPeriodicUpdateSignal.UpdateRate;
            set => _pooledPeriodicUpdateSignal.SetUpdateRate(value);
        }

#if UNITY_EDITOR
        public readonly Dictionary<int, string> PoolNamesByPoolIds = new(INITIAL_POOL_CAPACITY);

        public bool IsPoolingDisabled_EditorDebug { get; set; }
#endif

        protected override void AwakeImpl()
        {
            base.AwakeImpl();

            GameObject inactiveParentGO = new("Inactive Parent");
            _inactiveParentTransform = inactiveParentGO.transform;
            _inactiveParentTransform.SetParent(transform);
            inactiveParentGO.SetActive(false);

            GameObject inactiveParentCanvasGO = new("Inactive Parent UI Canvas");
            _inactiveParentUICanvas = inactiveParentCanvasGO.AddComponent<Canvas>();
            _inactiveParentUICanvas.enabled = false;
            _inactiveParentUICanvas.renderMode = RenderMode.ScreenSpaceOverlay;
            _inactiveParentUICanvas.vertexColorAlwaysGammaSpace = true;
            _inactiveParentTransformUI = inactiveParentCanvasGO.transform;
            _inactiveParentTransformUI.SetParent(transform);
        }

        private void Update()
        {
            if (TimeCache.Time >= _nextDespawnTime)
            {
                _nextDespawnTime = float.MaxValue;

                foreach (KeyValuePair<PooledObject, DelayedDespawnData> despawnTimeByObject in _delayedDespawnDataByObjects)
                {
                    if (TimeCache.Time >= despawnTimeByObject.Value.DespawnTime)
                    {
                        if (TimeCache.FrameCount >= despawnTimeByObject.Value.MinDespawnFrame)
                        {
                            _pooledObjectsToDespawn.Add(despawnTimeByObject.Key);
                        }
                        else
                        {
                            _nextDespawnTime = 0f;
                        }
                    }
                    else if (_nextDespawnTime > despawnTimeByObject.Value.DespawnTime)
                    {
                        _nextDespawnTime = despawnTimeByObject.Value.DespawnTime;
                    }
                }

                foreach (PooledObject pooledInstance in _pooledObjectsToDespawn)
                {
                    DespawnInstant(pooledInstance);
                }

                _pooledObjectsToDespawn.Clear();
            }

            if (_internalPooledObjectsToDespawn.Count > 0)
            {
                foreach (PooledObject pooledInstance in _internalPooledObjectsToDespawn)
                {
                    DespawnInstant(pooledInstance);
                }

                _internalPooledObjectsToDespawn.Clear();
            }

            _pooledUpdateSignal.Invoke();
            _pooledPeriodicUpdateSignal.Invoke();
        }

        public int GetPoolIdPrefab(PooledObject pooledPrefab)
        {
            return pooledPrefab.GetInstanceID();
        }

        public T Spawn<T>(T pooledPrefab, Transform parent = null, bool worldPositionStays = true, Action<T> onBeforeEnableInit = null)
            where T : PooledObject
        {
            return Spawn(pooledPrefab, Vector3.zero, Quaternion.identity, parent, worldPositionStays, onBeforeEnableInit);
        }

        public T Spawn<T>(T pooledPrefab,
            Vector3 position,
            Quaternion rotation,
            Transform parent = null,
            bool worldPositionStays = true,
            Action<T> onBeforeEnableInit = null) where T : PooledObject
        {
#if UNITY_EDITOR
            if (!pooledPrefab.gameObject.IsPrefab())
            {
                throw new ArgumentException("Only prefab spawning allowed");
            }
#endif

            int poolId = GetPoolIdPrefab(pooledPrefab);

            T pooledInstance;
            if (_inactiveObjectsByPoolIds.TryGetValue(poolId, out HashSet<PooledObject> inactivePool))
            {
                if (inactivePool.Count > 0)
                {
                    pooledInstance = (T)inactivePool.GetFirst();
                    pooledInstance.transform.SetPositionAndRotation(position, rotation);
                }
                else
                {
                    pooledInstance = Create(poolId, pooledPrefab, position, rotation);
                }
            }
            else
            {
                pooledInstance = Create(poolId, pooledPrefab, position, rotation);
            }

            pooledInstance.transform.SetParent(parent, worldPositionStays);
            onBeforeEnableInit?.Invoke(pooledInstance);
            UpdateObjectActiveState(poolId, pooledInstance, true);

            if (pooledInstance.HasPooledUpdate)
            {
                _pooledUpdateSignal.RegisterPooledUpdate(pooledInstance);
            }

            if (pooledInstance.HasPeriodicPooledUpdate)
            {
                _pooledPeriodicUpdateSignal.RegisterPooledUpdate(pooledInstance);
            }

            pooledInstance.Spawn();
            pooledInstance.IsSpawned = true;

            return pooledInstance;
        }

        public void Despawn(PooledObject pooledInstance)
        {
            if (BuggesticCore.IsApplicationQuitting)
            {
                return;
            }

            DespawnInstant(pooledInstance);
        }

        public void DespawnDelayed(PooledObject pooledInstance,
            float delay,
            int minFramesDelay = 1,
            DespawnDelayedType despawnDelayedType = DespawnDelayedType.Lowest)
        {
            float despawnTime = TimeCache.Time + delay;
            int minDespawnFrame = TimeCache.FrameCount + minFramesDelay;
            if (_nextDespawnTime > despawnTime)
            {
                _nextDespawnTime = despawnTime;
            }

            switch (despawnDelayedType)
            {
                case DespawnDelayedType.Lowest:
                    if (_delayedDespawnDataByObjects.TryGetValue(pooledInstance, out DelayedDespawnData delayedDespawnData))
                    {
                        despawnTime = Mathf.Min(despawnTime, delayedDespawnData.DespawnTime);
                        minDespawnFrame = Mathf.Min(minDespawnFrame, delayedDespawnData.MinDespawnFrame);
                    }

                    break;

                case DespawnDelayedType.Newest:
                    break;

                default:
                    throw new System.ComponentModel.InvalidEnumArgumentException(nameof(despawnDelayedType),
                        (int)despawnDelayedType,
                        typeof(DespawnDelayedType));
            }

            _delayedDespawnDataByObjects[pooledInstance] = new DelayedDespawnData(despawnTime, minDespawnFrame);
        }

        private void DespawnInstant(PooledObject pooledInstance)
        {
            if (!pooledInstance.IsSpawned)
            {
                if (_inactiveObjectsByPoolIds.TryGetValue(pooledInstance.PoolId, out HashSet<PooledObject> inactivePool) &&
                    inactivePool.Contains(pooledInstance))
                {
                    Debug.LogError($"{pooledInstance.gameObject.GetHierarchyName()} already despawned", pooledInstance);
                    return;
                }

                // Trying to despawn an object while it's being spawned
                _internalPooledObjectsToDespawn.Add(pooledInstance);
                return;
            }

            _delayedDespawnDataByObjects.Remove(pooledInstance);

            if (pooledInstance.HasPooledUpdate)
            {
                _pooledUpdateSignal.DeregisterPooledUpdate(pooledInstance);
            }

            if (pooledInstance.HasPeriodicPooledUpdate)
            {
                _pooledPeriodicUpdateSignal.DeregisterPooledUpdate(pooledInstance);
            }

            pooledInstance.IsSpawned = false;
            pooledInstance.Despawn();

            UpdateObjectActiveState(pooledInstance.PoolId, pooledInstance, false);

#if UNITY_EDITOR
            if (IsPoolingDisabled_EditorDebug)
            {
                _inactiveObjectsByPoolIds[pooledInstance.PoolId].Remove(pooledInstance);
                Destroy(pooledInstance);
            }
#endif
        }

        public bool HasDespawnDelay(PooledObject pooledInstance)
        {
            return _delayedDespawnDataByObjects.ContainsKey(pooledInstance);
        }

        public bool TryGetDespawnDelayLeft(PooledObject pooledInstance, out float despawnDelayLeft)
        {
            if (_delayedDespawnDataByObjects.TryGetValue(pooledInstance, out DelayedDespawnData delayedDespawnData))
            {
                despawnDelayLeft = Mathf.Max(delayedDespawnData.DespawnTime - TimeCache.Time, 0f);
                return true;
            }

            despawnDelayLeft = 0f;
            return false;
        }

        public void RemoveDespawnDelay(PooledObject pooledInstance)
        {
            _delayedDespawnDataByObjects.Remove(pooledInstance);
        }

        public void ExpandPoolToSize(PooledObject pooledPrefab, int toSize)
        {
            int poolId = GetPoolIdPrefab(pooledPrefab);
            for (int i = GetTotalPoolSize(poolId); i < toSize; i++)
            {
                PooledObject spawnedObject = Create(poolId, pooledPrefab, Vector3.zero, Quaternion.identity, toSize);
                UpdateObjectActiveState(poolId, spawnedObject, false);
            }
        }

        public int GetActivePoolSize(PooledObject pooledPrefab)
        {
            return GetActivePoolSize(GetPoolIdPrefab(pooledPrefab));
        }

        public int GetActivePoolSize(int poolId)
        {
            return _activeObjectsByPoolIds.TryGetValue(poolId, out HashSet<PooledObject> activePool) ? activePool.Count : 0;
        }

        public int GetInactivePoolSize(PooledObject pooledPrefab)
        {
            return GetInactivePoolSize(GetPoolIdPrefab(pooledPrefab));
        }

        public int GetInactivePoolSize(int poolId)
        {
            return _inactiveObjectsByPoolIds.TryGetValue(poolId, out HashSet<PooledObject> inactivePool) ? inactivePool.Count : 0;
        }

        public int GetTotalPoolSize(PooledObject pooledPrefab)
        {
            return GetTotalPoolSize(GetPoolIdPrefab(pooledPrefab));
        }

        public int GetTotalPoolSize(int poolId)
        {
            return GetActivePoolSize(poolId) + GetInactivePoolSize(poolId);
        }

        public void GetAllActiveInstances(PooledObject pooledPrefab, List<PooledObject> activeObjects)
        {
            GetAllActiveInstances(GetPoolIdPrefab(pooledPrefab), activeObjects);
        }

        public void GetAllActiveInstances(int poolId, List<PooledObject> activeObjects)
        {
            if (_activeObjectsByPoolIds.TryGetValue(poolId, out HashSet<PooledObject> activePool))
            {
                activeObjects.AddRange(activePool);
            }
        }

        private T Create<T>(int poolId, T pooledPrefab, Vector3 position, Quaternion rotation, int poolCapacity = 1) where T : PooledObject
        {
            T pooledInstance = Instantiate(pooledPrefab, position, rotation, _inactiveParentTransform);
            pooledInstance.PoolId = poolId;
            pooledInstance.gameObject.SetActive(false);

#if UNITY_EDITOR
            if (!PoolNamesByPoolIds.ContainsKey(poolId))
            {
                PoolNamesByPoolIds.Add(poolId, pooledPrefab.name);
            }
#endif

            if (!_activeObjectsByPoolIds.ContainsKey(poolId))
            {
                _activeObjectsByPoolIds.Add(poolId, new HashSet<PooledObject>(poolCapacity));
                _inactiveObjectsByPoolIds.Add(poolId, new HashSet<PooledObject>(poolCapacity));
            }

            return pooledInstance;
        }

        private void UpdateObjectActiveState(int poolId, PooledObject pooledInstance, bool isActive)
        {
            if (isActive)
            {
                _inactiveObjectsByPoolIds[poolId].Remove(pooledInstance);
                _activeObjectsByPoolIds[poolId].Add(pooledInstance);

                pooledInstance.gameObject.SetActive(true);
            }
            else
            {
                _activeObjectsByPoolIds[poolId].Remove(pooledInstance);
                _inactiveObjectsByPoolIds[poolId].Add(pooledInstance);

                if (pooledInstance.UseUICanvasPool)
                {
                    pooledInstance.transform.SetParent(_inactiveParentTransformUI, false);
                }
                else
                {
                    pooledInstance.gameObject.SetActive(false);

#if UNITY_EDITOR
                    pooledInstance.transform.SetParent(_inactiveParentTransform, false);
#endif
                }
            }
        }
    }
}
