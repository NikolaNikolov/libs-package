namespace Buggestic.Runtime.Pooling.Pool_Manager.Pooled_Signal
{
    public interface IPooledPeriodicUpdate
    {
        void OnPooledPeriodicUpdate(float deltaTime);
    }
}
