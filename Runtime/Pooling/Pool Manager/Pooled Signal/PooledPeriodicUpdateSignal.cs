using Buggestic.Runtime.Signals;
using UnityEngine.Pool;

namespace Buggestic.Runtime.Pooling.Pool_Manager.Pooled_Signal
{
    internal class PooledPeriodicUpdateSignal : PooledUpdateSignalBase<PeriodicUpdateSignal>
    {
        public float UpdateRate { get; private set; }

        public PooledPeriodicUpdateSignal(float updateRate, int capacity) : base(capacity)
        {
            SetUpdateRate(updateRate);
        }

        public void SetUpdateRate(float updateRate)
        {
            UpdateRate = updateRate;
            foreach (PeriodicUpdateSignal signal in _poledUpdateSignalsByPoolIds.Values)
            {
                signal.SetUpdateRate(updateRate);
            }
        }

        protected override bool HasObserversOrPendingActions(PeriodicUpdateSignal signal)
        {
            return signal.HasObserversOrPendingActions;
        }

        protected override void AddObserver(PeriodicUpdateSignal signal, PooledObject pooledObject)
        {
            signal.AddObserver(pooledObject.OnPooledPeriodicUpdateObserver);
        }

        protected override void RemoveObserver(PeriodicUpdateSignal signal, PooledObject pooledObject)
        {
            signal.RemoveObserver(pooledObject.OnPooledPeriodicUpdateObserver);
        }

        protected override void EarlyMarkForInvocation(PeriodicUpdateSignal signal)
        {
            signal.EarlyMarkForInvocation();
        }

        protected override void Invoke(PeriodicUpdateSignal signal)
        {
            signal.Invoke();
        }

        protected override PeriodicUpdateSignal GetPooledSignal()
        {
            PeriodicUpdateSignal signal = GenericPool<PeriodicUpdateSignal>.Get();
            signal.SetUpdateRate(UpdateRate);
            return signal;
        }

        protected override void ReleasePooledSignal(PeriodicUpdateSignal signal)
        {
            GenericPool<PeriodicUpdateSignal>.Release(signal);
        }
    }
}
