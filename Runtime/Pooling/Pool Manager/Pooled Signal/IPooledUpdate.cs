namespace Buggestic.Runtime.Pooling.Pool_Manager.Pooled_Signal
{
    public interface IPooledUpdate
    {
        void OnPooledUpdate();
    }
}
