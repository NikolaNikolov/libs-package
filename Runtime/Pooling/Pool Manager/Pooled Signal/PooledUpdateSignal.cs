using Buggestic.Runtime.Signals;
using UnityEngine.Pool;

namespace Buggestic.Runtime.Pooling.Pool_Manager.Pooled_Signal
{
    internal class PooledUpdateSignal : PooledUpdateSignalBase<Signal>
    {
        public PooledUpdateSignal(int capacity) : base(capacity)
        {
        }

        protected override bool HasObserversOrPendingActions(Signal signal)
        {
            return signal.HasObserversOrPendingActions;
        }

        protected override void AddObserver(Signal signal, PooledObject pooledObject)
        {
            signal.AddObserver(pooledObject.OnPooledUpdateObserver);
        }

        protected override void RemoveObserver(Signal signal, PooledObject pooledObject)
        {
            signal.RemoveObserver(pooledObject.OnPooledUpdateObserver);
        }

        protected override void EarlyMarkForInvocation(Signal signal)
        {
            signal.EarlyMarkForInvocation();
        }

        protected override void Invoke(Signal signal)
        {
            signal.Invoke();
        }

        protected override Signal GetPooledSignal()
        {
            return GenericPool<Signal>.Get();
        }

        protected override void ReleasePooledSignal(Signal signal)
        {
            GenericPool<Signal>.Release(signal);
        }
    }
}
