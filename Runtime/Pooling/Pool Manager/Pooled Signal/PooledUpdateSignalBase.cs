using System.Collections.Generic;

namespace Buggestic.Runtime.Pooling.Pool_Manager.Pooled_Signal
{
    internal abstract class PooledUpdateSignalBase<T>
    {
        protected readonly Dictionary<int, T> _poledUpdateSignalsByPoolIds;
        private readonly HashSet<PooledObject> _pooledObjectsPendingAdd = new();
        private readonly HashSet<int> _emptyPoolIds = new();

        private bool _isInvoking;

        internal PooledUpdateSignalBase(int capacity)
        {
            _poledUpdateSignalsByPoolIds = new Dictionary<int, T>(capacity);
        }

        internal void Invoke()
        {
            _isInvoking = true;

            foreach (KeyValuePair<int, T> signalByPoolId in _poledUpdateSignalsByPoolIds)
            {
                if (HasObserversOrPendingActions(signalByPoolId.Value))
                {
                    // Prepare for invocation
                    EarlyMarkForInvocation(signalByPoolId.Value);
                }
                else
                {
                    // Mark empty
                    _emptyPoolIds.Add(signalByPoolId.Key);
                }
            }

            // Clear empty
            if (_emptyPoolIds.Count > 0)
            {
                foreach (int emptyPoolId in _emptyPoolIds)
                {
                    ReleasePooledSignal(_poledUpdateSignalsByPoolIds[emptyPoolId]);
                    _poledUpdateSignalsByPoolIds.Remove(emptyPoolId);
                }

                _emptyPoolIds.Clear();
            }

            // Invoke
            foreach (T signal in _poledUpdateSignalsByPoolIds.Values)
            {
                Invoke(signal);
            }

            _isInvoking = false;

            // Add pending
            if (_pooledObjectsPendingAdd.Count > 0)
            {
                foreach (PooledObject pooledObject in _pooledObjectsPendingAdd)
                {
                    RegisterPooledUpdate(pooledObject);
                }

                _pooledObjectsPendingAdd.Clear();
            }
        }

        internal void RegisterPooledUpdate(PooledObject pooledObject)
        {
            if (!_poledUpdateSignalsByPoolIds.TryGetValue(pooledObject.PoolId, out T signal))
            {
                if (_isInvoking)
                {
                    _pooledObjectsPendingAdd.Add(pooledObject);
                    return;
                }

                signal = GetPooledSignal();
                _poledUpdateSignalsByPoolIds.Add(pooledObject.PoolId, signal);
            }

            AddObserver(signal, pooledObject);
        }

        internal void DeregisterPooledUpdate(PooledObject pooledObject)
        {
            if (_pooledObjectsPendingAdd.Count > 0)
            {
                _pooledObjectsPendingAdd.Remove(pooledObject);
            }

            if (_poledUpdateSignalsByPoolIds.TryGetValue(pooledObject.PoolId, out T signal))
            {
                RemoveObserver(signal, pooledObject);
            }
        }

        protected abstract bool HasObserversOrPendingActions(T signal);
        protected abstract void AddObserver(T signal, PooledObject pooledObject);
        protected abstract void RemoveObserver(T signal, PooledObject pooledObject);
        protected abstract void EarlyMarkForInvocation(T signal);
        protected abstract void Invoke(T signal);
        protected abstract T GetPooledSignal();
        protected abstract void ReleasePooledSignal(T signal);
    }
}
