using Buggestic.Runtime.Signals;

namespace Buggestic.Runtime.Pooling.Pool_Manager
{
    public abstract class PooledMonoBehaviour : CachedMonoBehaviour
    {
        public readonly Signal<PooledMonoBehaviour> OnDespawnedSignal_AutoCleanUp = new();

        public virtual void OnSpawn()
        {
        }

        public virtual void OnDespawn()
        {
        }
    }
}
