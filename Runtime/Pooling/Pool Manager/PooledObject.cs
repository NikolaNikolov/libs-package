using Buggestic.Runtime.Pooling.Pool_Manager.Pooled_Signal;
using Buggestic.Runtime.Signals;
using Buggestic.Runtime.Validation;
using UnityEngine;

namespace Buggestic.Runtime.Pooling.Pool_Manager
{
    public class PooledObject : PooledMonoBehaviour
    {
        [SerializeField, HideInInspector] private bool _hasPooledUpdate;
        [SerializeField, HideInInspector] private bool _hasPeriodicPooledUpdate;
        [SerializeField, HideInInspector] private PooledMonoBehaviour[] _pooledChildren;

        public bool HasPooledUpdate => _hasPooledUpdate;
        public bool HasPeriodicPooledUpdate => _hasPeriodicPooledUpdate;

        internal Observer OnPooledUpdateObserver { get; private set; }
        internal Observer<float> OnPooledPeriodicUpdateObserver { get; private set; }

        public int PoolId { get; internal set; }
        public bool IsSpawned { get; internal set; }
        public virtual bool UseUICanvasPool => false;

        internal override void OnValidateInternal(OnValidateChangeScope changeScope)
        {
            base.OnValidateInternal(changeScope);

            // ReSharper disable SuspiciousTypeConversion.Global
            changeScope.Assign(ref _hasPooledUpdate, this is IPooledUpdate);
            changeScope.Assign(ref _hasPeriodicPooledUpdate, this is IPooledPeriodicUpdate);
            // ReSharper restore SuspiciousTypeConversion.Global

            changeScope.GetComponentsInChildren(ref _pooledChildren, true);
        }

        internal override void AwakeInternal()
        {
            base.AwakeInternal();

            if (_hasPooledUpdate)
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                OnPooledUpdateObserver = new Observer(((IPooledUpdate)this).OnPooledUpdate);
            }

            if (_hasPeriodicPooledUpdate)
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                OnPooledPeriodicUpdateObserver = new Observer<float>(((IPooledPeriodicUpdate)this).OnPooledPeriodicUpdate);
            }
        }

        internal void Spawn()
        {
            foreach (PooledMonoBehaviour child in _pooledChildren)
            {
                child.OnSpawn();
            }
        }

        internal void Despawn()
        {
            foreach (PooledMonoBehaviour child in _pooledChildren)
            {
                child.OnDespawnedSignal_AutoCleanUp.Invoke(this);
                child.OnDespawnedSignal_AutoCleanUp.RemoveAllObservers();
                child.OnDespawn();
            }
        }
    }
}
