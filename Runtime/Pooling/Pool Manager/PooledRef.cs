using Buggestic.Runtime.Signals;

namespace Buggestic.Runtime.Pooling.Pool_Manager
{
    public class PooledRef<T> where T : PooledObject
    {
        protected T _target;

        protected readonly Observer<PooledMonoBehaviour> _onTargetDespawnedObserver_AutoCleanUp;

        public PooledRef()
        {
            _onTargetDespawnedObserver_AutoCleanUp = new Observer<PooledMonoBehaviour>(_ =>
            {
                HasTarget = false;
                _target = null;
            });
        }

        public bool HasTarget { get; protected set; }

        public virtual T Target
        {
            get => _target;
            set
            {
                if (_target == value)
                {
                    return;
                }

                if (HasTarget)
                {
                    _target.OnDespawnedSignal_AutoCleanUp.RemoveObserver(_onTargetDespawnedObserver_AutoCleanUp);
                }

                if (value is not null)
                {
                    HasTarget = true;
                    _target = value;
                    _target.OnDespawnedSignal_AutoCleanUp.AddObserver(_onTargetDespawnedObserver_AutoCleanUp);
                }
                else
                {
                    HasTarget = false;
                    _target = null;
                }
            }
        }
    }
}
