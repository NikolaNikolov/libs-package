namespace Buggestic.Runtime.Pooling.Pool_Manager
{
    public struct DelayedDespawnData
    {
        public float DespawnTime { get; private set; }
        public int MinDespawnFrame { get; private set; }

        public DelayedDespawnData(float despawnTime, int minDespawnFrame)
        {
            DespawnTime = despawnTime;
            MinDespawnFrame = minDespawnFrame;
        }
    }
}
