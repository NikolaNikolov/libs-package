using System.Collections.Generic;
using Buggestic.Runtime.Utils;
using Buggestic.Runtime.Validation;
using UnityEngine;
using UnityEngine.Pool;

namespace Buggestic.Runtime.Pooling.Pool_Manager
{
    public class CachedMonoBehaviour : MonoBehaviour, IOnValidateDependency
    {
        // ReSharper disable InconsistentNaming
        [HideInInspector] public new GameObject gameObject;
        [HideInInspector] public new Transform transform;
        // ReSharper restore InconsistentNaming

        internal static readonly Dictionary<IOnValidateDependency, HashSet<IOnValidateDependency>> ON_VALIDATE_DEPENDENCIES = new();

#if UNITY_EDITOR
        [UnityEditor.InitializeOnLoadMethod]
        private static void OnRuntimeInitialized()
        {
            UnityEditor.Selection.selectionChanged -= OnSelectionChanged_Editor;
            UnityEditor.Selection.selectionChanged += OnSelectionChanged_Editor;
        }

        private static void OnSelectionChanged_Editor()
        {
            using (ListPool<IOnValidateDependency>.Get(out List<IOnValidateDependency> onValidateObjects))
            {
                foreach (GameObject selectedGameObject in UnityEditor.Selection.gameObjects)
                {
                    if (Application.isPlaying && !selectedGameObject.IsPrefab())
                    {
                        continue;
                    }

                    selectedGameObject.GetComponents(onValidateObjects);
                    foreach (IOnValidateDependency onValidateObject in onValidateObjects)
                    {
                        onValidateObject.OnValidate();
                    }
                }
            }
        }
#endif

        public void OnValidate()
        {
#if UNITY_EDITOR
            EditorUtils.ExecuteAfterAssetDatabaseUpdate(OnValidateLogic, this);
#else
            OnValidateLogic();
#endif
        }

        private void OnValidateLogic()
        {
            if (this == null)
            {
                return;
            }

            using OnValidateChangeScope changeScope = OnValidateChangeScope.Get(this);

            changeScope.Assign(ref gameObject, base.gameObject);
            changeScope.Assign(ref transform, base.transform);

            OnValidateInternal(changeScope);
            OnValidateImpl(changeScope);

            TriggerOnValidateDependencies(this);
        }

        internal virtual void OnValidateInternal(OnValidateChangeScope changeScope)
        {
        }

        protected virtual void OnValidateImpl(OnValidateChangeScope changeScope)
        {
        }

        protected void Awake()
        {
            AwakeInternal();
            AwakeImpl();
        }

        internal virtual void AwakeInternal()
        {
        }

        protected virtual void AwakeImpl()
        {
        }

        public static void RegisterOnValidateDependency(IOnValidateDependency source, IOnValidateDependency target)
        {
            if (source == null || target == null)
            {
                return;
            }

            if (ON_VALIDATE_DEPENDENCIES.TryGetValue(source, out HashSet<IOnValidateDependency> dependencies))
            {
                if (dependencies.Contains(target))
                {
                    Debug.LogError(
                        $"Two-way validation dependency between {source.name} ({source.GetType()}) and {target.name} ({target.GetType()}) is not supported");
                    return;
                }
            }

            if (!ON_VALIDATE_DEPENDENCIES.TryGetValue(target, out dependencies))
            {
                dependencies = new HashSet<IOnValidateDependency>();
                ON_VALIDATE_DEPENDENCIES.Add(target, dependencies);
            }

            dependencies.Add(source);
        }

        internal static void TriggerOnValidateDependencies(IOnValidateDependency target)
        {
            if (ON_VALIDATE_DEPENDENCIES.TryGetValue(target, out HashSet<IOnValidateDependency> dependencies))
            {
                foreach (IOnValidateDependency dependency in dependencies)
                {
                    if (!dependency.Equals(null))
                    {
                        dependency.OnValidate();
                    }
                }
            }
        }
    }
}
