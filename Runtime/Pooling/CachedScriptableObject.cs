using Buggestic.Runtime.Pooling.Pool_Manager;
using Buggestic.Runtime.Utils;
using Buggestic.Runtime.Validation;
using UnityEngine;

namespace Buggestic.Runtime.Pooling
{
    public class CachedScriptableObject : ScriptableObject, IOnValidateDependency
    {
        public void OnValidate()
        {
#if UNITY_EDITOR
            EditorUtils.ExecuteAfterAssetDatabaseUpdate(OnValidateLogic, this);
#else
            OnValidateLogic();
#endif
        }

        private void OnValidateLogic()
        {
            if (this == null)
            {
                return;
            }

            using OnValidateChangeScope changeScope = OnValidateChangeScope.Get(this);

            OnValidateImpl(changeScope);

            CachedMonoBehaviour.TriggerOnValidateDependencies(this);
        }

        protected virtual void OnValidateImpl(OnValidateChangeScope changeScope)
        {
        }

        public static void RegisterOnValidateDependency(IOnValidateDependency source, IOnValidateDependency target)
        {
            CachedMonoBehaviour.RegisterOnValidateDependency(source, target);
        }
    }
}
