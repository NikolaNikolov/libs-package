using System;
using System.Collections.Generic;
using Buggestic.Runtime.Pooling;
using Buggestic.Runtime.Utils;
using Buggestic.Runtime.Validation;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Buggestic.Runtime.Scriptable_Database
{
    public abstract class ScriptableDatabase<TEnum, TData> : CachedScriptableObject where TEnum : struct, Enum where TData : ScriptableData<TEnum>
    {
        [SerializeField] private bool _ignoreZeroEnumValue = true;

        [Space]
        [SerializeField, ListDrawerSettings(HideAddButton = true, HideRemoveButton = true, DraggableItems = false, DefaultExpandedState = true),
         InlineEditor, Searchable]
        private TData[] _data;

        private Dictionary<TEnum, TData> _dataLookup;

        public IReadOnlyDictionary<TEnum, TData> DataLookup
        {
            get
            {
                if (_dataLookup == null)
                {
                    InitializeData();
                }

                return _dataLookup;
            }
        }

        protected override void OnValidateImpl(OnValidateChangeScope changeScope)
        {
            base.OnValidateImpl(changeScope);

            TData[] oldData = _data;

            string folderPath = GeneralUtils.GetScriptableObjectFolderPath_Editor(this);
            GeneralUtils.LoadScriptableObjects_Editor(ref _data, null, folderPath);

            foreach (TData data in _data)
            {
                RegisterOnValidateDependency(this, data);
            }

            Array enumTypesArray = Enum.GetValues(typeof(TEnum));
            HashSet<TEnum> enumTypes = new(enumTypesArray.Length);
            foreach (TEnum enumType in enumTypesArray)
            {
                enumTypes.Add(enumType);
            }

            foreach (TData data in _data)
            {
                if (Enum.IsDefined(typeof(TEnum), data.EnumType))
                {
                    if (!enumTypes.Remove(data.EnumType))
                    {
                        Debug.LogError($"{name} has duplicated data for enum type: {data.EnumType}", data);
                    }
                }
                else
                {
                    Debug.LogError($"{name} has undefined enum type: {data.EnumType}", data);
                }
            }

            foreach (TEnum enumType in enumTypes)
            {
                int enumValue = (int)(enumType as object);
                if (_ignoreZeroEnumValue && enumValue == 0)
                {
                    continue;
                }

                Debug.LogError($"{name} has missing enum type: {enumType}", this);
            }

            if (HasDataChanged(oldData))
            {
                _dataLookup = null;
                changeScope.ForceChanged();
            }
        }

        private bool HasDataChanged(TData[] oldData)
        {
            if (_data.Length != oldData.Length)
            {
                return true;
            }

            for (int i = 0; i < _data.Length; i++)
            {
                if (_data[i] != oldData[i])
                {
                    return true;
                }
            }

            return false;
        }

        private void InitializeData()
        {
            _dataLookup = new Dictionary<TEnum, TData>(_data.Length);
            foreach (TData data in _data)
            {
                _dataLookup.Add(data.EnumType, data);
            }
        }

        public TData GetData(TEnum enumType)
        {
            if (_dataLookup == null)
            {
                InitializeData();
            }

            return _dataLookup![enumType];
        }

#if UNITY_EDITOR
        [Button, HideInPlayMode, PropertySpace]
        public void CreateMissingData()
        {
            Array enumTypes = Enum.GetValues(typeof(TEnum));
            foreach (TEnum enumType in enumTypes)
            {
                int enumValue = (int)(enumType as object);
                if (_ignoreZeroEnumValue && enumValue == 0)
                {
                    continue;
                }

                if (!DataLookup.ContainsKey(enumType))
                {
                    TData data = CreateInstance<TData>();
                    data.name = $"{name} - {enumType}";
                    data.EnumType = enumType;

                    string path = UnityEditor.AssetDatabase.GetAssetPath(this);
                    path = path.Replace(".asset", $" - {UnityEditor.ObjectNames.NicifyVariableName(enumType.ToString())}.asset");
                    UnityEditor.AssetDatabase.CreateAsset(data, path);
                    UnityEditor.AssetDatabase.SaveAssets();
                }
            }

            OnValidate();
        }
#endif
    }
}
