using System;
using Buggestic.Runtime.Pooling;
using UnityEngine;

namespace Buggestic.Runtime.Scriptable_Database
{
    public abstract class ScriptableData<TEnum> : CachedScriptableObject where TEnum : struct, Enum
    {
        [SerializeField] private TEnum _enumType;

        public TEnum EnumType
        {
            get => _enumType;
            internal set => _enumType = value;
        }
    }
}
