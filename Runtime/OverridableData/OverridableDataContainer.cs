using System;
using UnityEngine;

namespace Buggestic.Runtime.OverridableData
{
    [Serializable]
    public class OverridableDataContainer<T> where T : OverridableDataBase
    {
        [SerializeField] private T _baseData;

        private T _overrideData;

        public T BaseData => _baseData;
        public T Data => HasOverrideData ? _overrideData : _baseData;

        public bool HasOverrideData { get; private set; }

        public void OverrideData(T overrideData)
        {
            HasOverrideData = true;
            _overrideData = overrideData;
        }

        public void OverrideData(OverridableDataBase overrideDataBase)
        {
            if (overrideDataBase is T overrideData)
            {
                OverrideData(overrideData);
            }
        }

        public void ClearOverrideData()
        {
            if (HasOverrideData)
            {
                HasOverrideData = false;
                _overrideData = null;
            }
        }
    }
}
