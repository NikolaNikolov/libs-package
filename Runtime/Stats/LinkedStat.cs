using System;
using Buggestic.Runtime.Signals;

namespace Buggestic.Runtime.Stats
{
    [Serializable]
    public class LinkedStat : StatBase
    {
        private Func<float, Stat, float> _targetStatProcessor;

        private Observer<Stat> _onLinkedStatModifiedValueChangedObserver;

        public Stat TargetStat { get; private set; }

        public readonly Signal<LinkedStat> OnModifiedValueChangedSignal = new();

        public LinkedStat()
        {
            _onLinkedStatModifiedValueChangedObserver = new Observer<Stat>(OnLinkedStatModifiedValueChanged);
        }

        public LinkedStat(float baseValue) : base(baseValue)
        {
            _onLinkedStatModifiedValueChangedObserver = new Observer<Stat>(OnLinkedStatModifiedValueChanged);
        }

        public void LinkStat(Stat stat, Func<float, Stat, float> targetStatProcessor)
        {
            if (TargetStat != null)
            {
                UnlinkStat(TargetStat);
            }

            TargetStat = stat;
            TargetStat.OnModifiedValueChangedSignal.AddObserver(_onLinkedStatModifiedValueChangedObserver);
            _targetStatProcessor = targetStatProcessor;
        }

        public void UnlinkStat(Stat stat)
        {
            if (TargetStat == stat)
            {
                TargetStat.OnModifiedValueChangedSignal.RemoveObserver(_onLinkedStatModifiedValueChangedObserver);
                TargetStat = null;
                _targetStatProcessor = null;
            }
        }

        private void OnLinkedStatModifiedValueChanged(Stat stat)
        {
            OnModifiersChanged();
        }

        protected override void OnModifiersChanged()
        {
            base.OnModifiersChanged();

            OnModifiedValueChangedSignal.Invoke(this);
        }

        protected override void CalculateModifications()
        {
            _modifiedValue = _targetStatProcessor?.Invoke(BaseValue, TargetStat) ?? BaseValue;
            _modifiedValueType = GetModifiedValueType(_modifiedValue);
            _modifiedOverBaseValue = GetModifiedOverBaseValue();
        }
    }
}
