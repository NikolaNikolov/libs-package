using System;
using System.Globalization;
using UnityEngine;

namespace Buggestic.Runtime.Stats
{
    [Serializable]
    public abstract class StatBase
    {
#if UNITY_EDITOR
        public static class StatBase_Editor
        {
            public const string BASE_VALUE = nameof(_baseValue);
        }
#endif

        [SerializeField] private float _baseValue;

        private bool _isDirty = true;
        protected StatModifiedValueType _modifiedValueType;
        protected float _modifiedValue;
        protected float _modifiedOverBaseValue;

        public float BaseValue => _baseValue;
        public int BaseValueRoundToInt => Mathf.RoundToInt(BaseValue);

        public StatModifiedValueType ModifiedValueType
        {
            get
            {
                HandleDirty();
                return _modifiedValueType;
            }
        }

        public float ModifiedValue
        {
            get
            {
                HandleDirty();
                return _modifiedValue;
            }
        }

        public int ModifiedValueRoundToInt => Mathf.RoundToInt(ModifiedValue);

        public float ModifiedOverBaseValue
        {
            get
            {
                HandleDirty();
                return _modifiedOverBaseValue;
            }
        }

        protected StatBase()
        {
        }

        protected StatBase(float baseValue)
        {
            _baseValue = baseValue;
        }

        public void MarkDirty()
        {
            _isDirty = true;
        }

        protected virtual void OnModifiersChanged()
        {
            _isDirty = true;
        }

        protected void HandleDirty()
        {
            if (_isDirty)
            {
                _isDirty = false;
                CalculateModifications();
            }
        }

        protected abstract void CalculateModifications();

        protected StatModifiedValueType GetModifiedValueType(float modifiedValue)
        {
            if (Mathf.Approximately(modifiedValue, BaseValue))
            {
                return StatModifiedValueType.Unchanged;
            }

            return modifiedValue > BaseValue ? StatModifiedValueType.Increased : StatModifiedValueType.Decreased;
        }

        protected float GetModifiedOverBaseValue()
        {
            if (BaseValue == 0f)
            {
                return 0f;
            }

            return _modifiedValue / BaseValue;
        }

        public override string ToString()
        {
            return ModifiedValue.ToString(CultureInfo.InvariantCulture);
        }

        public static implicit operator float(StatBase stat) => stat.ModifiedValue;
        public static implicit operator int(StatBase stat) => stat.ModifiedValueRoundToInt;
    }
}
