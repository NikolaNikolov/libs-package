namespace Buggestic.Runtime.Stats
{
    public enum StatModifiedValueType
    {
        Unchanged,
        Increased,
        Decreased,
    }
}
