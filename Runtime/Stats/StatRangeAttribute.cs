using System;

namespace Buggestic.Runtime.Stats
{
    [AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
    public class StatRangeAttribute : Attribute
    {
        public readonly float Min;
        public readonly float Max;

        public StatRangeAttribute(float min, float max)
        {
            Min = min;
            Max = max;
        }
    }
}
