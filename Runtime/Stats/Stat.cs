using System;
using System.Collections.Generic;
using Buggestic.Runtime.Signals;
using UnityEngine;

namespace Buggestic.Runtime.Stats
{
    [Serializable]
    public class Stat : StatBase
    {
        private float _unprocessedModifiedValue;
        private Func<float, float> _modifiedValueProcessor;

        private readonly Dictionary<StatModifier, int> _modifiers = new();

        public readonly Signal<Stat> OnModifiedValueChangedSignal = new();

        public float UnprocessedModifiedValue
        {
            get
            {
                HandleDirty();
                return _unprocessedModifiedValue;
            }
        }

        public int UnprocessedModifiedValueRoundToInt => Mathf.RoundToInt(UnprocessedModifiedValue);

        public Stat()
        {
        }

        public Stat(float baseValue) : base(baseValue)
        {
        }

        public void InitializeModifiedValueProcessor(Func<float, float> modifiedValueProcessor)
        {
            _modifiedValueProcessor = modifiedValueProcessor;
        }

        public void AddModifier(StatModifier modifier, int count = 1)
        {
            if (count <= 0)
            {
                return;
            }

            if (_modifiers.TryGetValue(modifier, out int currentCount))
            {
                _modifiers[modifier] = currentCount + count;
            }
            else
            {
                _modifiers.Add(modifier, count);
            }

            OnModifiersChanged();
        }

        public void RemoveModifier(StatModifier modifier, int count = 1)
        {
            if (count <= 0)
            {
                return;
            }

            if (_modifiers.TryGetValue(modifier, out int currentCount))
            {
                int newCount = currentCount - count;
                if (newCount > 0)
                {
                    _modifiers[modifier] = newCount;
                }
                else
                {
                    _modifiers.Remove(modifier);
                }

                OnModifiersChanged();
            }
        }

        public void RemoveAllModifiers()
        {
            if (_modifiers.Count > 0)
            {
                _modifiers.Clear();
                OnModifiersChanged();
            }
        }

        protected override void OnModifiersChanged()
        {
            base.OnModifiersChanged();

            OnModifiedValueChangedSignal.Invoke(this);
        }

        protected override void CalculateModifications()
        {
            _unprocessedModifiedValue = ApplyModifiers(BaseValue);
            _modifiedValue = _modifiedValueProcessor?.Invoke(_unprocessedModifiedValue) ?? _unprocessedModifiedValue;
            _modifiedValueType = GetModifiedValueType(_unprocessedModifiedValue);
            _modifiedOverBaseValue = GetModifiedOverBaseValue();
        }

        private float ApplyModifiers(float baseValue)
        {
            float flatValue = baseValue;
            float additiveMultiplier = 0;
            float multiplicativeMultiplier = 1f;

            foreach ((StatModifier modifier, int count) in _modifiers)
            {
                switch (modifier.Type)
                {
                    case StatModifierType.Flat:
                        flatValue += modifier.Value * count;
                        break;

                    case StatModifierType.AdditiveMultiplier:
                        additiveMultiplier += modifier.Value * count;
                        break;

                    case StatModifierType.MultiplicativeMultiplier:
                        multiplicativeMultiplier *= Mathf.Pow((1f + modifier.Value), count);
                        break;

                    default:
                        throw new System.ComponentModel.InvalidEnumArgumentException(nameof(modifier.Type),
                            (int)modifier.Type,
                            typeof(StatModifierType));
                }
            }

            return flatValue * (1f + additiveMultiplier) * multiplicativeMultiplier;
        }
    }
}
