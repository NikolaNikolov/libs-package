using System;
using UnityEngine;

namespace Buggestic.Runtime.Stats
{
    [Serializable]
    public struct StatModifier : IEquatable<StatModifier>
    {
#if UNITY_EDITOR
        public static class StatModifier_Editor
        {
            public const string VALUE = nameof(_value);
            public const string TYPE = nameof(_type);
        }
#endif

        [SerializeField] private float _value;
        [SerializeField] private StatModifierType _type;

        public float Value => _value;
        public StatModifierType Type => _type;

        public StatModifiedValueType ModifiedValueType
        {
            get
            {
                return _value switch
                {
                    > 0f => StatModifiedValueType.Increased,
                    < 0f => StatModifiedValueType.Decreased,
                    _ => StatModifiedValueType.Unchanged
                };
            }
        }

        public StatModifier(float value, StatModifierType type) : this()
        {
            _value = value;
            _type = type;
        }

        public bool Equals(StatModifier other)
        {
            return _value.Equals(other._value) && _type == other._type;
        }

        public override bool Equals(object obj)
        {
            return obj is StatModifier other && Equals(other);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(_value, (int)_type);
        }

        public static bool operator ==(StatModifier left, StatModifier right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(StatModifier left, StatModifier right)
        {
            return !left.Equals(right);
        }
    }
}
