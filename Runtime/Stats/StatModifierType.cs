namespace Buggestic.Runtime.Stats
{
    public enum StatModifierType
    {
        Flat,
        AdditiveMultiplier,
        MultiplicativeMultiplier,
    }
}
