//#define ON_VALIDATE_DIRTY_DEBUG

using System;
using System.Collections.Generic;
using System.Linq;
using Buggestic.Runtime.Utils;
using UnityEngine;
using UnityEngine.Pool;
using Object = UnityEngine.Object;

namespace Buggestic.Runtime.Validation
{
    public class OnValidateChangeScope : IDisposable
    {
        private static readonly Dictionary<Object, OnValidateChangeScope> ACTIVE_CHANGE_SCOPES = new();

        private int _ownersCount;
        private Object _targetObject;
        private GameObject _targetGameObject;
        private bool _changed;

        public bool Changed
        {
            get => _changed;
            private set
            {
#if UNITY_EDITOR && ON_VALIDATE_DIRTY_DEBUG
                if (!_changed && value)
                {
                    Debug.Log(_targetGameObject != null
                            ? $"Validation Dirty: {_targetGameObject.GetHierarchyName()} | Type: {_targetObject.GetType()}"
                            : $"Validation Dirty: {_targetObject.name} | Type: {_targetObject.GetType()}",
                        _targetObject);
                }
#endif
                _changed = value;
            }
        }

        [Obsolete("Use static Get method instead.", true)]
        public OnValidateChangeScope()
        {
        }

        public static OnValidateChangeScope Get(Object targetObject)
        {
            if (ACTIVE_CHANGE_SCOPES.TryGetValue(targetObject, out OnValidateChangeScope changeScope))
            {
                changeScope._ownersCount++;
                return changeScope;
            }

            changeScope = GenericPool<OnValidateChangeScope>.Get();

            changeScope._ownersCount++;
            changeScope._targetObject = targetObject;
            changeScope._targetGameObject = targetObject switch
            {
                GameObject targetGameObject => targetGameObject,
                Component targetComponent => targetComponent.gameObject,
                _ => null
            };

            ACTIVE_CHANGE_SCOPES.Add(targetObject, changeScope);
            return changeScope;
        }

        public void ForceChanged()
        {
            Changed = true;
        }

        public void Assign<T>(ref T target, T value, IEqualityComparer<T> equalityComparer = null)
        {
            if (target is Object targetObject)
            {
                Object resultObject = value as Object;
                if (targetObject != resultObject)
                {
                    Changed = true;
                    target = value;
                }

                return;
            }

            equalityComparer ??= EqualityComparer<T>.Default;
            if (!equalityComparer.Equals(target, value))
            {
                Changed = true;
                target = value;
            }
        }

        public void FixMissingReference<T>(ref T target) where T : Object
        {
            if (target == null && target is not null)
            {
                Changed = true;
                target = null;
            }
        }

        public void GetComponent<T>(ref T target) where T : Component
        {
            GetComponent(_targetGameObject, ref target);
        }

        public void GetComponent<T>(GameObject sourceGameObject, ref T target) where T : Component
        {
            T result = sourceGameObject.GetComponent<T>();
            AssignComponent(ref target, ref result);
        }

        public bool TryGetComponent<T>(ref T target) where T : Component
        {
            return TryGetComponent(_targetGameObject, ref target);
        }

        public bool TryGetComponent<T>(GameObject sourceGameObject, ref T target) where T : Component
        {
            bool found = sourceGameObject.TryGetComponent(out T result);
            AssignComponent(ref target, ref result);
            return found;
        }

        public void GetComponentInChildren<T>(ref T target, bool includeInactive) where T : Component
        {
            GetComponentInChildren(_targetGameObject, ref target, includeInactive);
        }

        public void GetComponentInChildren<T>(GameObject sourceGameObject, ref T target, bool includeInactive) where T : Component
        {
            T result = sourceGameObject.GetComponentInChildren<T>(includeInactive);
            AssignComponent(ref target, ref result);
        }

        public void GetComponentInParent<T>(ref T target, bool includeInactive) where T : Component
        {
            GetComponentInParent(_targetGameObject, ref target, includeInactive);
        }

        public void GetComponentInParent<T>(GameObject sourceGameObject, ref T target, bool includeInactive) where T : Component
        {
            T result = sourceGameObject.GetComponentInParent<T>(includeInactive);
            AssignComponent(ref target, ref result);
        }

        public void GetComponents<T>(ref T[] target) where T : Component
        {
            GetComponents(_targetGameObject, ref target);
        }

        public void GetComponents<T>(GameObject sourceGameObject, ref T[] target) where T : Component
        {
            T[] result = sourceGameObject.GetComponents<T>();
            AssignComponents(ref target, ref result);
        }

        public void GetComponents<T>(ref List<T> target) where T : Component
        {
            GetComponents(_targetGameObject, ref target);
        }

        public void GetComponents<T>(GameObject sourceGameObject, ref List<T> target) where T : Component
        {
            List<T> result = new(); // Intentionally not pooled
            sourceGameObject.GetComponents(result);
            AssignComponents(ref target, ref result);
        }

        public void GetComponentsInChildren<T>(ref T[] target, bool includeInactive) where T : Component
        {
            GetComponentsInChildren(_targetGameObject, ref target, includeInactive);
        }

        public void GetComponentsInChildren<T>(GameObject sourceGameObject, ref T[] target, bool includeInactive) where T : Component
        {
            T[] result = sourceGameObject.GetComponentsInChildren<T>(includeInactive);
            AssignComponents(ref target, ref result);
        }

        public void GetComponentsInChildren<T>(ref List<T> target, bool includeInactive) where T : Component
        {
            GetComponentsInChildren(_targetGameObject, ref target, includeInactive);
        }

        public void GetComponentsInChildren<T>(GameObject sourceGameObject, ref List<T> target, bool includeInactive) where T : Component
        {
            List<T> result = new(); // Intentionally not pooled
            sourceGameObject.GetComponentsInChildren(includeInactive, result);
            AssignComponents(ref target, ref result);
        }

        public void GetComponentsInParent<T>(ref T[] target, bool includeInactive) where T : Component
        {
            GetComponentsInParent(_targetGameObject, ref target, includeInactive);
        }

        public void GetComponentsInParent<T>(GameObject sourceGameObject, ref T[] target, bool includeInactive) where T : Component
        {
            T[] result = sourceGameObject.GetComponentsInParent<T>(includeInactive);
            AssignComponents(ref target, ref result);
        }

        public void GetComponentsInParent<T>(ref List<T> target, bool includeInactive) where T : Component
        {
            GetComponentsInParent(_targetGameObject, ref target, includeInactive);
        }

        public void GetComponentsInParent<T>(GameObject sourceGameObject, ref List<T> target, bool includeInactive) where T : Component
        {
            List<T> result = new(); // Intentionally not pooled
            sourceGameObject.GetComponentsInParent(includeInactive, result);
            AssignComponents(ref target, ref result);
        }

        public void TransformFind(ref Transform target, string childName)
        {
            TransformFind(_targetGameObject.transform, ref target, childName);
        }

        public void TransformFind(Transform sourceTransform, ref Transform target, string childName)
        {
            Transform result = sourceTransform.Find(childName);
            AssignComponent(ref target, ref result);
        }

        public void LoadScriptableObject_Editor<T>(ref T target, string name = null) where T : ScriptableObject
        {
            if (Changed)
            {
                GeneralUtils.LoadScriptableObject_Editor(ref target, name);
                return;
            }

            T result = null;
            GeneralUtils.LoadScriptableObject_Editor(ref result, name);

            if (target != result)
            {
                Changed = true;
                target = result;
            }
        }

        public void LoadScriptableObjects_Editor<T>(ref T[] target, string name = null) where T : ScriptableObject
        {
            if (Changed)
            {
                GeneralUtils.LoadScriptableObjects_Editor(ref target, name);
                return;
            }

            T[] result = null;
            GeneralUtils.LoadScriptableObjects_Editor(ref result, name);

            if (target.Length != result.Length)
            {
                Changed = true;
                target = result;
                return;
            }

            for (int i = 0; i < target.Length; i++)
            {
                if (target[i] != result[i])
                {
                    Changed = true;
                    target = result;
                    break;
                }
            }
        }

        public void MatchCollections<T>(ref List<T> target, ICollection<T> allItems, IEqualityComparer<T> equalityComparer = null)
        {
            if (target == null)
            {
                Changed = true;
                target = new List<T>();
            }

            if (!target.SequenceEqual(allItems, equalityComparer))
            {
                Changed = true;
                target.Clear();
                target.AddRange(allItems);
            }
        }

        public void MatchCollections<T>(ref HashSet<T> target, ICollection<T> allItems, IEqualityComparer<T> equalityComparer = null)
        {
            if (target == null)
            {
                Changed = true;
                target = new HashSet<T>();
            }

            if (!target.SequenceEqual(allItems, equalityComparer))
            {
                Changed = true;
                target.Clear();
                target.UnionWith(allItems);
            }
        }

        private void AssignComponent<T>(ref T target, ref T result) where T : Component
        {
            if (Changed)
            {
                target = result;
                return;
            }

            if (target == null)
            {
                if (result != null)
                {
                    Changed = true;
                    target = result;
                }

                return;
            }

            if (target != result)
            {
                Changed = true;
                target = result;
            }
        }

        private void AssignComponents<T>(ref T[] target, ref T[] result) where T : Component
        {
            if (Changed)
            {
                target = result;
                return;
            }

            if (target == null)
            {
                if (result != null)
                {
                    Changed = true;
                    target = result;
                }

                return;
            }

            if (target.Length != result.Length)
            {
                Changed = true;
                target = result;
                return;
            }

            for (int i = 0; i < target.Length; i++)
            {
                if (target[i] != result[i])
                {
                    Changed = true;
                    target = result;
                    break;
                }
            }
        }

        private void AssignComponents<T>(ref List<T> target, ref List<T> result) where T : Component
        {
            if (Changed)
            {
                target = result;
                return;
            }

            if (target == null)
            {
                if (result != null)
                {
                    Changed = true;
                    target = result;
                }

                return;
            }

            if (target.Count != result.Count)
            {
                Changed = true;
                target = result;
                return;
            }

            for (int i = 0; i < target.Count; i++)
            {
                if (target[i] != result[i])
                {
                    Changed = true;
                    target = result;
                    break;
                }
            }
        }

        public void Dispose()
        {
            _ownersCount--;
            if (_ownersCount > 0)
            {
                return;
            }

            ACTIVE_CHANGE_SCOPES.Remove(_targetObject);

            if (Changed)
            {
                SetDirty(_targetObject);
            }

            _ownersCount = 0;
            _targetObject = null;
            _targetGameObject = null;
            Changed = false;

            GenericPool<OnValidateChangeScope>.Release(this);
        }

        public static void SetDirty(Object target)
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(target);
#endif
        }
    }
}
