using Buggestic.Runtime.Controllers;
using UnityEngine;

namespace Buggestic.Runtime.Animation
{
    public class AutoDisableOnIdleAnimatorBehaviour : StateMachineBehaviour
    {
        private const float AUTO_DISABLE_TIMEOUT = 0.2f;

        private float _notInTransitionTime;

        public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if (animator.IsInTransition(layerIndex))
            {
                _notInTransitionTime = 0f;
            }
            else
            {
                if (_notInTransitionTime >= AUTO_DISABLE_TIMEOUT)
                {
                    animator.enabled = false;
                }

                _notInTransitionTime += TimeCache.DeltaTime;
            }
        }
    }
}
