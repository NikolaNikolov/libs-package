namespace Buggestic.Runtime.Signals
{
    public abstract class ObserverBase<TAction>
    {
        protected readonly TAction _action;

        protected ObserverBase(TAction action)
        {
            _action = action;
        }
    }
}
