using System.Collections.Generic;
using Buggestic.Runtime.Controllers;

namespace Buggestic.Runtime.Signals
{
    public class PeriodicUpdateSignal : SignalBase<Observer<float>>
    {
        public const float DEFAULT_PERIODIC_UPDATE_RATE = 0.2f;

        private float _nextUpdateTime;

        private readonly List<float> _lastInvokeTimes = new();

        public float UpdateRate { get; private set; }

        public PeriodicUpdateSignal()
        {
            SetUpdateRate(DEFAULT_PERIODIC_UPDATE_RATE);
        }

        public PeriodicUpdateSignal(float updateRate)
        {
            SetUpdateRate(updateRate);
        }

        public void SetUpdateRate(float updateRate)
        {
            UpdateRate = updateRate;
            if (_nextUpdateTime > updateRate)
            {
                _nextUpdateTime = updateRate;
            }
        }

        public void Invoke()
        {
            if (TimeCache.Time >= _nextUpdateTime)
            {
                _nextUpdateTime = UpdateRate;
                OnInvoke(true);
            }
            else
            {
                OnInvoke(false);
            }
        }

        protected override void OnInvokeObserver(Observer<float> observer, int observerIndex)
        {
            float lastInvokeTime = _lastInvokeTimes[observerIndex];
            float nextUpdateTime = lastInvokeTime + UpdateRate;
            if (TimeCache.Time >= nextUpdateTime)
            {
                float deltaTime = TimeCache.Time - lastInvokeTime;
                _lastInvokeTimes[observerIndex] = TimeCache.Time;
                observer.OnInvoke(deltaTime);
            }
            else if (_nextUpdateTime > nextUpdateTime)
            {
                _nextUpdateTime = nextUpdateTime;
            }
        }

        protected override void AddObserverInstantImpl(Observer<float> observer)
        {
            base.AddObserverInstantImpl(observer);

            _lastInvokeTimes.Add(TimeCache.Time);
        }

        protected override void RemoveObserverInstantAtSwapBackImpl(Observer<float> observer, int removeIndex)
        {
            base.RemoveObserverInstantAtSwapBackImpl(observer, removeIndex);

            int lastIndex = _lastInvokeTimes.Count - 1;
            if (removeIndex != lastIndex)
            {
                _lastInvokeTimes[removeIndex] = _lastInvokeTimes[lastIndex];
            }

            _lastInvokeTimes.RemoveAt(lastIndex);
        }

        protected override void RemoveAllObserversInstantImpl()
        {
            base.RemoveAllObserversInstantImpl();

            _lastInvokeTimes.Clear();
        }
    }
}
