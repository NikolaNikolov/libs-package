using System.Collections.Generic;

namespace Buggestic.Runtime.Signals
{
    public abstract class SignalBase<T>
    {
        private bool _isInvoking;
        private bool _isRemoveAllPending;

        private readonly List<T> _observers = new();
        private readonly Dictionary<T, int> _observerIndicesByObservers = new();

        private readonly HashSet<T> _observersPendingAdd = new();
        private readonly HashSet<T> _observersPendingRemove = new();

        protected bool HasPendingActions { get; private set; }
        public bool HasObserversOrPendingActions => HasPendingActions || _observers.Count > 0;

        public void AddObserver(T observer)
        {
            if (_isInvoking)
            {
                HasPendingActions = true;
                _observersPendingAdd.Add(observer);
                if (_observersPendingRemove.Count > 0)
                {
                    _observersPendingRemove.Remove(observer);
                }
            }
            else
            {
                AddObserverInstant(observer);
            }
        }

        public void RemoveObserver(T observer)
        {
            if (_isInvoking)
            {
                HasPendingActions = true;
                _observersPendingRemove.Add(observer);
                if (_observersPendingAdd.Count > 0)
                {
                    _observersPendingAdd.Remove(observer);
                }
            }
            else
            {
                RemoveObserverInstantAtSwapBack(observer);
            }
        }

        public void RemoveAllObservers()
        {
            if (_isInvoking)
            {
                HasPendingActions = true;
                _isRemoveAllPending = true;
                _observersPendingAdd.Clear();
                _observersPendingRemove.UnionWith(_observers);
            }
            else
            {
                RemoveAllObserversInstant();
            }
        }

        internal void EarlyMarkForInvocation()
        {
            _isInvoking = true;
        }

        internal void ClearMarkForInvocation()
        {
            _isInvoking = false;
        }

        protected void OnInvoke(bool invokeObservers)
        {
            if (invokeObservers)
            {
                _isInvoking = true;

                int observersCount = _observers.Count;
                for (int i = 0; i < observersCount; i++)
                {
                    T observer = _observers[i];
                    if (_observersPendingRemove.Count == 0 || !_observersPendingRemove.Contains(observer))
                    {
                        OnInvokeObserver(observer, i);
                    }
                }
            }

            _isInvoking = false;

            if (!HasPendingActions)
            {
                return;
            }

            if (_isRemoveAllPending)
            {
                _isRemoveAllPending = false;
                RemoveAllObserversInstant();
            }

            if (_observersPendingAdd.Count > 0)
            {
                foreach (T observer in _observersPendingAdd)
                {
                    AddObserverInstant(observer);
                }

                _observersPendingAdd.Clear();
            }

            if (_observersPendingRemove.Count > 0)
            {
                foreach (T observer in _observersPendingRemove)
                {
                    RemoveObserverInstantAtSwapBack(observer);
                }

                _observersPendingRemove.Clear();
            }

            HasPendingActions = false;
        }

        protected abstract void OnInvokeObserver(T observer, int observerIndex);

        protected void AddObserverInstant(T observer)
        {
            if (_observerIndicesByObservers.TryAdd(observer, _observers.Count))
            {
                _observers.Add(observer);
                AddObserverInstantImpl(observer);
            }
        }

        protected virtual void AddObserverInstantImpl(T observer)
        {
        }

        protected void RemoveObserverInstantAtSwapBack(T observer)
        {
            if (!_observerIndicesByObservers.Remove(observer, out int removeIndex))
            {
                return;
            }

            int lastIndex = _observers.Count - 1;
            if (removeIndex != lastIndex)
            {
                T lastObserver = _observers[lastIndex];
                _observers[removeIndex] = lastObserver;
                _observerIndicesByObservers[lastObserver] = removeIndex;
            }

            _observers.RemoveAt(lastIndex);
            RemoveObserverInstantAtSwapBackImpl(observer, removeIndex);
        }

        protected virtual void RemoveObserverInstantAtSwapBackImpl(T observer, int removeIndex)
        {
        }

        protected void RemoveAllObserversInstant()
        {
            _observers.Clear();
            _observerIndicesByObservers.Clear();
            RemoveAllObserversInstantImpl();
        }

        protected virtual void RemoveAllObserversInstantImpl()
        {
        }
    }
}
