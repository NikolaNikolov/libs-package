namespace Buggestic.Runtime.Signals
{
    public class Signal : SignalBase<Observer>
    {
        public void Invoke()
        {
            // Early exit not needed without any parameters
            // if (!HasObserversOrPendingActions)
            // {
            //     ClearMarkForInvocation();
            //     return;
            // }

            OnInvoke(true);
        }

        protected override void OnInvokeObserver(Observer observer, int observerIndex)
        {
            observer.OnInvoke();
        }
    }

    public class Signal<T> : SignalBase<Observer<T>>
    {
        private T _value;

        public void Invoke(T value)
        {
            if (!HasObserversOrPendingActions)
            {
                ClearMarkForInvocation();
                return;
            }

            _value = value;

            OnInvoke(true);

            _value = default;
        }

        protected override void OnInvokeObserver(Observer<T> observer, int observerIndex)
        {
            observer.OnInvoke(_value);
        }
    }

    public class Signal<T1, T2> : SignalBase<Observer<T1, T2>>
    {
        private T1 _value1;
        private T2 _value2;

        public void Invoke(T1 value1, T2 value2)
        {
            if (!HasObserversOrPendingActions)
            {
                ClearMarkForInvocation();
                return;
            }

            _value1 = value1;
            _value2 = value2;

            OnInvoke(true);

            _value1 = default;
            _value2 = default;
        }

        protected override void OnInvokeObserver(Observer<T1, T2> observer, int observerIndex)
        {
            observer.OnInvoke(_value1, _value2);
        }
    }

    public class Signal<T1, T2, T3> : SignalBase<Observer<T1, T2, T3>>
    {
        private T1 _value1;
        private T2 _value2;
        private T3 _value3;

        public void Invoke(T1 value1, T2 value2, T3 value3)
        {
            if (!HasObserversOrPendingActions)
            {
                ClearMarkForInvocation();
                return;
            }

            _value1 = value1;
            _value2 = value2;
            _value3 = value3;

            OnInvoke(true);

            _value1 = default;
            _value2 = default;
            _value3 = default;
        }

        protected override void OnInvokeObserver(Observer<T1, T2, T3> observer, int observerIndex)
        {
            observer.OnInvoke(_value1, _value2, _value3);
        }
    }
}
