using System;

namespace Buggestic.Runtime.Signals
{
    public class Observer : ObserverBase<Action>
    {
        public Observer(Action action) : base(action)
        {
        }

        public void OnInvoke()
        {
            _action();
        }
    }

    public class Observer<T> : ObserverBase<Action<T>>
    {
        public Observer(Action<T> action) : base(action)
        {
        }

        public void OnInvoke(T value)
        {
            _action(value);
        }
    }

    public class Observer<T1, T2> : ObserverBase<Action<T1, T2>>
    {
        public Observer(Action<T1, T2> action) : base(action)
        {
        }

        public void OnInvoke(T1 value1, T2 value2)
        {
            _action(value1, value2);
        }
    }

    public class Observer<T1, T2, T3> : ObserverBase<Action<T1, T2, T3>>
    {
        public Observer(Action<T1, T2, T3> action) : base(action)
        {
        }

        public void OnInvoke(T1 value1, T2 value2, T3 value3)
        {
            _action(value1, value2, value3);
        }
    }
}
