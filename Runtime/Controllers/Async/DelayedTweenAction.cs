using System;
using PrimeTween;
using UnityEngine.Pool;

namespace Buggestic.Runtime.Controllers.Async
{
    internal interface IDelayedTweenAction
    {
        public int AsyncOwnerId { get; }
        public void ReleaseToPool();
    }

    internal class DelayedTweenAction : IDelayedTweenAction
    {
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction GetFromPool(int asyncOwnerId, float delay, bool useUnscaledTime, Action onComplete)
        {
            DelayedTweenAction delayedTweenAction = GenericPool<DelayedTweenAction>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction GetFromPool(int asyncOwnerId, Tween tween, Action onComplete)
        {
            DelayedTweenAction delayedTweenAction = GenericPool<DelayedTweenAction>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke();
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1> : IDelayedTweenAction
    {
        private T1 _data1;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1> GetFromPool(int asyncOwnerId, float delay, bool useUnscaledTime, T1 data1, Action<T1> onComplete)
        {
            DelayedTweenAction<T1> delayedTweenAction = GenericPool<DelayedTweenAction<T1>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1> GetFromPool(int asyncOwnerId, Tween tween, T1 data1, Action<T1> onComplete)
        {
            DelayedTweenAction<T1> delayedTweenAction = GenericPool<DelayedTweenAction<T1>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1>>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1, T2> : IDelayedTweenAction
    {
        private T1 _data1;
        private T2 _data2;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1, T2> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1, T2> GetFromPool(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            Action<T1, T2> onComplete)
        {
            DelayedTweenAction<T1, T2> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1, T2> GetFromPool(int asyncOwnerId, Tween tween, T1 data1, T2 data2, Action<T1, T2> onComplete)
        {
            DelayedTweenAction<T1, T2> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1, _data2);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _data2 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1, T2>>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1, T2, T3> : IDelayedTweenAction
    {
        private T1 _data1;
        private T2 _data2;
        private T3 _data3;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1, T2, T3> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1, T2, T3> GetFromPool(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            Action<T1, T2, T3> onComplete)
        {
            DelayedTweenAction<T1, T2, T3> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2, T3>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1, T2, T3> GetFromPool(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            Action<T1, T2, T3> onComplete)
        {
            DelayedTweenAction<T1, T2, T3> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2, T3>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1, _data2, _data3);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _data2 = default;
            _data3 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1, T2, T3>>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1, T2, T3, T4> : IDelayedTweenAction
    {
        private T1 _data1;
        private T2 _data2;
        private T3 _data3;
        private T4 _data4;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1, T2, T3, T4> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1, T2, T3, T4> GetFromPool(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            Action<T1, T2, T3, T4> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2, T3, T4>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1, T2, T3, T4> GetFromPool(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            Action<T1, T2, T3, T4> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2, T3, T4>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1, _data2, _data3, _data4);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _data2 = default;
            _data3 = default;
            _data4 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1, T2, T3, T4>>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1, T2, T3, T4, T5> : IDelayedTweenAction
    {
        private T1 _data1;
        private T2 _data2;
        private T3 _data3;
        private T4 _data4;
        private T5 _data5;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1, T2, T3, T4, T5> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5> GetFromPool(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            Action<T1, T2, T3, T4, T5> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5> GetFromPool(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            Action<T1, T2, T3, T4, T5> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1, _data2, _data3, _data4, _data5);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _data2 = default;
            _data3 = default;
            _data4 = default;
            _data5 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5>>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1, T2, T3, T4, T5, T6> : IDelayedTweenAction
    {
        private T1 _data1;
        private T2 _data2;
        private T3 _data3;
        private T4 _data4;
        private T5 _data5;
        private T6 _data6;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1, T2, T3, T4, T5, T6> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6> GetFromPool(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            Action<T1, T2, T3, T4, T5, T6> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6> GetFromPool(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            Action<T1, T2, T3, T4, T5, T6> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1, _data2, _data3, _data4, _data5, _data6);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _data2 = default;
            _data3 = default;
            _data4 = default;
            _data5 = default;
            _data6 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6>>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7> : IDelayedTweenAction
    {
        private T1 _data1;
        private T2 _data2;
        private T3 _data3;
        private T4 _data4;
        private T5 _data5;
        private T6 _data6;
        private T7 _data7;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1, T2, T3, T4, T5, T6, T7> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7> GetFromPool(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            Action<T1, T2, T3, T4, T5, T6, T7> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7> GetFromPool(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            Action<T1, T2, T3, T4, T5, T6, T7> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7> delayedTweenAction = GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1, _data2, _data3, _data4, _data5, _data6, _data7);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _data2 = default;
            _data3 = default;
            _data4 = default;
            _data5 = default;
            _data6 = default;
            _data7 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7>>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8> : IDelayedTweenAction
    {
        private T1 _data1;
        private T2 _data2;
        private T3 _data3;
        private T4 _data4;
        private T5 _data5;
        private T6 _data6;
        private T7 _data7;
        private T8 _data8;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1, T2, T3, T4, T5, T6, T7, T8> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8> GetFromPool(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            Action<T1, T2, T3, T4, T5, T6, T7, T8> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8> delayedTweenAction =
                GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._data8 = data8;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8> GetFromPool(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            Action<T1, T2, T3, T4, T5, T6, T7, T8> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8> delayedTweenAction =
                GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._data8 = data8;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1, _data2, _data3, _data4, _data5, _data6, _data7, _data8);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _data2 = default;
            _data3 = default;
            _data4 = default;
            _data5 = default;
            _data6 = default;
            _data7 = default;
            _data8 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8>>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> : IDelayedTweenAction
    {
        private T1 _data1;
        private T2 _data2;
        private T3 _data3;
        private T4 _data4;
        private T5 _data5;
        private T6 _data6;
        private T7 _data7;
        private T8 _data8;
        private T9 _data9;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> GetFromPool(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> delayedTweenAction =
                GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._data8 = data8;
            delayedTweenAction._data9 = data9;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> GetFromPool(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9> delayedTweenAction =
                GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._data8 = data8;
            delayedTweenAction._data9 = data9;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1, _data2, _data3, _data4, _data5, _data6, _data7, _data8, _data9);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _data2 = default;
            _data3 = default;
            _data4 = default;
            _data5 = default;
            _data6 = default;
            _data7 = default;
            _data8 = default;
            _data9 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> : IDelayedTweenAction
    {
        private T1 _data1;
        private T2 _data2;
        private T3 _data3;
        private T4 _data4;
        private T5 _data5;
        private T6 _data6;
        private T7 _data7;
        private T8 _data8;
        private T9 _data9;
        private T10 _data10;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> GetFromPool(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> delayedTweenAction =
                GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._data8 = data8;
            delayedTweenAction._data9 = data9;
            delayedTweenAction._data10 = data10;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> GetFromPool(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> delayedTweenAction =
                GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._data8 = data8;
            delayedTweenAction._data9 = data9;
            delayedTweenAction._data10 = data10;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1, _data2, _data3, _data4, _data5, _data6, _data7, _data8, _data9, _data10);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _data2 = default;
            _data3 = default;
            _data4 = default;
            _data5 = default;
            _data6 = default;
            _data7 = default;
            _data8 = default;
            _data9 = default;
            _data10 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> : IDelayedTweenAction
    {
        private T1 _data1;
        private T2 _data2;
        private T3 _data3;
        private T4 _data4;
        private T5 _data5;
        private T6 _data6;
        private T7 _data7;
        private T8 _data8;
        private T9 _data9;
        private T10 _data10;
        private T11 _data11;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> GetFromPool(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            T11 data11,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> delayedTweenAction =
                GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._data8 = data8;
            delayedTweenAction._data9 = data9;
            delayedTweenAction._data10 = data10;
            delayedTweenAction._data11 = data11;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> GetFromPool(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            T11 data11,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> delayedTweenAction =
                GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._data8 = data8;
            delayedTweenAction._data9 = data9;
            delayedTweenAction._data10 = data10;
            delayedTweenAction._data11 = data11;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1, _data2, _data3, _data4, _data5, _data6, _data7, _data8, _data9, _data10, _data11);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _data2 = default;
            _data3 = default;
            _data4 = default;
            _data5 = default;
            _data6 = default;
            _data7 = default;
            _data8 = default;
            _data9 = default;
            _data10 = default;
            _data11 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>>.Release(this);
        }
    }

    internal class DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> : IDelayedTweenAction
    {
        private T1 _data1;
        private T2 _data2;
        private T3 _data3;
        private T4 _data4;
        private T5 _data5;
        private T6 _data6;
        private T7 _data7;
        private T8 _data8;
        private T9 _data9;
        private T10 _data10;
        private T11 _data11;
        private T12 _data12;
        private Tween _tween;
        private readonly Action _onCompleteTweenAction;
        private Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> _onComplete;

        public int AsyncOwnerId { get; private set; }

        public DelayedTweenAction()
        {
            _onCompleteTweenAction = OnCompleteTween;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> GetFromPool(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            T11 data11,
            T12 data12,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> delayedTweenAction =
                GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._data8 = data8;
            delayedTweenAction._data9 = data9;
            delayedTweenAction._data10 = data10;
            delayedTweenAction._data11 = data11;
            delayedTweenAction._data12 = data12;
            delayedTweenAction._tween = Tween.Delay(delay, delayedTweenAction._onCompleteTweenAction, useUnscaledTime);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        public static DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> GetFromPool(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            T11 data11,
            T12 data12,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> onComplete)
        {
            DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> delayedTweenAction =
                GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>>.Get();
            delayedTweenAction.AsyncOwnerId = asyncOwnerId;
            delayedTweenAction._data1 = data1;
            delayedTweenAction._data2 = data2;
            delayedTweenAction._data3 = data3;
            delayedTweenAction._data4 = data4;
            delayedTweenAction._data5 = data5;
            delayedTweenAction._data6 = data6;
            delayedTweenAction._data7 = data7;
            delayedTweenAction._data8 = data8;
            delayedTweenAction._data9 = data9;
            delayedTweenAction._data10 = data10;
            delayedTweenAction._data11 = data11;
            delayedTweenAction._data12 = data12;
            delayedTweenAction._tween = tween.OnComplete(delayedTweenAction._onCompleteTweenAction);
            delayedTweenAction._onComplete = onComplete;
            return delayedTweenAction;
        }

        private void OnCompleteTween()
        {
            _onComplete?.Invoke(_data1, _data2, _data3, _data4, _data5, _data6, _data7, _data8, _data9, _data10, _data11, _data12);
            BuggesticCore.AsyncController.OnDelayedTweenActionExecuted(this);
        }

        public void ReleaseToPool()
        {
            _tween.Stop();

            AsyncOwnerId = default;
            _data1 = default;
            _data2 = default;
            _data3 = default;
            _data4 = default;
            _data5 = default;
            _data6 = default;
            _data7 = default;
            _data8 = default;
            _data9 = default;
            _data10 = default;
            _data11 = default;
            _data12 = default;
            _onComplete = default;
            GenericPool<DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>>.Release(this);
        }
    }
}
