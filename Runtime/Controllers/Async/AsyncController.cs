using System;
using System.Collections.Generic;
using Buggestic.Runtime.Pooling.Pool_Manager;
using PrimeTween;
using UnityEngine.Pool;

namespace Buggestic.Runtime.Controllers.Async
{
    public class AsyncController : CachedMonoBehaviour
    {
        private readonly Dictionary<int, HashSet<IDelayedTweenAction>> _delayedTweenActionsByOwnerIds = new();

        private static int _nextAsyncOwnerId = 1;

        public static int GetAsyncOwnerId()
        {
            return _nextAsyncOwnerId++;
        }

        public bool HasDelayedTweenAction(int asyncOwnerId)
        {
            return _delayedTweenActionsByOwnerIds.ContainsKey(asyncOwnerId);
        }

        public void RegisterDelayedTweenAction(int asyncOwnerId, float delay, bool useUnscaledTime, Action onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction.GetFromPool(asyncOwnerId, delay, useUnscaledTime, onComplete));
        }

        public void RegisterDelayedTweenAction(int asyncOwnerId, Tween tween, Action onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction.GetFromPool(asyncOwnerId, tween, onComplete));
        }

        public void RegisterDelayedTweenAction<T1>(int asyncOwnerId, float delay, bool useUnscaledTime, T1 data1, Action<T1> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1>.GetFromPool(asyncOwnerId, delay, useUnscaledTime, data1, onComplete));
        }

        public void RegisterDelayedTweenAction<T1>(int asyncOwnerId, Tween tween, T1 data1, Action<T1> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1>.GetFromPool(asyncOwnerId, tween, data1, onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2>(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            Action<T1, T2> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2>.GetFromPool(asyncOwnerId, delay, useUnscaledTime, data1, data2, onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2>(int asyncOwnerId, Tween tween, T1 data1, T2 data2, Action<T1, T2> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2>.GetFromPool(asyncOwnerId, tween, data1, data2, onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3>(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            Action<T1, T2, T3> onComplete)
        {
            RegisterDelayedTweenAction(
                DelayedTweenAction<T1, T2, T3>.GetFromPool(asyncOwnerId, delay, useUnscaledTime, data1, data2, data3, onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3>(int asyncOwnerId, Tween tween, T1 data1, T2 data2, T3 data3, Action<T1, T2, T3> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3>.GetFromPool(asyncOwnerId, tween, data1, data2, data3, onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4>(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            Action<T1, T2, T3, T4> onComplete)
        {
            RegisterDelayedTweenAction(
                DelayedTweenAction<T1, T2, T3, T4>.GetFromPool(asyncOwnerId, delay, useUnscaledTime, data1, data2, data3, data4, onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4>(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            Action<T1, T2, T3, T4> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4>.GetFromPool(asyncOwnerId, tween, data1, data2, data3, data4, onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5>(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            Action<T1, T2, T3, T4, T5> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5>.GetFromPool(asyncOwnerId,
                delay,
                useUnscaledTime,
                data1,
                data2,
                data3,
                data4,
                data5,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5>(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            Action<T1, T2, T3, T4, T5> onComplete)
        {
            RegisterDelayedTweenAction(
                DelayedTweenAction<T1, T2, T3, T4, T5>.GetFromPool(asyncOwnerId, tween, data1, data2, data3, data4, data5, onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6>(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            Action<T1, T2, T3, T4, T5, T6> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6>.GetFromPool(asyncOwnerId,
                delay,
                useUnscaledTime,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6>(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            Action<T1, T2, T3, T4, T5, T6> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6>.GetFromPool(asyncOwnerId,
                tween,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7>(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            Action<T1, T2, T3, T4, T5, T6, T7> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7>.GetFromPool(asyncOwnerId,
                delay,
                useUnscaledTime,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7>(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            Action<T1, T2, T3, T4, T5, T6, T7> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7>.GetFromPool(asyncOwnerId,
                tween,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8>(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            Action<T1, T2, T3, T4, T5, T6, T7, T8> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8>.GetFromPool(asyncOwnerId,
                delay,
                useUnscaledTime,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                data8,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8>(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            Action<T1, T2, T3, T4, T5, T6, T7, T8> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8>.GetFromPool(asyncOwnerId,
                tween,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                data8,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>.GetFromPool(asyncOwnerId,
                delay,
                useUnscaledTime,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                data8,
                data9,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9>.GetFromPool(asyncOwnerId,
                tween,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                data8,
                data9,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>.GetFromPool(asyncOwnerId,
                delay,
                useUnscaledTime,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                data8,
                data9,
                data10,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10>.GetFromPool(asyncOwnerId,
                tween,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                data8,
                data9,
                data10,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            T11 data11,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>.GetFromPool(asyncOwnerId,
                delay,
                useUnscaledTime,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                data8,
                data9,
                data10,
                data11,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            T11 data11,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11>.GetFromPool(asyncOwnerId,
                tween,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                data8,
                data9,
                data10,
                data11,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(int asyncOwnerId,
            float delay,
            bool useUnscaledTime,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            T11 data11,
            T12 data12,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>.GetFromPool(asyncOwnerId,
                delay,
                useUnscaledTime,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                data8,
                data9,
                data10,
                data11,
                data12,
                onComplete));
        }

        public void RegisterDelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>(int asyncOwnerId,
            Tween tween,
            T1 data1,
            T2 data2,
            T3 data3,
            T4 data4,
            T5 data5,
            T6 data6,
            T7 data7,
            T8 data8,
            T9 data9,
            T10 data10,
            T11 data11,
            T12 data12,
            Action<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12> onComplete)
        {
            RegisterDelayedTweenAction(DelayedTweenAction<T1, T2, T3, T4, T5, T6, T7, T8, T9, T10, T11, T12>.GetFromPool(asyncOwnerId,
                tween,
                data1,
                data2,
                data3,
                data4,
                data5,
                data6,
                data7,
                data8,
                data9,
                data10,
                data11,
                data12,
                onComplete));
        }

        private void RegisterDelayedTweenAction(IDelayedTweenAction delayedTweenAction)
        {
            if (!_delayedTweenActionsByOwnerIds.TryGetValue(delayedTweenAction.AsyncOwnerId, out HashSet<IDelayedTweenAction> delayedTweenActions))
            {
                delayedTweenActions = HashSetPool<IDelayedTweenAction>.Get();
                _delayedTweenActionsByOwnerIds.Add(delayedTweenAction.AsyncOwnerId, delayedTweenActions);
            }

            delayedTweenActions.Add(delayedTweenAction);
        }

        public void CancelDelayedTweenActions(int asyncOwnerId)
        {
            if (_delayedTweenActionsByOwnerIds.TryGetValue(asyncOwnerId, out HashSet<IDelayedTweenAction> delayedTweenActions))
            {
                foreach (IDelayedTweenAction delayedTweenAction in delayedTweenActions)
                {
                    delayedTweenAction.ReleaseToPool();
                }

                HashSetPool<IDelayedTweenAction>.Release(delayedTweenActions);
                _delayedTweenActionsByOwnerIds.Remove(asyncOwnerId);
            }
        }

        internal void OnDelayedTweenActionExecuted(IDelayedTweenAction delayedTweenAction)
        {
            if (_delayedTweenActionsByOwnerIds.TryGetValue(delayedTweenAction.AsyncOwnerId, out HashSet<IDelayedTweenAction> delayedTweenActions))
            {
                delayedTweenActions.Remove(delayedTweenAction);
                if (delayedTweenActions.Count == 0)
                {
                    HashSetPool<IDelayedTweenAction>.Release(delayedTweenActions);
                    _delayedTweenActionsByOwnerIds.Remove(delayedTweenAction.AsyncOwnerId);
                }
            }

            delayedTweenAction.ReleaseToPool();
        }
    }
}
