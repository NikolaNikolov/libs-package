using Buggestic.Runtime.Pooling.Pool_Manager;
using UnityEngine;

namespace Buggestic.Runtime.Controllers
{
    [DefaultExecutionOrder(-32000)]
    public class TimeCacheController : CachedMonoBehaviour
    {
        private void Update()
        {
            TimeCache.Time = Time.time;
            TimeCache.DeltaTime = Time.deltaTime;
            TimeCache.FrameCount = Time.frameCount;
        }
    }
}
