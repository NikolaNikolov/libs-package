﻿// Example Rewired input controller implementation

/*
using UnityEngine;

namespace Buggestic
{
    public class RewiredInputController : InputControllerBase
    {
        private Player _rewiredPlayer;
    
        public override bool HasBackInput => _rewiredPlayer.GetButtonDown(RewiredConsts.Action.UI.UICANCEL);
    
        protected override void InitializeInput()
        {
            _rewiredPlayer = ReInput.players.GetPlayer(0);
    
            if (_rewiredPlayer.controllers.hasMouse)
            {
                InputType = InputType.Mouse;
            }
            else if (_rewiredPlayer.controllers.hasKeyboard)
            {
                InputType = InputType.Keyboard;
            }
            else if (_rewiredPlayer.controllers.joystickCount > 0)
            {
                InputType = InputType.Controller;
            }
            else
            {
                InputType = InputType.Touch;
            }
        }
    
        protected override InputType GetCurrentInputType()
        {
            // Touch detection
            if (Input.touchCount > 0)
            {
                return InputType.Touch;
            }
    
            // Controller detection
            foreach (var joystick in _rewiredPlayer.controllers.Joysticks)
            {
                bool anyJoystickButton = joystick.GetAnyButton();
                bool anyJoystickAxis = joystick.GetLastTimeAnyAxisActive() == ReInput.time.unscaledTime;
                if (anyJoystickButton || anyJoystickAxis)
                {
                    return InputType.Controller;
                }
            }
    
            // Keyboard detection
            bool anyKeyboardButton = _rewiredPlayer.controllers.Keyboard.GetAnyButton();
            if (anyKeyboardButton)
            {
                return InputType.Keyboard;
            }
    
            // Mouse detection
            bool anyMouseButton = _rewiredPlayer.controllers.Mouse.GetAnyButton();
            bool anyMouseAxis = _rewiredPlayer.controllers.Mouse.GetLastTimeAnyAxisActive() == ReInput.time.unscaledTime;
            if (anyMouseButton || anyMouseAxis)
            {
                return InputType.Mouse;
            }
    
            return InputType;
        }
    
        //public bool TryVibrateController()
        //{
        //    if (InputType == InputType.Controller)
        //    {
        //        Controller lastActiveController = _rewiredPlayer.controllers.GetLastActiveController();
    
        //        if (lastActiveController != null && lastActiveController.type == ControllerType.Joystick)
        //        {
        //            if (lastActiveController is Joystick joystick && joystick.supportsVibration)
        //            {
        //                joystick.SetVibration(0.1f, 0.1f, 0.08f, 0.08f);
    
        //                return true;
        //            }
        //        }
        //    }
    
        //    return false;
        //}
    }
}
*/
