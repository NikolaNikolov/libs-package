﻿namespace Buggestic.Runtime.Controllers.Input
{
    public static class InputTypeExtensions
    {
        public static bool IsTouch(this InputType inputType)
        {
            return inputType == InputType.Touch;
        }

        public static bool IsMouse(this InputType inputType)
        {
            return inputType == InputType.Mouse;
        }

        public static bool IsKeyboard(this InputType inputType)
        {
            return inputType == InputType.Keyboard;
        }

        public static bool IsController(this InputType inputType)
        {
            return inputType == InputType.Controller;
        }

        public static bool IsMouseOrTouch(this InputType inputType)
        {
            return inputType is InputType.Mouse or InputType.Touch;
        }

        public static bool IsMouseOrKeyboard(this InputType inputType)
        {
            return inputType is InputType.Mouse or InputType.Keyboard;
        }

        public static bool IsKeyboardOrController(this InputType inputType)
        {
            return inputType is InputType.Keyboard or InputType.Controller;
        }
    }
}
