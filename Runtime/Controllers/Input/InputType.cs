﻿namespace Buggestic.Runtime.Controllers.Input
{
    public enum InputType
    {
        None,
        Mouse,
        Keyboard,
        Controller,
        Touch,
    }
}
