﻿using Buggestic.Runtime.Pooling.Pool_Manager;
using Buggestic.Runtime.Signals;
using UnityEngine;

namespace Buggestic.Runtime.Controllers.Input
{
    [DefaultExecutionOrder(-5000)]
    public abstract class InputControllerBase : CachedMonoBehaviour
    {
        public readonly Signal<InputType, InputType> OnInputTypeChangedSignal = new();

        public InputType InputType { get; protected set; } = InputType.None;

        public abstract bool HasSubmitButtonDown { get; }
        public abstract bool HasCancelButtonDown { get; }
        public abstract bool HasTabNextNavigationButtonDown { get; }
        public abstract bool HasTabPreviousNavigationButtonDown { get; }

        protected virtual void Start()
        {
            InitializeInputInternal();
        }

        protected virtual void Update()
        {
            UpdateCurrentInputType();
        }

        protected virtual void InitializeInputInternal()
        {
            InitializeInput();
            UpdateCursorVisibility();
        }

        protected abstract void InitializeInput();

        protected virtual void UpdateCurrentInputType()
        {
            InputType previousInputType = InputType;
            InputType = GetCurrentInputType();
            if (InputType == previousInputType)
            {
                return;
            }

            UpdateCursorVisibility();

            OnInputTypeChangedSignal.Invoke(previousInputType, InputType);
        }

        protected abstract InputType GetCurrentInputType();

        public virtual void UpdateCursorVisibility()
        {
#if UNITY_EDITOR
            return;
#endif

#pragma warning disable CS0162 // Unreachable code detected
            // ReSharper disable HeuristicUnreachableCode
            if (InputType.IsMouseOrKeyboard())
            {
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
            }
            else
            {
                Cursor.visible = false;
                Cursor.lockState = CursorLockMode.Locked;
            }
            // ReSharper restore HeuristicUnreachableCode
#pragma warning restore CS0162 // Unreachable code detected
        }
    }
}
