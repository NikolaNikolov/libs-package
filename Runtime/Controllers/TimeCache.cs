namespace Buggestic.Runtime.Controllers
{
    public static class TimeCache
    {
        public static float Time { get; internal set; }
        public static float DeltaTime { get; internal set; }
        public static int FrameCount { get; internal set; }
    }
}
