﻿using Buggestic.Runtime.Controllers.Async;
using Buggestic.Runtime.Controllers.Input;
using Buggestic.Runtime.Pooling.Misc;
using Buggestic.Runtime.Pooling.Pool_Manager;
using Buggestic.Runtime.UI.Aspect_And_Orientation;
using UnityEngine;

namespace Buggestic.Runtime.Controllers
{
    public class BuggesticCore : CachedMonoBehaviour
    {
        [SerializeField] protected bool _dontDestroyOnLoad = true;

        [Header("Buggestic")]
        [SerializeField] protected PoolManager _poolManager;
        [SerializeField] protected MaterialPool _materialPool;
        [SerializeField] protected AsyncController _asyncController;
        [SerializeField] protected InputControllerBase _inputController;
        [SerializeField] protected UINavigationController _uiNavigationController;
        [SerializeField] protected ScreenAspectChangeDetector _screenAspectChangeDetector;
        [SerializeField] protected TimeCacheController _timeCacheController;

        protected static BuggesticCore _buggesticCoreInstance;

        public static PoolManager PoolManager => _buggesticCoreInstance._poolManager;
        public static MaterialPool MaterialPool => _buggesticCoreInstance._materialPool;
        public static AsyncController AsyncController => _buggesticCoreInstance._asyncController;
        public static InputControllerBase InputController => _buggesticCoreInstance._inputController;
        public static UINavigationController UINavigationController => _buggesticCoreInstance._uiNavigationController;
        public static ScreenAspectChangeDetector ScreenAspectChangeDetector => _buggesticCoreInstance._screenAspectChangeDetector;
        public static TimeCacheController TimeCacheController => _buggesticCoreInstance._timeCacheController;

        public static bool BuggesticCoreExists => _buggesticCoreInstance is not null;

        public static bool IsApplicationQuitting { get; protected set; }

        protected override void AwakeImpl()
        {
            base.AwakeImpl();

            if (_buggesticCoreInstance != null)
            {
                Destroy(gameObject);
                return;
            }

            _buggesticCoreInstance = this;
            if (_dontDestroyOnLoad)
            {
                DontDestroyOnLoad(gameObject);
            }

            IsApplicationQuitting = false;
            Application.quitting += OnApplicationQuitting;
        }

        protected virtual void OnDestroy()
        {
            Application.quitting -= OnApplicationQuitting;
        }

        protected virtual void OnApplicationQuitting()
        {
            IsApplicationQuitting = true;
        }

        public static void QuitGame()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
            return;
#endif

#pragma warning disable CS0162 // Unreachable code detected
            // ReSharper disable once HeuristicUnreachableCode
            Application.Quit();
#pragma warning restore CS0162 // Unreachable code detected
        }
    }
}
