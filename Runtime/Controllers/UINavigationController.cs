﻿using Buggestic.Runtime.Pooling.Pool_Manager;
using Buggestic.Runtime.Signals;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Buggestic.Runtime.Controllers
{
    public class UINavigationController : CachedMonoBehaviour
    {
        public readonly Signal<GameObject, GameObject> OnSelectedObjectChangedSignal = new();
        public readonly Signal<Selectable, Selectable> OnHoveredSelectableChangedSignal = new();

        protected GameObject _previousSelectedObject;
        protected Selectable _hoveredSelectable;

        public Selectable PreviousHoveredSelectable { get; protected set; }

        public Selectable HoveredSelectable
        {
            get => _hoveredSelectable;
            set
            {
                if (_hoveredSelectable == value)
                {
                    return;
                }

                PreviousHoveredSelectable = _hoveredSelectable;
                _hoveredSelectable = value;
                OnHoveredSelectableChangedSignal.Invoke(PreviousHoveredSelectable, _hoveredSelectable);
            }
        }

        protected virtual void Start()
        {
            _previousSelectedObject = EventSystem.current?.currentSelectedGameObject;
        }

        protected virtual void Update()
        {
            GameObject selectedObject = EventSystem.current?.currentSelectedGameObject;
            if (_previousSelectedObject != selectedObject)
            {
                OnSelectedObjectChangedSignal.Invoke(_previousSelectedObject, selectedObject);
                _previousSelectedObject = selectedObject;
            }
        }
    }
}
