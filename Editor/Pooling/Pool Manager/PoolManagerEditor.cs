﻿using System;
using System.Collections.Generic;
using Buggestic.Runtime.Controllers;
using Buggestic.Runtime.Pooling.Pool_Manager;
using UnityEditor;
using UnityEngine;

namespace Buggestic.Editor.Pooling.Pool_Manager
{
    [CustomEditor(typeof(PoolManager))]
    public class PoolManagerEditor : UnityEditor.Editor
    {
        private string _searchTerm;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (!Application.isPlaying)
            {
                return;
            }

            BuggesticCore.PoolManager.IsPoolingDisabled_EditorDebug =
                EditorGUILayout.Toggle("Is Pooling Disabled", BuggesticCore.PoolManager.IsPoolingDisabled_EditorDebug);
            EditorGUILayout.Space();

            _searchTerm = EditorGUILayout.TextField("Search", _searchTerm);
            EditorGUILayout.Space();

            DisplayPoolManagerStatistics();
            DisplayMaterialPoolStatistics();

            Repaint();
        }

        private void DisplayPoolManagerStatistics()
        {
            EditorGUILayout.LabelField("Pool Manager", EditorStyles.boldLabel);

            int totalActivePoolsSize = 0;
            int totalPoolsSize = 0;

            foreach (KeyValuePair<int, string> poolNameByPoolId in BuggesticCore.PoolManager.PoolNamesByPoolIds)
            {
                if (!string.IsNullOrEmpty(_searchTerm))
                {
                    if (!poolNameByPoolId.Value.Contains(_searchTerm, StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }
                }

                int currentActivePoolSize = BuggesticCore.PoolManager.GetActivePoolSize(poolNameByPoolId.Key);
                int currentTotalPoolSize = BuggesticCore.PoolManager.GetTotalPoolSize(poolNameByPoolId.Key);
                totalActivePoolsSize += currentActivePoolSize;
                totalPoolsSize += currentTotalPoolSize;

                if (currentActivePoolSize > 0)
                {
                    EditorGUILayout.LabelField($"{poolNameByPoolId.Value}: {currentActivePoolSize} / {currentTotalPoolSize}", EditorStyles.boldLabel);
                }
                else
                {
                    EditorGUILayout.LabelField($"{poolNameByPoolId.Value}: {currentActivePoolSize} / {currentTotalPoolSize}");
                }
            }

            EditorGUILayout.LabelField($"Total: {totalActivePoolsSize} / {totalPoolsSize}", EditorStyles.boldLabel);
            EditorGUILayout.Space();
        }

        private void DisplayMaterialPoolStatistics()
        {
            EditorGUILayout.LabelField("Material Pool", EditorStyles.boldLabel);

            int totalActivePoolsSize = 0;
            int totalPoolsSize = 0;

            if (BuggesticCore.MaterialPool is not null)
            {
                foreach (Material sourceMaterial in BuggesticCore.MaterialPool.SourceMaterials)
                {
                    if (!string.IsNullOrEmpty(_searchTerm))
                    {
                        if (!sourceMaterial.name.Contains(_searchTerm, StringComparison.InvariantCultureIgnoreCase))
                        {
                            continue;
                        }
                    }

                    int currentActivePoolSize = BuggesticCore.MaterialPool.GetActiveCount(sourceMaterial);
                    int currentTotalPoolSize = BuggesticCore.MaterialPool.GetTotalCount(sourceMaterial);
                    totalActivePoolsSize += currentActivePoolSize;
                    totalPoolsSize += currentTotalPoolSize;

                    if (currentActivePoolSize > 0)
                    {
                        EditorGUILayout.LabelField($"{sourceMaterial.name}: {currentActivePoolSize} / {currentTotalPoolSize}",
                            EditorStyles.boldLabel);
                    }
                    else
                    {
                        EditorGUILayout.LabelField($"{sourceMaterial.name}: {currentActivePoolSize} / {currentTotalPoolSize}");
                    }
                }
            }

            EditorGUILayout.LabelField($"Total: {totalActivePoolsSize} / {totalPoolsSize}", EditorStyles.boldLabel);
            EditorGUILayout.Space();
        }
    }
}
