﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Buggestic.Editor.Utils
{
    public static class SortChildrenHelper
    {
        [MenuItem("Tools/Buggestic/Sort Selection Children")]
        public static void SortSelectionChildren()
        {
            foreach (var selectedTransform in Selection.transforms)
            {
                List<Transform> childTransforms = new List<Transform>(selectedTransform.childCount);
                for (int i = 0; i < selectedTransform.childCount; i++)
                {
                    childTransforms.Add(selectedTransform.GetChild(i));
                }

                childTransforms.Sort((a, b) => string.Compare(a.name, b.name, StringComparison.Ordinal));

                for (int i = 0; i < childTransforms.Count; i++)
                {
                    childTransforms[i].SetSiblingIndex(i);
                }
            }
        }
    }
}
