﻿using System;
using System.Reflection;
using UnityEditor;

namespace Buggestic.Editor.Utils
{
    public static class EditorShortcuts
    {
        [MenuItem("Tools/Buggestic/Clear Console &Z")]
        public static void ClearConsole()
        {
            Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
            Type type = assembly.GetType("UnityEditor.LogEntries");
            MethodInfo method = type.GetMethod("Clear");
            if (method != null)
            {
                method.Invoke(new object(), null);
            }
        }

        [MenuItem("Tools/Buggestic/Maximize Window &X")]
        public static void MaximizeView()
        {
            EditorWindow focusedWindow = EditorWindow.focusedWindow;
            if (focusedWindow != null)
            {
                focusedWindow.maximized = !focusedWindow.maximized;
            }
        }
    }
}
