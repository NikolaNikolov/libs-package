﻿using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace Buggestic.Editor.Utils
{
    /// <summary>
    /// A helper editor script for finding missing references to objects.
    /// </summary>
    public static class MissingReferencesFinder
    {
        private const bool SEARCH_MAIN_ASSEMBLY_ONLY = true;
        private const bool SEARCH_ARRAYS_FOR_NULL = true;

        private const string MENU_ROOT = "Tools/Buggestic/Missing References/";

        /// <summary>
        /// Finds all missing references to objects in the currently loaded scene.
        /// </summary>
        [MenuItem(MENU_ROOT + "Search in scene", false, 50)]
        public static void FindMissingReferencesInCurrentScene()
        {
            EditorUtility.DisplayProgressBar("Finding Missing References...", "0%", 0f);

            var sceneObjects = GetSceneObjects();
            FindMissingReferences(EditorSceneManager.GetActiveScene().path, sceneObjects);

            EditorUtility.ClearProgressBar();
        }

        /// <summary>
        /// Finds all missing references to objects in all enabled scenes in the project.
        /// This works by loading the scenes one by one and checking for missing object references.
        /// </summary>
        [MenuItem(MENU_ROOT + "Search in all scenes", false, 51)]
        public static void FindMissingReferencesInAllScenes()
        {
            string activeScenePath = EditorSceneManager.GetActiveScene().path;

            var scenes = EditorBuildSettings.scenes.Where(s => s.enabled);
            foreach (var scene in scenes)
            {
                EditorUtility.DisplayProgressBar("Finding Missing References...", "0%", 0f);

                EditorSceneManager.OpenScene(scene.path);
                var sceneObjects = GetSceneObjects();
                FindMissingReferences(EditorSceneManager.GetActiveScene().path, sceneObjects);
            }

            EditorSceneManager.OpenScene(activeScenePath);

            EditorUtility.ClearProgressBar();
        }

        /// <summary>
        /// Finds all missing references to objects in assets (objects from the project window).
        /// </summary>
        [MenuItem(MENU_ROOT + "Search in assets", false, 52)]
        public static void FindMissingReferencesInAssets()
        {
            EditorUtility.DisplayProgressBar("Finding Missing References...", "0%", 0f);

            var allAssets = AssetDatabase.GetAllAssetPaths().Where(path => path.StartsWith("Assets/")).ToArray();
            var objs = allAssets.Select(a => AssetDatabase.LoadAssetAtPath(a, typeof(GameObject)) as GameObject).Where(a => a != null).ToArray();

            FindMissingReferences("Project", objs);

            EditorUtility.ClearProgressBar();
        }

        private static void FindMissingReferences(string context, GameObject[] gameObjects)
        {
            if (gameObjects == null)
            {
                return;
            }

            for (int i = 0; i < gameObjects.Length; i++)
            {
                float progress = i / (float)gameObjects.Length;
                EditorUtility.DisplayProgressBar("Finding Missing References...", $"{Mathf.RoundToInt(progress * 100f)}%", progress);

                GameObject go = gameObjects[i];
                var components = go.GetComponents<Component>();

                foreach (var component in components)
                {
                    // Missing components will be null, we can't find their type, etc.
                    if (!component)
                    {
                        Debug.LogErrorFormat(go, "Missing Component {0} in GameObject: {1}", component.GetType().FullName, GetFullPath(go));

                        continue;
                    }

                    if (SEARCH_MAIN_ASSEMBLY_ONLY)
                    {
                        if (string.CompareOrdinal("Assembly-CSharp", component.GetType().Assembly.GetName().Name) != 0)
                        {
                            continue;
                        }
                    }

                    SerializedObject so = new(component);
                    var sp = so.GetIterator();

                    var objRefValueMethod = typeof(SerializedProperty).GetProperty("objectReferenceStringValue",
                        BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

                    // Iterate over the components' properties.
                    while (sp.NextVisible(true))
                    {
                        if (sp.propertyType == SerializedPropertyType.ObjectReference)
                        {
                            string objectReferenceStringValue = string.Empty;

                            if (objRefValueMethod != null)
                            {
                                objectReferenceStringValue = (string)objRefValueMethod.GetGetMethod(true).Invoke(sp, new object[] { });
                            }

                            if (sp.objectReferenceValue == null &&
                                (sp.objectReferenceInstanceIDValue != 0 || objectReferenceStringValue.StartsWith("Missing")))
                            {
                                ShowError(context, go, component.GetType().Name, ObjectNames.NicifyVariableName(sp.name));
                            }
                        }
                        else if (sp.isArray)
                        {
                            if (SEARCH_ARRAYS_FOR_NULL)
                            {
                                for (int k = 0; k < sp.arraySize; k++)
                                {
                                    SerializedProperty currentArrayElement = sp.GetArrayElementAtIndex(k);
                                    if (currentArrayElement.propertyType == SerializedPropertyType.ObjectReference &&
                                        currentArrayElement.objectReferenceValue == null)
                                    {
                                        ShowError(context, go, component.GetType().Name, ObjectNames.NicifyVariableName(currentArrayElement.name));
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private static GameObject[] GetSceneObjects()
        {
            // Use this method since GameObject.FindObjectsOfType will not return disabled objects.
            return Resources.FindObjectsOfTypeAll<GameObject>()
                .Where(go => string.IsNullOrEmpty(AssetDatabase.GetAssetPath(go)) && go.hideFlags == HideFlags.None)
                .ToArray();
        }

        private static void ShowError(string context, GameObject go, string componentName, string propertyName)
        {
            const string errorTemplate = "Missing Ref in: [{3}]{0}. Component: {1}, Property: {2}";

            Debug.LogError(string.Format(errorTemplate, GetFullPath(go), componentName, propertyName, context), go);
        }

        private static string GetFullPath(GameObject go)
        {
            return go.transform.parent == null ? go.name : GetFullPath(go.transform.parent.gameObject) + "/" + go.name;
        }
    }
}
