using System.Reflection;
using UnityEditor;

namespace Buggestic.Editor.Utils
{
    public static class PropertyDrawerUtils
    {
        public static object GetTargetObject(this SerializedProperty propertyDrawerTarget, FieldInfo fieldInfo)
        {
            if (propertyDrawerTarget.depth == 0)
            {
                return fieldInfo.GetValue(propertyDrawerTarget.serializedObject.targetObject);
            }

            string nestedPropertyPath = propertyDrawerTarget.propertyPath.Replace($".{propertyDrawerTarget.name}", "");
            SerializedProperty nestedProperty = propertyDrawerTarget.serializedObject.FindProperty(nestedPropertyPath);
            return fieldInfo.GetValue(nestedProperty.boxedValue);
        }
    }
}
