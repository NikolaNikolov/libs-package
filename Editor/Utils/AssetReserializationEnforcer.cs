﻿using UnityEditor;

namespace Buggestic.Editor.Utils
{
    public static class AssetReserializationEnforcer
    {
        [MenuItem("Tools/Buggestic/Reserialize All Assets")]
        public static void ReserializeAllAssets()
        {
            AssetDatabase.ForceReserializeAssets();
        }
    }
}
