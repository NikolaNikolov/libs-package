using System.Collections.Generic;
using System.Reflection;
using Buggestic.Runtime.Stats;
using UnityEditor;
using UnityEngine;

namespace Buggestic.Editor.Stats
{
    [CustomPropertyDrawer(typeof(StatModifier), true)]
    public class StatModifierPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            List<Rect> rects = StatPropertyDrawerHelper.GetStatPropertyRects(position, 3);

            SerializedProperty valueProperty = property.FindPropertyRelative(StatModifier.StatModifier_Editor.VALUE);
            SerializedProperty typeProperty = property.FindPropertyRelative(StatModifier.StatModifier_Editor.TYPE);
            StatRangeAttribute statRangeAttribute = fieldInfo.GetCustomAttribute<StatRangeAttribute>();

            if (statRangeAttribute != null)
            {
                EditorGUI.Slider(rects[0], valueProperty, statRangeAttribute.Min, statRangeAttribute.Max, label);
            }
            else
            {
                EditorGUI.PropertyField(rects[0], valueProperty, label);
            }

            EditorGUI.PropertyField(rects[1], typeProperty, GUIContent.none);

            string modifierResult = CalculateModifierResult(valueProperty.floatValue, (StatModifierType)typeProperty.enumValueIndex);
            EditorGUI.LabelField(rects[2], modifierResult, StatPropertyDrawerHelper.CenteredLabelStyle);

            EditorGUI.EndProperty();
        }

        private string CalculateModifierResult(float modifierValue, StatModifierType modifierType)
        {
            return modifierType switch
            {
                StatModifierType.Flat => $"{(modifierValue >= 0f ? "+" : "")}{modifierValue}",
                StatModifierType.AdditiveMultiplier or StatModifierType.MultiplicativeMultiplier => $"x{1f + modifierValue:0.##}",
                _ => throw new System.ComponentModel.InvalidEnumArgumentException(nameof(modifierType), (int)modifierType, typeof(StatModifierType))
            };
        }
    }
}
