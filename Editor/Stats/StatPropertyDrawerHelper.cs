using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Buggestic.Editor.Stats
{
    public static class StatPropertyDrawerHelper
    {
        private const float HORIZONTAL_SPACE = 6f;

        private static readonly List<Rect> RECTS_BUFFER = new();

        public static GUIStyle CenteredLabelStyle { get; } = new(EditorStyles.label)
        {
            alignment = TextAnchor.MiddleCenter,
            clipping = TextClipping.Ellipsis,
        };

        public static List<Rect> GetStatPropertyRects(Rect position, int propertiesCount)
        {
            float totalWidth = Mathf.Max(position.width - HORIZONTAL_SPACE - HORIZONTAL_SPACE, 0f);
            float propertyWidth = Mathf.Max((totalWidth - EditorGUIUtility.labelWidth) / propertiesCount, 0f);
            float mainPropertyWidth = totalWidth - (propertyWidth * (propertiesCount - 1));

            RECTS_BUFFER.Clear();
            RECTS_BUFFER.Add(new Rect(position.x, position.y, mainPropertyWidth, position.height));
            for (int i = 0; i < propertiesCount - 1; i++)
            {
                float positionX = position.x + mainPropertyWidth + HORIZONTAL_SPACE + (i * (propertyWidth + HORIZONTAL_SPACE));
                RECTS_BUFFER.Add(new Rect(positionX, position.y, propertyWidth, position.height));
            }

            return RECTS_BUFFER;
        }

        public static Rect CombineRects(Rect first, Rect second)
        {
            Vector2 min = Vector2.Min(first.min, second.min);
            Vector2 max = Vector2.Max(first.max, second.max);
            return new Rect(min, max - min);
        }
    }
}
