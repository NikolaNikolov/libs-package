using System.Collections.Generic;
using System.Reflection;
using Buggestic.Editor.Utils;
using Buggestic.Runtime.Stats;
using UnityEditor;
using UnityEngine;

namespace Buggestic.Editor.Stats
{
    [CustomPropertyDrawer(typeof(StatBase), true)]
    public class StatPropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);

            List<Rect> rects = StatPropertyDrawerHelper.GetStatPropertyRects(position, 3);

            SerializedProperty baseValueProperty = property.FindPropertyRelative(StatBase.StatBase_Editor.BASE_VALUE);
            StatRangeAttribute rangeAttribute = fieldInfo.GetCustomAttribute<StatRangeAttribute>();

            using (EditorGUI.ChangeCheckScope changeScope = new())
            {
                Rect rect = StatPropertyDrawerHelper.CombineRects(rects[0], rects[1]);

                if (rangeAttribute != null)
                {
                    EditorGUI.Slider(rect, baseValueProperty, Mathf.Max(rangeAttribute.Min, 0f), rangeAttribute.Max, label);
                }
                else
                {
                    EditorGUI.PropertyField(rect, baseValueProperty, label);
                }

                if (changeScope.changed)
                {
                    baseValueProperty.floatValue = Mathf.Max(baseValueProperty.floatValue, 0f);
                    StatBase stat = (StatBase)property.GetTargetObject(fieldInfo);
                    stat.MarkDirty();
                }
            }

            if (Application.isPlaying && !PrefabUtility.IsPartOfAnyPrefab(property.serializedObject.targetObject))
            {
                object targetObject = property.GetTargetObject(fieldInfo);
                if (targetObject is Stat stat)
                {
                    EditorGUI.LabelField(rects[2],
                        $"x{stat.ModifiedOverBaseValue:0.##} = {stat.UnprocessedModifiedValue:0.##} ({stat.ModifiedValue:0.##})",
                        StatPropertyDrawerHelper.CenteredLabelStyle);
                }
                else
                {
                    StatBase statBase = (StatBase)targetObject;
                    EditorGUI.LabelField(rects[2],
                        $"x{statBase.ModifiedOverBaseValue:0.##} = {statBase.ModifiedValue:0.##}",
                        StatPropertyDrawerHelper.CenteredLabelStyle);
                }
            }

            EditorGUI.EndProperty();
        }
    }
}
