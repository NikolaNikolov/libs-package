using Buggestic.Runtime.UI.Misc;
using UnityEditor;

namespace Buggestic.Editor.UI.Misc
{
    [CustomEditor(typeof(CustomButtonBase), true)]
    public class CustomButtonBaseEditor : CustomSelectableBaseEditor
    {
        private SerializedProperty _onClickProperty;

        protected override void OnEnable()
        {
            base.OnEnable();

            _onClickProperty = serializedObject.FindProperty(CustomButtonBase.CustomButtonBase_Editor.ON_CLICK);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(_onClickProperty);

            serializedObject.ApplyModifiedProperties();
        }
    }
}
