using Buggestic.Runtime.UI.Misc;
using UnityEditor;
using UnityEditor.UI;

namespace Buggestic.Editor.UI.Misc
{
    [CustomEditor(typeof(CustomSelectableBase), true)]
    public class CustomSelectableBaseEditor : SelectableEditor
    {
        protected SerializedProperty _onDoStateTransitionProperty;

        protected override void OnEnable()
        {
            base.OnEnable();

            _onDoStateTransitionProperty = serializedObject.FindProperty(CustomSelectableBase.CustomSelectableBase_Editor.ON_DO_STATE_TRANSITION);
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            serializedObject.Update();

            EditorGUILayout.Space();
            EditorGUILayout.PropertyField(_onDoStateTransitionProperty);

            serializedObject.ApplyModifiedProperties();
        }
    }
}
